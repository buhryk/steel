<?php
use backend\widgets\ImagesWidget;

$this->title = Yii::t('event', 'Updating event images');
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Events list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= ImagesWidget::widget([
        'model' => $model,
        'parameters' => [
            'table_name' => $model::tableName()
        ]
    ]); ?>
</div>