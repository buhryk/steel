<?php
use yii\helpers\Html;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Events list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('event', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('event', 'Short description'); ?></th>
            <td><?= $model->short_description; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('event', 'Image'); ?></th>
            <td>
                <?php if ($model->image) { ?>
                    <?= Html::img($model->image->path, ['width' => 175]); ?>
                <?php } ?></td>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('event', 'Start date'); ?></th>
            <td><?= $model->start_date; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('event', 'Active'); ?></th>
            <td><?= $model->activeDetail; ?></td>
        </tr>
        </tbody>
    </table>
</div>