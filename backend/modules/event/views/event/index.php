<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\event\models\EventSubject;
use backend\modules\event\models\EventType;
use backend\modules\commondata\models\Country;
use backend\modules\event\models\Event;
use dosamigos\datepicker\DatePicker;

$this->title = Yii::t('event', 'Events list');
?>
<div class="event-index">
    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('event', 'Add event'); ?>
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('event', 'Title'),
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['event/view', 'id' => $model->primaryKey]);
                },
            ],

//            'alias',
            [
                'attribute' => 'start_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],

            [
                'attribute' => 'active',
                'filter' => Event::getAllActiveProperties(),
                'value' => 'activeDetail'
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>