<?php

use backend\modules\core\models\Page;
use backend\widgets\MainInputFile;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;
use backend\modules\event\models\EventSubject;
use backend\modules\event\models\EventType;
use backend\modules\commondata\models\Country;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'title') ?>

    <?= $form->field($modelLang, 'short_description')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 100,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/site/image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <?= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',

            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/site/image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <?= $form->field($model, 'active')->dropDownList(\backend\modules\event\models\Event::getAllActiveProperties()); ?>

    <?= $form->field($model, 'image')->widget(MainInputFile::className(), [
        'language'      => 'ru',
        'path'          => 'events',
        'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false       // возможность выбора нескольких файлов
    ])->label(false); ?>

    <?=  $form->field($model, 'page_id')->widget(Select2::classname(), [
        'data' => Page::getPageAll(true),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php echo $form->field($model, 'start_date')->widget(
    'backend\widgets\DateTimeWidget',
        [
            'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
            'clientOptions' => [
                'minDate' => new \yii\web\JsExpression('new Date("2015-01-01")'),
                'allowInputToggle' => false,
                'sideBySide' => true,
                'locale' => 'zh-cn',
                'widgetPositioning' => [
                   'horizontal' => 'auto',
                   'vertical' => 'auto'
                ]
            ]
        ]
); ?>

    <?= $form->field($model, 'start_date')->widget(DateTimePicker::className(),[
        'options' => [],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii'
        ]
    ]); ?>


    <div style="clear: both"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>