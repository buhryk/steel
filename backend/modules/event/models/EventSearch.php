<?php

namespace backend\modules\event\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\event\models\Event;
use yii\helpers\ArrayHelper;

class EventSearch extends Event
{
    public $title;

    public function rules()
    {
        return [
            [['id', 'subject_id', 'type_id', 'country_id', 'active'], 'integer'],
            [['alias', 'start_date', 'end_date', 'title'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $id = false)
    {
        $query = Event::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'type_id' => $this->type_id,
            'country_id' => $this->country_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'active' => $this->active,
        ]);

        $query->orderBy('id DESC');

        $query->andFilterWhere(['like', 'alias', $this->alias]);

        if ($id) {
            $query->andFilterWhere(['page_id' => $id]);
        }

        if ($this->title) {
            $langs = EventLang::find()
                ->where(['like', 'title', '%'.$this->title.'%', false])
                ->groupBy('event_id')
                ->all();

            $query->andFilterWhere(['in', 'id', $langs ? ArrayHelper::getColumn($langs, 'event_id') : [0]]);
        }

        return $dataProvider;
    }
}