<?php
namespace backend\modules\event\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\event\models\Event;
use backend\modules\event\models\EventSearch;
use backend\modules\event\models\EventLang;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;

class EventController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($page_id = false)
    {
        $model = new Event();
        $model->page_id = $page_id ;
        $modelLang = new EventLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {

            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->event_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->event_id = $model->primaryKey;
                $modelLang->save();

                if ($page_id){
                    return $this->redirect(['/core/page/event', 'id' => $page_id]);
                } else {
                    return $this->redirect(['update', 'id' => $model->id]);
                }

            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findEvent($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findEvent($id);

        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new EventLang();
            $modelLang->event_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                if ($modelLang->lang == TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT) {
                    $model->alias = TransliteratorService::transliterate($modelLang->title);
                } else {
                    $modelLangForAlias = EventLang::find()
                        ->where([
                            'category_id' => $model->primaryKey,
                            'lang' => TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT
                        ])
                        ->one();

                    if ($modelLangForAlias) {
                        $model->alias = TransliteratorService::transliterate($modelLangForAlias->title);
                    }
                }
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    private function findEvent($id)
    {
        $model = Event::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('event', 'Event not found'));
        }

        return $model;
    }

    public function actionDelete($id)
    {
        $model = $this->findEvent($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionSeo($id)
    {
        $model = $this->findEvent($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    public function actionImages($id)
    {
        $model = $this->findEvent($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang = Yii::$app->language;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }

    public function actionAddress($id)
    {
        $model = $this->findEvent($id);
        $modelLang = $model->lang;
        if ($modelLang->load(Yii::$app->request->post())&&$model->load(Yii::$app->request->post()) && $model->save() && $modelLang->save()) {
            return $this->refresh();
        }



        return $this->render('address', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }
}