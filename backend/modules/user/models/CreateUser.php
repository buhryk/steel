<?php

namespace backend\modules\user\models;

use common\models\User;
use yii\base\Model;

class CreateUser extends Model
{
    public $name;
    public $phone;
    public $username;
    public $email;
    public $password;

    public $subdivision_id;
    public $weight;
    public $role;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Уже використовуэться.'],

            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['subdivision_id' , 'integer'],
            ['weight' , 'integer'],
            [['phone', 'name'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон',
            'username' => 'Логін',
            'name' => 'ПІБ',
            'password' => 'Пароль',
        ];
    }

    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->email = $this->email;
        $user->name = $this->name;
        $user->role = $this->role;
        $user->phone = $this->phone;
        $user->username = $this->username;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        $user->status = User::STATUS_ACTIVE;
        if (!$user->save()) {
            return ;
        }

        return $user;
    }
}