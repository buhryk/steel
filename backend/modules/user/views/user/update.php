<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Редагування: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => $model->roleDetail, 'url' => ['index', 'role' => $model->role]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];

$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="user-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'modelChange'=>$modelChange,
    ]) ?>

</div>
