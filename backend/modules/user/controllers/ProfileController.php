<?php
namespace backend\modules\user\controllers;

use backend\controllers\BaseController;
use common\models\User;
use Yii;
use yii\web\Controller;
use backend\modules\user\models\PasswordChangeForm;

class ProfileController extends BaseController
{

    public function actionView()
    {
        return $this->render('view', [
            'model' => Yii::$app->user->identity,
        ]);
    }

    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->user->id);
        $modelChange = new PasswordChangeForm($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } elseif ($modelChange->load(Yii::$app->request->post()) && $modelChange->changePassword()) {
            Yii::$app->session->setFlash('info', 'Пароль змінено');
            return $this->redirect(['view']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelChange'=>$modelChange,
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}