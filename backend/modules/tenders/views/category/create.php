<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\NewsCategory */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Тендеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
