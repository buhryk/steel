<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\tenders\models\tenders as CurrentModel;
use backend\widgets\SortActionWidget;

$this->title = 'Тендеры';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => CurrentModel::className()]);
?>
<div class="page-index">

    <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>

    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => CurrentModel::className()]) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-custom dataTable no-footer'],
        'class'=>'table table-custom dataTable no-footer',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'image',
                'format' => ['image',['width'=>'70','height'=>'100']],
            ],
            'created_at:datetime',
            [
                'attribute' => 'status',
                'filter' => $searchModel::getStatusList(),
                'value' => 'statusDetail'
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
