<?php
//use vova07\imperavi\Widget as ImperaviWidget;
use backend\assets\FrontendAsset;
use backend\assets\RedactorAsset;
use backend\modules\core\models\Page;
use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use frontend\models\User;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\modules\tenders\models\tendersCategory;
use mihaildev\elfinder\InputFile;
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\tenders */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(['id'=>'form']); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), [])?>

    <div class="row">
        <div class="col-md-4">
            <?=  $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => tendersCategory::getCategoryAll(true),
                'language' => 'ru',
                'options' => ['placeholder' => 'Выберите компанию ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?=  $form->field($model, 'company_id')->widget(Select2::classname(), [
                'data' => User::getCompanyAll(true),
                'language' => 'ru',
                'options' => ['placeholder' => 'Выберите компанию ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model::getTypeList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?php
            if($model->data_pub) {
                $model->created_at = date("yyyy-mm-dd hh:ii", $model->data_pub);
            }
            echo $form->field($model, 'data_pub')->widget(DateTimePicker::className(),[
                'options' => [],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii'
                ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'tenders',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script language="JavaScript">
    function validForm() {
        form.submit() // Отправляем на сервер
    }
</script>

