<?php

namespace backend\modules\tenders\models;

use backend\modules\core\models\Lang;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page_category".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $alias
 * @property string $additional_data
 *
 * @property tenders[] $pages
 * @property tendersCategorLang[] $pageCategorLangs
 * @property Lang[] $langs
 */
class tendersCategory extends BaseDataModel
{
    public $title;
    public $description;
    public $text;

    public static function tableName()
    {
        return 'tenders_category';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
            [
                'class' => LangBehavior::className(),
                't' => new tendersCategorLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['alias'], 'string', 'max' => 255],
            [['parent_id'], 'integer'],
            [['title', 'description', 'text'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'description' => 'Опис',
            'text' => 'Контент',
            'parent_id' => 'Батьківська категорія'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function gettenders()
    {
        return $this->hasMany(tenders::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function gettendersCategorLangs()
    {
        return $this->hasMany(tendersCategorLang::className(), ['record_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Lang::className(), ['id' => 'lang_id'])->viaTable('tenders_categor_lang', ['record_id' => 'id']);
    }

    public function getChildrens()
    {
        return $this->hasMany(tendersCategory::className(), ['parent_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(tendersCategory::className(), ['id' => 'parent_id']);
    }


    static public function getCategoryAll($map = false, $limit = null)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC]);

        if ($limit) {
            $query->limit($limit);
        }

        $models = $query->all();
        if ($map) {
            return ['' => ''] + ArrayHelper::map($models, 'id', 'title');
        }
        return $models;
    }
}
