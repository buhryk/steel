<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\publication\models\Publication */

$this->title = 'Створення';
$this->params['breadcrumbs'][] = ['label' => 'Видання', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publication-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
