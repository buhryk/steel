<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\publication\models\Publication */

$this->title = 'Редагування: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Видання', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="publication-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
