<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\ImperaviWidget;
use backend\modules\core\models\Menu;
?>

<div class="publication-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), []) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'menu_id')->dropDownList(Menu::getMenuAll(true, 37)) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(\backend\widgets\MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'publication',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
