<?php

namespace backend\modules\publication\models;

use backend\modules\core\models\Lang;
use backend\modules\core\models\Menu;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;

/**
 * This is the model class for table "publication".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 * @property string $alias
 * @property integer $menu_id
 * @property string $additional_data
 *
 * @property PublicationLang[] $publicationLangs
 * @property Lang[] $langs
 */
class Publication extends BaseDataModel
{
    public $title;
    public $description;

    public static function tableName()
    {
        return 'publication';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['image'], 'string', 'max' => 255],
            [['menu_id',], 'integer'],
            [['menu_id',], 'required'],
            [['title', 'description'], 'safe'],
            [['menu_id'], 'exist', 'skipOnError' => false, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ]);
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => LangBehavior::className(),
                't' => new PublicationLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'title' => 'Заголовок',
            'description' => 'Опис',
            'menu_id' => 'Пункт меню',
        ]);
    }

    public function getLang()
    {
        return $this->hasMany(PublicationLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
