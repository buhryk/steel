<?php
use yii\helpers\Html;
use backend\assets\RulesManagingAsset;
use yii\helpers\Url;
RulesManagingAsset::register($this);

$this->title = 'Управление доступами';
$this->params['breadcrumbs'][] = ['label' => 'Список ролей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $role->title, 'url' => ['view', 'id' => $role->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .role-update .glyphicon{
        font-size: 17px;
        cursor: pointer;
    }
</style>

<div class="role-update">
    <form onsubmit="return false;" data-url="<?= Url::to(['rules', 'id' => $role->primaryKey]); ?>" id="rules-form">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php foreach ($modules as $module) { ?>
            <?php $controllers = $module->controllers; ?>
            <?php if (!$controllers || !$controllers[0]->actions) continue; ?>
            <div style="border: 1px solid #ddd; margin: 5px 0;">
                <div class="col-md-2">
                    <h3>
                        <?= $module->title; ?>&nbsp;
                        <span class="glyphicon glyphicon-info-sign"
                              aria-hidden="true"
                              title="<?= $module->description; ?>">
                        </span>
                    </h3>
                </div>
                <div class="col-md-7" style="border-bottom: 1px solid #ddd;">
                    <?php foreach ($module->controllers as $controller) { ?>
                        <?php $actions = $controller->actions; ?>
                        <?php if (!$actions) continue; ?>
                        <div class="col-md-2">
                            <h4>
                                <?= $controller->title; ?>&nbsp;
                                <span class="glyphicon glyphicon-info-sign"
                                      aria-hidden="true"
                                      title="<?= $controller->description; ?>">
                                </span>
                            </h4>
                        </div>
                        <div class="col-md-10">
                            <div class="checkbox" style="float: right">
                                <label>
                                    <input type="checkbox"
                                           class="check-all-controller"
                                           data-controller-id="<?= $controller->primaryKey; ?>"
                                           data-module-id="<?= $module->primaryKey; ?>"
                                    >
                                    Отметить весь контроллер
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            &nbsp;
                        </div>
                        <div class="col-md-9">
                            <?php foreach ($actions as $action) { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               class="action-checkbox"
                                               name="action[<?= $action->primaryKey; ?>]"
                                               data-controller-id="<?= $controller->primaryKey; ?>"
                                               data-module-id="<?= $module->primaryKey; ?>"
                                               data-action-id="<?= $action->primaryKey; ?>"
                                               <?= in_array($action->primaryKey, $currentRoleActionsIds) ? 'checked="checked"' : ''; ?>
                                        >
                                        <?= $action->title; ?>
                                    </label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-3">
                    <div class="checkbox" style="float: right">
                        <label>
                            <input type="checkbox"
                                   data-module-id="<?= $module->primaryKey; ?>"
                                   class="check-all-module"
                            >
                            Отметить весь модуль
                        </label>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        <?php } ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn btn-primary',
                'id' => 'submit-rules-form'
            ]) ?>
        </div>
    </form>
</div>