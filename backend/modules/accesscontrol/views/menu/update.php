<?php
use yii\helpers\Html;

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Меню админ панели', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>