<?php
use yii\helpers\Html;

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Список модулей', 'url' => ['module/index']];
$controller = $model->controller;
$module = $controller ? $controller->module : null;
if ($module) {
    $this->params['breadcrumbs'][] = [
        'label' => 'Модуль "' . $module->title . '"',
        'url' => ['module/view', 'id' => $module->primaryKey]
    ];
    $this->params['breadcrumbs'][] = [
        'label' => 'Контроллер "' . $controller->title . '"',
        'url' => ['controller/view', 'id' => $controller->primaryKey]
    ];
}
$this->params['breadcrumbs'][] = [
    'label' => 'Действие "' . $model->title . '"',
    'url' => ['view', 'id' => $model->primaryKey]
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>