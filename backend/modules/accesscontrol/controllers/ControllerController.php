<?php
namespace backend\modules\accesscontrol\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\accesscontrol\models\Module;
use Yii;
use yii\web\Controller;
use backend\modules\accesscontrol\models\ModuleController;
use yii\web\NotFoundHttpException;

class ControllerController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionCreate()
    {
        $model = new ModuleController();
        $module = Yii::$app->request->get('module') ? Module::findOne(Yii::$app->request->get('module')) : null;

        if ($module) {
            $model->module_id = $module->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = ModuleController::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}