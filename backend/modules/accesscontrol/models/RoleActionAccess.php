<?php

namespace backend\modules\accesscontrol\models;

use Yii;

class RoleActionAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role_action_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'role_id'], 'required'],
            [['action_id', 'role_id'], 'integer'],
            [['action_id'], 'exist', 'skipOnError' => true,
                'targetClass' => ModuleControllerAction::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'role_id' => 'Role ID',
        ];
    }

    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public function getAction()
    {
        return $this->hasOne(ModuleControllerAction::className(), ['id' => 'action_id']);
    }
}