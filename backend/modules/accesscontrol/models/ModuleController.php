<?php

namespace backend\modules\accesscontrol\models;

use Yii;


class ModuleController extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_controller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id', 'alias', 'title'], 'required'],
            [['module_id'], 'integer'],
            [['description'], 'string'],
            [['alias', 'title'], 'string', 'max' => 128],
            [['module_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Module::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module_id' => 'Модуль',
            'alias' => 'Alias',
            'title' => 'Название',
            'description' => 'Описание',
        ];
    }

    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }

    public function getActions()
    {
        return $this->hasMany(ModuleControllerAction::className(), ['controller_id' => 'id']);
    }

    public function beforeDelete()
    {
        ModuleControllerAction::deleteAll(['controller_id' => $this->primaryKey]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}