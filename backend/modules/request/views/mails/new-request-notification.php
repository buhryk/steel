<?php
use backend\modules\request\models\Request;

/* @var $model Request */
/* @var $user backend\models\User */
?>

<table>
    <tr>
        <td>Заказ №<?= $model->id; ?> :: <?= isset(Yii::$app->params['projectName']) ? Yii::$app->params['projectName'] : 'True'; ?></td>
    </tr>
    <tr>
        <td>
            <p><b>Уважаемый <?= $user->name ? $user->name : $user->email; ?>!</b></p>
            <?php $url = Yii::$app->urlManager->createAbsoluteUrl(['/admin/request/request/view', 'id' => $model->id]); ?>
            <p><b>Появилась новая заявка (<?= $model->typeDetail; ?>):</b> <a href="<?= $url; ?>">#<?= $model->id; ?></a></p>
            <p>
                <b>Тип заявки:</b> <?= $model->typeDetail; ?><br>
                <b>Имя:</b> <?= $model->name; ?><br>
                <b>Телефон:</b> <?= $model->phone; ?><br>
                <b>Email:</b> <?= $model->email; ?><br>
                <b>Сообщение:</b> <?= $model->text; ?>
            </p>
        </td>
    </tr>
</table>