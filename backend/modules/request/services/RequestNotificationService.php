<?php

namespace backend\modules\request\services;

use backend\modules\request\models\Request;
use Yii;
use backend\models\User;

class RequestNotificationService
{
    public function notifyAboutNewRequest(Request $request)
    {
        $users = User::find()->where(['notification_of_request' => User::YES])->all();

        if ($request) {
            foreach ($users as $user) {
                Yii::$app->mailer->compose('@backend/modules/request/views/mails/new-request-notification', ['user' => $user, 'model' => $request])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($user->email)
                    ->setSubject('Уведомление о новой заявке')
                    ->send();
            }
        }
    }
}