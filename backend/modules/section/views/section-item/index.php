<?php

use backend\modules\catalog\models\Catalog as CurrentModel;
use backend\modules\core\models\Page;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Секции';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="page-index">
    <?php Pjax::begin() ?>
    <?= Html::a('<i class="fa fa-plus modalButton" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>

<!--    <div class="pull-right">-->
<!--        --><?//=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => CurrentModel::className()]) ?>
<!--    </div>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-custom dataTable no-footer'],
        'class'=>'table table-custom dataTable no-footer',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'section_id',
                'filter' => ArrayHelper::map(Page::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $page = $model->page;
                    return $page ? $page->title : '';
                }
            ],
            'widget_id',
            [
                'attribute' => 'image',
                'format' => ['image',['width'=>'70','height'=>'100']],
            ],
//            'created_at:datetime',
//            [
//                'attribute' => 'status',
//                'filter' => $searchModel::getStatusList(),
//                'value' => 'statusDetail'
//            ],
            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return

                        Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            ['/section/section-item/update', 'id' => $model->id],
                            ['class' => 'btn btn-default btn-sm modalButton', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['/section/section-item/delete', 'id' => $model->id],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
