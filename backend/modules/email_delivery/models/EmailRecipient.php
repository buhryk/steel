<?php

namespace backend\modules\email_delivery\models;

use Yii;

/**
 * This is the model class for table "email_recipient".
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property string $to_email
 * @property integer $status
 * @property integer $views_count
 * @property integer $first_view
 * @property integer $last_view
 * @property integer $created_at
 */
class EmailRecipient extends \yii\db\ActiveRecord
{
    const STATUS_NOT_VIEWED = 0;
    const STATUS_VIEWED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_recipient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'to_email'], 'required'],
            [['delivery_id', 'status', 'views_count', 'first_view', 'last_view', 'created_at'], 'integer'],
            [['to_email'], 'string', 'max' => 255],
            ['to_email', 'email'],
            ['views_count', 'default', 'value' => 0],
            ['status', 'in', 'range' => [self::STATUS_NOT_VIEWED, self::STATUS_VIEWED]],
            ['status', 'default', 'value' => self::STATUS_NOT_VIEWED]
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_id' => 'Delivery ID',
            'to_email' => 'To Email',
            'status' => 'Status',
            'views_count' => 'Views Count',
            'first_view' => 'First View',
            'last_view' => 'Last View',
            'created_at' => 'Created At',
        ];
    }
}
