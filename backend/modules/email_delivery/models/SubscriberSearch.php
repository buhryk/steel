<?php

namespace backend\modules\email_delivery\models;

use backend\modules\blog\models\ItemSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\email_delivery\models\Subscriber;

/**
 * SubscriberSearch represents the model behind the search form about `backend\modules\email_delivery\models\Subscriber`.
 */
class SubscriberSearch extends Subscriber
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'can_send'], 'integer'],
            [['email', 'name'], 'safe'],
            ['created_at', 'validateDateTime'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute && ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)))) {
            $this->addError($attribute, 'Поле должно быть формата "2000-00-00"');
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subscriber::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'can_send' => $this->can_send,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name]);

        if ($this->created_at) {
            $query->andFilterWhere([
                'between',
                'created_at',
                strtotime($this->created_at),
                strtotime($this->created_at) + ItemSearch::ONE_DAY_IN_SECONDS]);
        }

        return $dataProvider;
    }
}
