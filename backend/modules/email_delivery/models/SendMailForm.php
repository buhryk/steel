<?php

namespace backend\modules\email_delivery\models;

use backend\modules\email_delivery\services\EmailSendingService;
use Yii;
use yii\base\Model;


class SendMailForm extends Model
{
    public $from_name;
    public $from_email;
    public $to_emails = [];
    public $subject;
    public $body;

    public function __construct(array $config)
    {
        $this->from_name = isset($config['from_name']) && $config['from_name']
            ? $config['from_name']
            : Yii::$app->name;
        $this->from_email = isset(Yii::$app->params['supportEmail']) && Yii::$app->params['supportEmail']
            ? Yii::$app->params['supportEmail']
            : null;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_name', 'from_email', 'to_emails', 'subject', 'body'], 'required'],
            [['from_name', 'from_email', 'subject'], 'string', 'min' => 3, 'max' => 128],
            ['body', 'string'],
            ['from_email', 'email'],
            ['to_emails', 'safe']
        ];
    }

    public function sendEmail()
    {
        if (is_array($this->to_emails)) {
            $emailSendingService = new EmailSendingService();
            $emailSendingService->send($this);

            return true;
        }

        return false;
    }
}
