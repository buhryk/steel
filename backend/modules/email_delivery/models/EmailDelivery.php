<?php

namespace backend\modules\email_delivery\models;

use Yii;

/**
 * This is the model class for table "email_delivery".
 *
 * @property integer $id
 * @property string $subject
 * @property string $from_email
 * @property string $from_name
 * @property string $body
 * @property integer $created_at
 */
class EmailDelivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'from_email', 'from_name', 'body'], 'required'],
            [['body'], 'string'],
            [['created_at'], 'integer'],
            [['subject', 'from_email', 'from_name'], 'string', 'max' => 255],
            ['from_email', 'email']
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'from_email' => 'From Email',
            'from_name' => 'From Name',
            'body' => 'Body',
            'created_at' => 'Created At',
        ];
    }

    public function getRecipientsCount()
    {
        return EmailRecipient::find()->where(['delivery_id' => $this->id])->count();
    }

    public function getRecipients()
    {
        return $this->hasMany(EmailRecipient::className(), ['delivery_id' => 'id']);
    }
}
