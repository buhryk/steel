<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\email_delivery\models\EmailDeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рассылки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-delivery-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['send']); ?>" class="btn btn-primary right block">
            <i class="fa fa-paper-plane" aria-hidden="true"></i>Создать рассылку
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'subject',
            'from_email:email',
            'from_name',
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'contentOptions' => ['style'=>'width: 160px;'],
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'label' => 'Кол-во получателей',
                'value' => 'recipientsCount'
            ],
            [
                'contentOptions' => ['style' => 'width: 75px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")',
                                'data-method' => 'post'
                            ]
                        );
                }
            ],
        ],
    ]); ?>
</div>
