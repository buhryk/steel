<?php

use yii\helpers\Html;
use backend\modules\email_delivery\models\EmailRecipient;

/* @var $this yii\web\View */
/* @var $model backend\modules\email_delivery\models\EmailDelivery */

$this->title = "Рассылка #$model->id";
$this->params['breadcrumbs'][] = ['label' => 'Рассылки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$recipients = $model->recipients;
?>

<div class="email-delivery-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('subject'); ?></th>
            <td><?= $model->subject; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('from_email'); ?></th>
            <td><?= $model->from_email; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('from_name'); ?></th>
            <td><?= $model->from_name; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('body'); ?></th>
            <td><?= $model->body; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('created_at'); ?></th>
            <td><?= date('Y-m-d H:i', $model->created_at); ?></td>
        </tr>
        <tr>
            <th>Получатели (<?= count($recipients); ?>)</th>
            <td>
                <table id="w1" class="table table-striped table-bordered detail-view">
                    <thead>
                    <tr>
                        <th width="40px;">&nbsp;</th>
                        <th>Email</th>
                        <th>Кол-во просмотров</th>
                        <th>Первый просмотр</th>
                        <th>Последний просмотр</th>
                        <th>Отправлено</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($recipients as $recipient) { ?>
                        <?php $viewed = $recipient->status == EmailRecipient::STATUS_VIEWED ? true : false; ?>
                        <tr>
                            <td style="padding-left: 16px; padding-top: 12px;" title="<?= $viewed ? 'Просмотрено' : 'Не просмотрено'; ?>">
                                <div style="height: 8px; width: 8px; background: <?= $viewed ? 'green' : 'red'; ?>;"></div>
                            </td>
                            <td><?= $recipient->to_email; ?></td>
                            <td><?= $recipient->views_count; ?></td>
                            <td><?= $recipient->first_view ? date('Y-m-d H:i:s', $recipient->first_view) : ''; ?></td>
                            <td><?= $recipient->last_view ? date('Y-m-d H:i:s', $recipient->last_view) : ''; ?></td>
                            <td><?= date('Y-m-d H:i:s', $recipient->created_at); ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
