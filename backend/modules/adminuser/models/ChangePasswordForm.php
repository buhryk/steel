<?php

namespace backend\modules\adminuser\models;

use frontend\models\User;
use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $repeat_new_password;

    public function rules()
    {
        return [
            [['old_password', 'new_password', 'repeat_new_password'], 'required', 'message' => 'Необходимо заполнить "{attribute}"'],
            ['repeat_new_password',
                'compare',
                'compareAttribute' => 'new_password',
                'message' => '"'. $this->getAttributeLabel('repeat_new_password') . '" и "' . $this->getAttributeLabel('new_password') . '" должны совпадать'
            ],
        ];
    }

    public function attributeLabels(){
        return [
            'old_password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'repeat_new_password' => 'Повторить новый пароль',
        ];
    }

    public function changePassword($user)
    {
        $user->setPassword($this->new_password);
        $user->update(false);

        return true;
    }

    public function validateOldPassword($user, $old_password)
    {
        if (!Yii::$app->security->validatePassword($old_password, $user->password_hash)) {
            $this->addError('old_password', '"'.$this->getAttributeLabel('old_password') . '" введен неверно');
            return false;
        }

        return true;
    }
}