<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение пароля';
$this->params['breadcrumbs'][] = ['label' => 'Список администраторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->email, 'url' => ['view', 'id' => $user->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="role-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'old_password')->textInput(['type' => 'password']); ?>
        <?= $form->field($model, 'new_password')->textInput(['type' => 'password']); ?>
        <?= $form->field($model, 'repeat_new_password')->textInput(['type' => 'password']); ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>