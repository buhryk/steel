<?php

namespace backend\modules\news\controllers;

use backend\controllers\BaseController;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\catalog\models\CatalogToCategory;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\NewsToCategory;
use common\models\Lang;
use Yii;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class NewsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if (!$model->image){
            $model->image = '/images/news.png';
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $category_ids = Yii::$app->request->post('category_id');
            NewsToCategory::deleteAll(['news_id' => $model->id]);
            foreach ($category_ids as $item) {
                $category = NewsCategory::findOne($item);
                $catalog_to_category = new NewsToCategory();
                $catalog_to_category->news_id = $model->id;
                $catalog_to_category->category_id = $item;
                $catalog_to_category->save();
            }

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->image){
            $model->image = '/images/news.png';
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $category_ids = Yii::$app->request->post('category_id');
            NewsToCategory::deleteAll(['news_id' => $model->id]);
            foreach ($category_ids as $item) {
                $category = NewsCategory::findOne($item);
                $catalog_to_category = new NewsToCategory();
                $catalog_to_category->news_id = $model->id;
                $catalog_to_category->category_id = $item;
                $catalog_to_category->save();
            }

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImages($id)
    {
        $model = $this->findModel($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang_id = Lang::getCurrent()->id;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }
}
