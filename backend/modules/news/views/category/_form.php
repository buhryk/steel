<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\news\models\NewsCategory;
use yii\helpers\ArrayHelper;

$parentsList = NewsCategory::find()
    ->where(['status' => NewsCategory::STATUS_ACTIVE])
    ->andFilterWhere(['!=', 'id', $model->id])
    ->orderBy(['position' => SORT_ASC])
    ->all();
?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'text')->textarea() ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'parent_id')
                ->dropDownList(ArrayHelper::map($parentsList, 'id', 'title'), ['prompt' => '']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
