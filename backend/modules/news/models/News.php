<?php

namespace backend\modules\news\models;

use backend\modules\catalog\models\CatalogToCategory;
use backend\modules\images\models\Image;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 * @property string $alias
 * @property integer $category_id
 * @property string $additional_data
 */
class News extends BaseDataModel
{
    const TYPE_NEWS = 1;
    const TYPE_PUBLICATION = 2;
    const TYPE_PRESENTATION = 3;

    public $title;
    public $description;
    public $text;
    public $record_id;

    static $current = null;

    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
            [
                'class' => LangBehavior::className(),
                't' => new NewsLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['data_pub'], 'required'],
            [['alias', 'image'], 'string', 'max' => 255],
            [['category_id', 'type', 'company_id'], 'integer'],
            [['title', 'description', 'text'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'description' => 'Опис',
            'text' => 'Контент',
            'category_id' => 'Категорія',
            'company_id' => 'Компания',
            'data_pub' => 'Дата публікації',
            'type' => 'Тип'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasMany(NewsLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
               // 'is_main' => Image::IS_MAIN_YES
            ]);
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
            ])
            ->orderBy('is_main DESC')
            ->addOrderBy('sort DESC');
    }

    static public function getCategoryAll($map = false)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC]);

        $models = $query->all();
        if ($map) {
            return ['' => ''] + ArrayHelper::map($models, 'id', 'title');
        }
        return $models;
    }

    public static function getCurrent()
    {
        if (self::$current == null) {
            self::$current = self::find()
                ->where(['alias' => Yii::$app->request->get('alias')])
                ->one();
        }

        return self::$current;
    }

    public function getLettersItem($ids)
    {
        $connection = Yii::$app->db;

        $model = $connection->createCommand('select  LEFT(title, 1) as letter   from `news` 
                            LEFT JOIN news_lang ON (record_id = id)
                              WHERE  lang_id = ' . Lang::getCurrent()->id . '
                              AND category_id in('.implode(',',$ids).')
                              Group By LEFT(title, 1)
                                ORDER BY LEFT(title, 1) COLLATE  utf8_unicode_ci'
        );

        return $model->queryAll();
    }

    public function getItemByLetter($letter, $ids)
    {
        $connection = Yii::$app->db;

        $model = $connection->createCommand('select *  from `news` 
                            LEFT JOIN news_lang ON (record_id = id)
                              WHERE  lang_id = ' . Lang::getCurrent()->id . '
                              AND category_id in('.implode(',',$ids).')
                              and  LEFT(title, 1) = "'.$letter.'"
                                ORDER BY `title` COLLATE  utf8_unicode_ci'
        );

        $news = [];
        foreach ($model->queryAll() as $item){
            $k = str_replace("'","",mb_strtolower($item['title']));
            $k = str_replace("і","й",$k);
            $k = str_replace("ї","й",$k);
            $news[$k] = (object)$item;
        }

        ksort($news);

        return $news;
    }

    static public function getTypeList()
    {
        return [
            self::TYPE_NEWS => 'Новость',
            self::TYPE_PUBLICATION => 'Публикация',
            self::TYPE_PRESENTATION => 'Презентация',
        ];
    }

    public function getTypeDetail()
    {
        return isset(self::getTypeList()[$this->type]) ? self::getTypeList()[$this->type] : '';
    }

    public function getCategories()
    {
        return $this->hasMany(NewsToCategory::className(), ['news_id' => 'id']);
    }
}
