<?php

namespace backend\modules\news\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "page_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property string $text
 */
class NewsLang extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'news_lang';
    }

    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description', 'text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
        ];
    }

    public function getRecord()
    {
        return $this->hasOne(News::className(), ['id' => 'record_id']);
    }
}
