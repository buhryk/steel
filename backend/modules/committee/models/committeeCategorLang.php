<?php

namespace backend\modules\committee\models;

use Yii;
use common\models\Lang;
/**
 * This is the model class for table "page_categor_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property string $text
 *
 * @property Lang $lang
 * @property committeeCategory $record
 */
class committeeCategorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'committee_categor_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'title'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description', 'text'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => committeeCategory::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(committeeCategory::className(), ['id' => 'record_id']);
    }
}
