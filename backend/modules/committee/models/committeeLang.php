<?php

namespace backend\modules\committee\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "page_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property string $text
 */
class committeeLang extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'committee_lang';
    }

    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description', 'text', 'text2'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => committee::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    public function getRecord()
    {
        return $this->hasOne(committee::className(), ['id' => 'record_id']);
    }
}
