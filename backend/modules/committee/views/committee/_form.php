<?php
//use vova07\imperavi\Widget as ImperaviWidget;
use backend\assets\FrontendAsset;
use backend\assets\RedactorAsset;
use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\modules\committee\models\committeeCategory;
use mihaildev\elfinder\InputFile;
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\committee */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(['id'=>'form']); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), []) ?>

    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), [])?>
    <?= $form->field($model, 'text2')->widget(ImperaviWidget::className(), [])?>

    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'preview')->widget(MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'committee',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'committee',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ]); ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script language="JavaScript">
    function validForm() {
        form.submit() // Отправляем на сервер
    }
</script>

