<?php

namespace backend\modules\core\models;

use Yii;
use common\behaviors\JsonFields;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $key
 * @property string $group
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $additional_data
 */
class Setting extends \yii\db\ActiveRecord
{
    const FOOTER_SOCIAL_ICONS = 'footer_social_icons';
    const FILTER_TIME_ITEMS = 'filter_time_items';
    const FILTER_PRICE_STEP = 'filter_price_step';
    const FILTER_COUNT_USER = 'filter_count_user';
    const FILTER_TAG_MANAGER = 'tag_manager';

    const COMMON = 'common';
    const SYSTEM = 'system';
    const ADDITIONAL_DATA_ITEMS = 'items';

    public $shortClassName;

    static $_commissionPercent = null;
    static $_system = null;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    public function behaviors()
    {
        return [
            'jsonFields' => [
                'class' => JsonFields::className(),
                'fields' => [
                    'additional_data'
                ]
            ],
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group'], 'required'],
            [['value'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['key', 'group'], 'string', 'max' => 255],
            [['key'], 'unique'],
            [ 'additional_data', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'group' => 'Group',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'additional_data' => 'Additional Data',
        ];
    }

    public static  function getGroupItem()
    {
        return self::find()->select('group')->distinct(['group'])->all();
    }

    public static function getByKey($key)
    {
        return  self::find()->andWhere(['key' => $key])->one();
    }

    public static function getValueByKey($key)
    {
        return  self::find()->andWhere(['key' => $key])->one()->value ?? null;
    }

    public static function getSystemElement($key)
    {
        if (self::$_system === null) {
            $items = self::find()->where(['group' => self::SYSTEM])->asArray()->all();
            foreach ($items as $item) {
                self::$_system[$item['key']] = $item['value'];
            }
        }

        return isset(self::$_system[$key]) ? self::$_system[$key] : null;
    }

    public static function getDataByKey($key, $field)
    {
        $model = self::find()->andWhere(['key' => $key])->one();

        return $model->getData($field);
    }

    public static function getCommissionPercent()
    {
        if (static::$_commissionPercent == null) {
            $model = self::getByKey('commission_percent');
            static::$_commissionPercent = $model ? $model->value : 10;
        }

        return static::$_commissionPercent;
    }

}
