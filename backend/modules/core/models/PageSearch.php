<?php

namespace backend\modules\core\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\core\models\Page;

/**
 * PageSearch represents the model behind the search form about `backend\modules\core\models\Page`.
 */
class PageSearch extends Page
{
    public $menu_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'status'], 'integer'],
            [['image', 'alias', 'additional_data', 'title'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->menu_id) {

            $query->innerJoin('menu', 'page.id = menu.page_id');
            $query->andWhere(['IN', 'menu.id', $this->findMenuId()]);
        }

        if ($this->title) {
            $query->joinWith('lang');
            $query->andWhere(['like', 'title', $this->title]);
        }
        if ($this->category_id) {
            $categories_id = $this->findCategoriesId();
            $query->andWhere(['in', 'category_id', $categories_id]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'additional_data', $this->additional_data]);

        return $dataProvider;
    }

    protected function findCategoriesId()
    {
        $citegories = [$this->category_id];
        $model = PageCategory::findOne($this->category_id);
        if (!$model) {
            return [];
        }
       return array_merge($citegories, $this->item($model));
    }

    protected function item($model, $categories_id = [])
    {
        $categories = [];
        foreach ($model->childrens as $item) {
            $categories[] = $item->id;
            $categories = array_merge($categories, $this->item($item));
        }
        return $categories;
    }

    protected function findMenuId()
    {
        $citegories = [$this->menu_id];
        $model = Menu::findOne($this->menu_id);
        if (!$model) {
            return [];
        }
        return array_merge($citegories, $this->item($model));
    }




}
