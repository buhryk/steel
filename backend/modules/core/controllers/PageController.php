<?php

namespace backend\modules\core\controllers;

use backend\controllers\BaseController;
use backend\modules\core\models\Menu;
use backend\modules\core\models\PageCategory;
use backend\modules\core\models\Setting;
use backend\modules\event\models\EventSearch;
use backend\modules\faq\models\Faq;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use backend\modules\table\models\TableSearch;
use Yii;
use backend\modules\core\models\Page;
use backend\modules\core\models\PageSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BaseController
{
    public $enableCsrfValidation = false;

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex($category_id = null, $menu_id = null)
    {

        $searchModel = new PageSearch();
        $searchModel->category_id = $category_id;
        $searchModel->menu_id = $menu_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $menuTree = $this->menuGenerate($menu_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'menuTree' => $menuTree
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Зміни збережено');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionMenu($id)
    {
        $model = $this->findModel($id);
        $menuModel = Menu::findOne(['page_id' => $id]);
        if (! $menuModel) {
            $menuModel = new Menu();
            $menuModel->page_id = $id;
        }

        if ($menuModel->load(Yii::$app->request->post()) && $menuModel->save()) {
            Yii::$app->session->setFlash('success', 'Зміни збережено');
            return $this->redirect(['menu', 'id' => $model->id]);
        } else {
            return $this->render('menu', [
                'model' => $model,
                'menuModel' => $menuModel
            ]);
        }
    }

    public function actionWidget($id, $section_id = false, $key = false)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {

            $new_data = [];

            if (Yii::$app->request->post('widget')) {

                if (Yii::$app->request->post('faq')) {

//                    pr(Yii::$app->request->post('widget'), true);

                    Faq::deleteAll(['page_id' => $id]);

                    $faq = Yii::$app->request->post('faq');

                    foreach ($faq as $k => $item) {
                        $faq_model = new Faq();
                        $faq_model->page_id = $id;
                        $faq_model->title = $item['title'];
                        $faq_model->text = $item['text'];
                        $faq_model->save();
                    }
                }

                $data = Yii::$app->request->post('widget');

                foreach ($data as $k => $item) {
                    if (!isset($item['delete'])) {
                        $new_data[$k] = $item;
                    }
                }
            }
            $model->setData($model::ADDITIONAL_DATA_ITEMS, $new_data);

            $model->save();

            Yii::$app->session->setFlash('success', 'Changes saved.');
            return $this->redirect(['widget', 'id' => $model->id]);
        }


        return $this->render('widget', [
            'model' => $model,
            'section_key' => $key,
            'section_id' => $section_id
        ]);
    }

    public function actionEvent($id)
    {
        $model = $this->findModel($id);
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('event', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTable($id)
    {
        $model = $this->findModel($id);
        $searchModel = new TableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('table', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function categoryGenerate($category_id)
    {
        $items[] = ['id' => '0', 'text' => 'Всі',];
        $models = PageCategory::find()
            ->where(['IS', 'parent_id', null])
            //->andFilterWhere(['id' => $category_id])
            ->all();
        foreach ($models as $model) {
            $items[] = [
                'id' => $model->id,
                'text' => $model->title,
                'children' => $this->findChildren($model, $category_id),
                'state' =>  $model->id == $category_id ? ['opened' => true, 'selected' => true] : []
            ];
        }

        return $items;
    }

    protected function findChildren($model, $category_id, $mas = [])
    {
        foreach ($model->childrens as $item) {
            $items[] = [
                'text' => $item->title,
                'children' => $this->findChildren($item, $category_id),
                'id' => $item->id,
                'state' =>  $item->id == $category_id ? ['opened' => true, 'selected' => true] : []
            ];
        }
        return $items;
    }

    protected function menuGenerate($menu_id)
    {
        $items[] = ['id' => '0', 'text' => 'Всі',];
        $models = Menu::find()
            ->where(['parent' => 0])
            //->andFilterWhere(['id' => $category_id])
            ->all();
        foreach ($models as $model) {
            $items[] = [
                'id' => $model->id,
                'text' => $model->name,
                'children' => $this->findMenuChildren($model, $menu_id),
                'state' =>  $model->id == $menu_id ? ['opened' => true, 'selected' => true] : []
            ];
        }

        return $items;
    }

    protected function findMenuChildren($model, $menu_id, $mas = [])
    {
        foreach ($model->childrens as $item) {
            $items[] = [
                'text' => $item->name,
                'children' => $this->findMenuChildren($item, $menu_id),
                'id' => $item->id,
                'state' =>  $item->id == $menu_id? ['opened' => true, 'selected' => true] : []
            ];
        }
        return $items ?? [];
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

//        if(Yii::$app->request->post()){
//            pr($seo);
//        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }
}
