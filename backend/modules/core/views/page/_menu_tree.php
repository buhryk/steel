<?php
use yii\helpers\Url;
?>
<div id="using_json"></div>

<?= $this->registerJs("
var urlRedirect = '".Url::to(['/core/page/index'])."';
$('#using_json').jstree({ 'core' : {
    'data' : ".json_encode($menuTree)."
} });

$('body').on('click', '#using_json li a', function() {
     var menu_id = $(this).parent().attr('id'); 
     url = urlRedirect+ '?menu_id=' + menu_id;
     window.location.href = url;
});
", \yii\web\View::POS_READY) ?>
