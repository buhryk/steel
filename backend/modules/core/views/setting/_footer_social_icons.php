<?php
use backend\modules\core\models\Setting;
use yii\helpers\Html;
use dominus77\iconpicker\IconPicker;

$model = current($models);

$items = $model->getData(Setting::ADDITIONAL_DATA_ITEMS) ?: [];
$add = Yii::$app->request->get('add-items')
?>
<div class="col-md-7">
    <?php foreach ($items as $key => $item) : ?>
        <div class="row  item-setting">
            <div class="col-md-5">
                <label>Icon</label>

                <?=\backend\widgets\MainInputFile::widget( [
                    'name' => 'setting['.$key.'][icons]',
                    'language'   => 'ru',
                    'path'          => 'social',
                    'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                    'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                    'value'      =>  $item['icons'],
                ]) ?>
             </div>
            <div class="col-md-5">
                <label>Link</label>
                <?= \yii\helpers\Html::input('url','setting['.$key.'][link]', $item['link'], ['class' => 'form-control', 'required' => 'required']) ?>
            </div>
            <div class="col-md-1">
                <img src="<?= $item['icons'] ?>">
            </div>
            <div class="col-md-1">
                <a href="#" class="setting-delete-item" title="Delete" ><i class="glyphicon glyphicon-trash"></i> </a>
            </div>
        </div>
    <?php endforeach;?>
    <?php if ($add): ?>
        <?php $key = $key + 1; ?>
        <div class="row">
            <div class="col-md-5">
                <label>Icon</label>
                <?=\backend\widgets\MainInputFile::widget( [
                    'name' => 'setting['.$key.'][icons]',
                    'language'   => 'ru',
                    'path'          => 'social',
                    'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                    'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                    'value'      => '',
                ]) ?>
            </div>
            <div class="col-md-5">
                <label>Link</label>
                <?= \yii\helpers\Html::input('url','setting['.$key.'][link]', '', ['class' => 'form-control', 'required' => 'required']) ?>
            </div>
        </div>
    <?php else:; ?>
        <hr>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Add item', ['index', 'group' => $model->group,  'add-items' => 'colum'], ['class' => 'btn btn-success btn-sm']) ?>
    <?php endif; ?>
    <hr>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>




