<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<h3>TagManager </h3>
<div class="row">
    <?php if ($models['tag_manager_head']): ?>
        <div class="form-group">
            <label for="input02" class="col-sm-1 control-label">Head</label>
            <div class="col-sm-7">
                <?= $form->field($models['tag_manager_head'], '[tag_manager_head]value')->textarea(['rows' => 10])->label(false); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <?php if ($models['tag_manager_body']): ?>
        <div class="form-group">
            <label for="input02" class="col-sm-1 control-label">Body begin</label>
            <div class="col-sm-7">
                <?=$form->field($models['tag_manager_body'], '[tag_manager_body]value')->textarea(['rows' => 10])->label(false); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <?php if ($models['tag_manager_body_end']): ?>
        <div class="form-group">
            <label for="input02" class="col-sm-1 control-label">Body end</label>
            <div class="col-sm-7">
                <?=$form->field($models['tag_manager_body_end'], '[tag_manager_body_end]value')->textarea(['rows' => 10])->label(false); ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="form-group">
    <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class' => 'btn btn-success']) ?>
</div>
