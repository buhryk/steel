<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\modules\core\models\Setting;

$this->title = 'Edit settings';
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin(['options' => ['id'=> 'form-common-info']]);
?>
<!--    <iframe src='https://docs.google.com/viewer?url=http://calibre-ebook.com/downloads/demos/demo.docx&embedded=true' frameborder='0'></iframe>-->
<div class="row">
    <div class="col-md-10">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <?php $i = 1; foreach ($groupItem as $item): ?>
                <?php if ($i < 4): ?>
                    <li role="presentation" class="<?= $item->group == $group ? 'active' : ''; ?>">
                        <a href="<?= Url::to(['index', 'group' => $item->group]) ?>" id="home-tab" ><?= $item->group ?></a>
                    </li>
                <?php endif; ?>
            <?php $i++; endforeach; ?>
        </ul>

        <?= $this->render($template, ['form' => $form, 'models' => $models]) ?>
</div>

<?php ActiveForm::end();?>