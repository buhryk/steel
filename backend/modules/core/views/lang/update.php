<?php

use yii\helpers\Html;


$this->title = 'Обновлення мови: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Мови', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Оновити';
?>
<div class="lang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
