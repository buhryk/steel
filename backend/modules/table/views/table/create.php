<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\table\models\Table */

$this->title = Yii::t('app', 'Створити Table');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
