<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\table\models\Table */

$this->title = Yii::t('app', 'Редагувати : {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редагувати');
?>
<div class="table-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
