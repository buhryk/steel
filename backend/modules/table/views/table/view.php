<?php

use backend\modules\core\models\Page;
use backend\modules\table\models\Firm;
use backend\modules\table\models\TableType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\table\models\Table */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-view">

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i> '.Yii::t('app', 'Редагувати'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i> '.Yii::t('app', 'Видалити'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Ви впевнені, що хочете видалити цей елемент?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            [
                'attribute' => 'type_id',
                'filter' => ArrayHelper::map(TableType::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $type = $model->tableType;
                    return $type ? $type->title : '';
                }
            ],
            'cut',
            'mark',
            'standard',
            [
                'attribute' => 'firm_id',
                'filter' => ArrayHelper::map(Firm::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $firm = $model->firm;
                    return $firm ? $firm->title : '';
                }
            ],
            [
                'attribute' => 'page_id',
                'filter' => ArrayHelper::map(Page::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $page = $model->page;
                    return $page ? $page->title : '';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusDetail;
                }
            ],
            'updated_at:date',
        ],
    ]) ?>

</div>
