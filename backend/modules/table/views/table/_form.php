<?php

use backend\modules\core\models\Page;
use backend\modules\table\models\Firm;
use backend\modules\table\models\TableType;
use backend\widgets\ImperaviWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\table\models\Table */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'cut')->textInput() ?>
    <?= $form->field($model, 'mark')->textInput() ?>
    <?= $form->field($model, 'standard')->textInput() ?>
    <?=  $form->field($model, 'firm_id')->widget(Select2::classname(), [
        'data' => Firm::getFirmAll(true),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?=  $form->field($model, 'type_id')->widget(Select2::classname(), [
        'data' => TableType::getTypeAll(true),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?=  $form->field($model, 'page_id')->widget(Select2::classname(), [
        'data' => Page::getPageAll(true),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> '.Yii::t('app', 'Зберегти'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
