<?php

use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\CatalogCategory;
use yii\helpers\ArrayHelper;

$parentsList = CatalogCategory::find()
    ->where(['status' => CatalogCategory::STATUS_ACTIVE])
    ->andFilterWhere(['!=', 'id', $model->id])
    ->orderBy(['position' => SORT_ASC])
    ->all();
?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'title')->widget(ImperaviWidget::className(), [])?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>


    <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
