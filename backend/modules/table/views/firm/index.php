<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\modules\table\models\TableType as CurrentModel;
use backend\widgets\SortActionWidget;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\core\models\PageCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фирмы';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => CurrentModel::className()]);
?>
<div class="page-category-index">
    <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>


    <?php Pjax::begin(['id' => 'content-list']) ?>

    <div id="myTabContent" class="tab-content">
        <div class="col-md-12 grid-view">
            <table class="table table-custom dataTable no-footer ">
                <thead>
                <tr>
                    <th></th>
                    <th>Название</th>
                    <th>Статус</th>

                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($models as $model): ?>
                    <tr>
                        <td class="checkbox-item" style="width: 10px;" >
                            <input type="checkbox" name="selection[]" value="<?=$model->id ?>">
                        </td>
                        <td  style="padding-left: <?= $level ? $level * 25 : 10 ?>px" class="sort-item">
                            <i class="fa fa-arrows-alt"></i> <?=$model->title ?>
                        </td>
                        <td>
                            <?=$model->statusDetail ?>
                        </td>

                        <td style="">
                            <a class="btn btn-info btn-xs" href="<?=Url::to(['update', 'id' => $model->id]) ?>" title="Редагувати">
                                <i class="fa fa-pencil"></i></a>
                            <a class="btn btn-danger btn-xs" href="<?=Url::to(['delete', 'id' => $model->id]) ?>" title="Видалити" onclick="return confirm(&quot;Ви дійсно хочете видалити даний запис?&quot;)">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody
            </table>
        </div>
    </div>
    <?php Pjax::end() ?>
</div>
