<?php

namespace backend\modules\table\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "table_lang".
 *
 * @property int $record_id
 * @property string $title
 * @property string $cut
 * @property string $mark
 * @property string $standard
 * @property string $firm
 * @property string $type
 * @property int $lang_id
 *
 * @property Lang $lang
 * @property Table $record
 */
class TableLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'table_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'lang_id'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['title', 'cut', 'mark', 'standard'], 'string', 'max' => 333],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => Yii::t('app', 'Record ID'),
            'title' => Yii::t('app', 'Title'),
            'cut' => Yii::t('app', 'Cut'),
            'mark' => Yii::t('app', 'Mark'),
            'standard' => Yii::t('app', 'Standard'),
            'lang_id' => Yii::t('app', 'Lang ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(Table::className(), ['id' => 'record_id']);
    }
}
