<?php

namespace backend\modules\table\models;

use backend\modules\table\models\TableTypeLang;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page_category".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $alias
 * @property string $additional_data
 *
 * @property catalog[] $pages
 * @property catalogCategorLang[] $pageCategorLangs
 * @property Lang[] $langs
 */
class Firm extends BaseDataModel
{
    public $title;


    public static function tableName()
    {
        return 'table_firm';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => LangBehavior::className(),
                't' => new FirmLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'title' => 'Заголовок',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTable()
    {
        return $this->hasMany(Table::className(), ['type_id' => 'id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Lang::className(), ['id' => 'lang_id'])->viaTable('table_firm_lang', ['record_id' => 'id']);
    }

    public function getLang()
    {
        return $this->hasOne(FirmLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    static public function getFirmAll($map = false)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC]);

        $models = $query->all();
        if ($map) {
            return ArrayHelper::map($models, 'id', 'title');
        }
        return $models;
    }
}
