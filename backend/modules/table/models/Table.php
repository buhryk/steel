<?php

namespace backend\modules\table\models;

use backend\modules\core\models\Page;
use backend\modules\faq\models\FaqLang;
use common\behaviors\LangBehavior;
use common\behaviors\PositionBehavior;
use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "table".
 *
 * @property int $id
 * @property int $page_id
 * @property int $status
 * @property int $position
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TableLang[] $tableLangs
 * @property Lang[] $langs
 */
class Table extends \yii\db\ActiveRecord
{
    public $title;
    public $cut;
    public $mark;
    public $standard;
    public $record_id;

    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'table';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'cut', 'mark', 'standard'], 'safe'],
            [['page_id', 'status', 'position', 'created_at', 'updated_at', 'type_id','firm_id'], 'integer'],
        ];
    }

    public function behaviors()
    {
        return [
            PositionBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => LangBehavior::className(),
                't' => new TableLang(),
                'fk' => 'record_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_id' => Yii::t('app', 'Page ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'status' => Yii::t('app', 'Status'),
            'position' => Yii::t('app', 'Position'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTableLangs()
    {
        return $this->hasMany(TableLang::className(), ['record_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(TableLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    public function getTableType()
    {
        return $this->hasOne(TableType::className(), ['id' => 'type_id']);
    }

    public function getFirm()
    {
        return $this->hasOne(Firm::className(), ['id' => 'firm_id']);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }


    static public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_NOTACTIVE => 'Неактивна'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }
}
