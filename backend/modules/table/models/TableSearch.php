<?php

namespace backend\modules\table\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\table\models\Table;

/**
 * TableSearch represents the model behind the search form of `backend\modules\table\models\Table`.
 */
class TableSearch extends Table
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'status', 'type_id', 'firm_id'], 'integer'],
            [[  'title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = false)
    {
        $query = Table::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                    'position' => SORT_ASC
                    ]
                ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->title) {
            $query->joinWith('lang');
            $query->andWhere(['like', 'title', $this->title]);
        }

        if ($id) {
            $query->andFilterWhere(['page_id' => $id]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->type_id,
            'firm_id' => $this->firm_id,
            'status' => $this->status,
            'position' => $this->position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
