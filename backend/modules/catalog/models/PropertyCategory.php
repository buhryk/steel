<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "property_category".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $category_id
 * @property integer $position
 * @property integer $add_to_filter
 *
 * @property Category $category
 */
class PropertyCategory extends \yii\db\ActiveRecord
{
    const FILTER_STATUS_ADD = 2;
    const FILTER_STATUS_REMOTE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'category_id'], 'required'],
            [['property_id', 'category_id', 'position', 'add_to_filter'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            ['category_id', 'validateCategory']
        ];
    }

    public function validateCategory($attribute, $params)
    {
        $model = self::find()
            ->andWhere(['property_id' => $this->property_id])
            ->andWhere(['category_id' => $this->category_id])
            ->andFilterWhere(['!=', 'id', $this->id])
            ->exists();

        if ($model) {
            $this->addError($attribute, 'Категория уже указана');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'PropertySoap ID',
            'category_id' => 'CategorySoap ID',
            'position' => 'Position',
            'add_to_filter' => 'Add To Filter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    public function getFilterStatusList()
    {
        return [
            self::FILTER_STATUS_ADD => 'Выводить в фильтре',
            self::FILTER_STATUS_REMOTE => 'Не выводить в фильтре',
        ];
    }

    public function getCurrentFilterStatus()
    {
        return isset($this->FilterStatusList[$this->add_to_filter]) ? $this->FilterStatusList[$this->add_to_filter] : '';
    }
}
