<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "catalog_propery".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $catalog_id
 * @property integer $property_data_id
 * @property string $slug
 *
 * @property catalog $catalog
 * @property PropertyData $propertyData
 * @property Property $property
 */
class CatalogToCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_to_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'catalog_id'], 'required'],
            [['category_id', 'catalog_id'], 'integer'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'catalog_id' => 'catalogSoap ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(catalog::className(), ['id' => 'catalog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CatalogCategory::className(), ['id' => 'category_id']);
    }
}
