<?php
namespace backend\modules\catalog\models;

use yii\base\Component;

class PropertyBilder extends Component
{
    public $catalog;
    public $propertyData;

    public function setProperty()
    {
        $propertyData = $this->propertyData;
        $catalog = $this->catalog;

        $productProperty = CatalogPropery::find()->andWhere(['property_data_id' => $propertyData->id , 'catalog_id' => $catalog->id])->one();
        if (!$productProperty) {
            $model = new CatalogPropery();
            $model->catalog_id = $catalog->id;
            $model->property_data_id = $propertyData->id;
            $model->property_id = $propertyData->property_id;
            $model->slug = $propertyData->slug;
            $model->save();
        }
    }
}