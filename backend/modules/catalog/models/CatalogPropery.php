<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "catalog_propery".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $catalog_id
 * @property integer $property_data_id
 * @property string $slug
 *
 * @property catalog $catalog
 * @property PropertyData $propertyData
 * @property Property $property
 */
class CatalogPropery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_propery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'catalog_id', 'property_data_id'], 'required'],
            [['property_id', 'catalog_id', 'property_data_id'], 'integer'],
            [['slug'], 'string', 'max' => 255],
            [['catalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['catalog_id' => 'id']],
            [['property_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyData::className(), 'targetAttribute' => ['property_data_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'PropertySoap ID',
            'catalog_id' => 'catalogSoap ID',
            'property_data_id' => 'PropertySoap Data ID',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(catalog::className(), ['id' => 'catalog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyData()
    {
        return $this->hasOne(PropertyData::className(), ['id' => 'property_data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
