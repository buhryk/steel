<?php

use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\CatalogCategory;
use yii\helpers\ArrayHelper;

$parentsList = CatalogCategory::find()
    ->where(['status' => CatalogCategory::STATUS_ACTIVE])
    ->andFilterWhere(['!=', 'id', $model->id])
    ->orderBy(['position' => SORT_ASC])
    ->all();
?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'title')->widget(ImperaviWidget::className(), [])?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), []) ?>

<!--    --><?//= $form->field($model, 'text')->textarea() ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'parent_id')
                ->dropDownList(ArrayHelper::map($parentsList, 'id', 'title'), ['prompt' => '']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'image')->widget(MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'catalog',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
