<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\catalog\models\CatalogCategory as CurrentModel;
use backend\widgets\SortActionWidget;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\core\models\PageCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => CurrentModel::className()]);
?>
<div class="page-category-index">
    <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>

    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => CurrentModel::className()]) ?>
    </div>

    <?php Pjax::begin(['id' => 'content-list']) ?>

    <div id="myTabContent" class="tab-content">
        <div class="col-md-12 grid-view">
            <table class="table table-custom dataTable no-footer ">
                <thead>
                <tr>
                    <th></th>
                    <th>Назва</th>
                    <th>Статус</th>
                    <th>Редаговано</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($models as $model): ?>
                    <?=$this->render('_view', ['model' => $model, 'level' => 0]) ?>
                <?php endforeach; ?>
                </tbody
            </table>
        </div>
    </div>
    <?php Pjax::end() ?>
</div>
