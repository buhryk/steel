<?php
use backend\widgets\ImagesWidget;

$this->title = 'Редактирование изображений';
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => 'Запись #' . $model->primaryKey, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= ImagesWidget::widget([
        'model' => $model,
        'parameters' => [
            'table_name' => $model::tableName()
        ]
    ]); ?>
</div>