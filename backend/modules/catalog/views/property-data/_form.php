<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\Property;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\PropertyData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'property_id')->dropDownList(Property::getPropertyAll(true)) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelLang, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
