<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Property */

$this->title = 'Редактирование: ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Свойства', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="property-update">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>
