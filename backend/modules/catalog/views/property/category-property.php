<?php
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\Category;
use yii\helpers\ArrayHelper;

$categoryList = Category::find()->joinWith('lang')->all();
?>
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3><?=$model->isNewRecord ? 'Добавление' : 'Редактирование' ?></h3>
        </div>
        <div class="modal-body">
            <?php $form = ActiveForm::begin(['id' => 'form-property-category']) ?>
            <?= $form->field($model, 'category_id')
                ->dropDownList(ArrayHelper::map($categoryList, 'id', 'title')); ?>
            <?=$form->field($model, 'add_to_filter')->dropDownList($model->filterStatusList) ?>
            <?=$form->field($model, 'position')->textInput() ?>
            <div class="form-group">
                <button class="btn btn-success">
                    Сохранить
                </button>
            </div>
            <?php ActiveForm::end() ?>
        </div>

    </div>
</div>
