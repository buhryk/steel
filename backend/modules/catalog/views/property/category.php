<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Property */

$this->title = 'Связь с категориями: ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Свойства', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Связь с категориями';

$propertyCategory = $model->propertyCategory;
?>
<div class="property-update">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>
   <div class="content">
       <div class="">
           <a class="btn modalButton btn-success" href="<?=Url::to(['category-create', 'property_id' => $model->id]) ?>">
               Добавить
           </a>

           <a class="btn  btn-success" href="<?=Url::to(['category-create-all', 'property_id' => $model->id]) ?>">
               Добавить во все категории
           </a>

           <a class="btn  btn-danger" href="<?=Url::to(['category-delete', 'property_id' => $model->id]) ?>">
               Удалить из всех категории
           </a>
       </div>
       <table class="table">
           <thead>
                <tr>
                    <td></td>
                    <td><b>Категория</b></td>
                    <td><b>Добавлять в фильтр</b></td>
                </tr>
           </thead>
           <tbody>
               <?php foreach ($propertyCategory as $item): ?>
                   <tr>
                       <td><?=$item->id ?></td>
                       <td><?=$item->category->title ?></td>
                       <td><?=$item->currentFilterStatus ?></td>
                       <td>
                           <a class="btn btn-info btn-xs modalButton"
                              href="<?=Url::to(['category-update', 'id' => $item->id]) ?>"
                              title="Редактировать"><i class="fa fa-pencil"></i>
                           </a>
                           <a class="btn btn-danger btn-xs"
                              href="<?=Url::to(['category-delete', 'id' => $item->id]) ?>"
                              title="Удалить"
                              onclick="return confirm(&quot;Вы уверены, что хотите удалить данную запись?&quot;)">
                               <i class="fa fa-trash-o"></i>
                           </a>
                       </td>
                   </tr>
               <?php endforeach; ?>
           </tbody>
       </table>
   </div>
</div>
<?php
    $this->registerJs("
        Form.init('form-property-category',{})
    ");
 ?>