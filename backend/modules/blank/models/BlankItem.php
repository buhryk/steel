<?php

namespace backend\modules\blank\models;

use Yii;
use common\models\Lang;
/**
 * This is the model class for table "blank_item".
 *
 * @property integer $id
 * @property string $title
 * @property string $file
 * @property integer $blank_id
 * @property integer $lang_id
 *
 * @property Lang $lang
 * @property Blank $blank
 */
class BlankItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blank_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'file', 'blank_id', 'lang_id'], 'required'],
            [['blank_id', 'lang_id'], 'integer'],
            [['title', 'file'], 'string', 'max' => 300],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['blank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blank::className(), 'targetAttribute' => ['blank_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Назва',
            'file' => 'Файл',
            'blank_id' => 'Blank ID',
            'lang_id' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlank()
    {
        return $this->hasOne(Blank::className(), ['id' => 'blank_id']);
    }
}
