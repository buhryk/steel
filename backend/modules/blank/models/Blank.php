<?php

namespace backend\modules\blank\models;

use common\models\BaseDataModel;
use common\models\Lang;
use Yii;
use backend\modules\core\models\Menu;
use yii\db\Query;

/**
 * This is the model class for table "blank".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $name
 * @property integer $menu_id
 * @property string $additional_data
 *
 * @property Menu $menu
 * @property BlankItem[] $blankItems
 */
class Blank extends BaseDataModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['menu_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'name' => 'Назва',
            'menu_id' => 'Меню',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(BlankItem::className(), ['blank_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->setMenuCurrentUrl();
    }


    public function setMenuCurrentUrl()
    {
        $menu = $this->menu;
        $menu->url = '/blank/'.$this->id;
        $menu->save();
    }

    public function getLettersItem()
    {
        $connection = Yii::$app->db;

        $model = $connection->createCommand('select *, LEFT(title, 1) as letter   from `blank_item` 
                              WHERE blank_id = ' . $this->id. '
                              and lang_id = ' . Lang::getCurrent()->id . '
                              Group By LEFT(title, 1)
                                ORDER BY LEFT(title, 1) COLLATE  utf8_unicode_ci'
        );

        return $model->queryAll();
    }

    public function getItemByLetter($letter)
    {
        $connection = Yii::$app->db;

        $model = $connection->createCommand('select *  from `blank_item` 
                              WHERE blank_id = ' . $this->id. '
                              and lang_id = ' . Lang::getCurrent()->id . '
                              and  LEFT(title, 1) = "'.$letter.'"
                                ORDER BY `title` COLLATE  utf8_unicode_ci'
        );

        $blanks = [];
        foreach ($model->queryAll() as $item){
            $k = str_replace("'","",mb_strtolower($item['title']));
            $k = str_replace("і","й",$k);
            $k = str_replace("ї","й",$k);
            $blanks[$k] = $item;
        }

        ksort($blanks);
                          
        return $blanks;
    }

}
