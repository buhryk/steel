<?php

namespace backend\modules\blank\controllers;

use backend\controllers\BaseController;
use backend\modules\blank\models\BlankItem;
use common\models\Lang;
use Yii;
use backend\modules\blank\models\Blank;
use backend\modules\blank\models\BlankSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * DefaultController implements the CRUD actions for Blank model.
 */
class DefaultController extends BaseController
{


    /**
     * Lists all Blank models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlankSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blank model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $itemModel = new BlankItem();
        $itemModel->blank_id = $id;
        $itemModel->lang_id = Lang::getCurrent()->id;

        if ($itemModel->load(Yii::$app->request->post()) && $itemModel->save()) {
            $itemModel = new BlankItem();
        }
        $query = BlankItem::find()
            ->where(['blank_id' => $id])
            ->andWhere(['lang_id' => Lang::getCurrent()->id])
            ->orderBy(['title' => SORT_ASC]);

        $pagination = new Pagination([
            'defaultPageSize' => 50,
            'totalCount' => $query->count(),
        ]);

        $items = $query->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();

        return $this->render('view', [
            'model' => $model,
            'itemModel' => $itemModel,
            'pagination' => $pagination,
            'items' => $items
        ]);
    }

    public function actionItemUpdate($id)
    {
        $model = BlankItem::findOne($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->save()) {
                $data['status'] = true;
                $data['message'] = 'Збережено';
            } else {
                $data['errors'] = ActiveForm::validate($model);
            }
            return $data;
        }
    }

    /**
     * Creates a new Blank model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blank();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blank model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Blank model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blank model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blank the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blank::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
