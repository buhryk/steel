<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blank\models\Blank */

$this->title = 'Редагування: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Бланки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="blank-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
