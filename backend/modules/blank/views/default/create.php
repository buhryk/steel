<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\blank\models\Blank */

$this->title = 'Створення';
$this->params['breadcrumbs'][] = ['label' => 'Бланки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blank-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
