<?php
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

$formId = 'form-item-' . $item->id;
?>
<?php $form = ActiveForm::begin(['id' => $formId, 'action' => ['item-update', 'id' => $item->id]]); ?>
<tr>
    <td class="checkbox-item" style="width: 10px;" >
        <input type="checkbox" name="selection[]" value="<?=$item->id ?>">
    </td>
    <td width="35%"><?= $form->field($item, 'file')->widget(InputFile::className(), [
            'language'      => 'ru',
            'controller'    => 'elfinder',
            'path'          => $filePath,
            'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple'      => false
        ])->label(false); ?>
    </td>
    <td width="50"><a href="<?= $item->file ?>" download> Скачати</a> </td>
    <td width="35%"><?= $form->field($item, 'title')->label(false); ?></td>
    <td><button class="btn btn-primary">Зберегти</button></td>
</tr>
<?php ActiveForm::end() ?>

<?= $this->registerJs("
    Form.init('".$formId."', {})
", \yii\web\View::POS_READY) ?>