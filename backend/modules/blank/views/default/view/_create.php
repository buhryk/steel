<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use mihaildev\elfinder\InputFile;

?>

<h2>Новий файл</h2>
<table class="table table-striped">
    <thead>
    <?php $form = ActiveForm::begin([]); ?>
    <tr>
        <th>
            <?= $form->field($itemModel, 'file')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
                'path'          => $filePath,
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ]); ?>
        </th>
        <th><?= $form->field($itemModel, 'title'); ?></th>
        <th>
            <div class="form-group" style="text-align: right;">
                <?= Html::submitButton('Добавити', ['class' => 'btn btn-primary',]); ?>
            </div>
        </th>
    </tr>
    <?php ActiveForm::end(); ?>
    </thead>
</table>
