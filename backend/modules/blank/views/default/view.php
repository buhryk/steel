<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use mihaildev\elfinder\InputFile;
use yii\widgets\Pjax;
use backend\modules\blank\models\BlankItem as CurrentModel;
use yii\widgets\LinkPager;

$this->title = 'Файли: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Бланки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$filePath = 'blank/' . $model->id;
?>
<div class="blank-view">

    <?= $this->render('view/_create', ['itemModel' => $itemModel, 'filePath' => $filePath]); ?>

    <h2>Додані файли</h2>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'className' => CurrentModel::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <div id="myTabContent" class="grid-view">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" class="select-all" name="selection_all" value="1">
                </th>
                <th width="300">Файл</th>
                <th>&nbsp;</th>
                <th>Назва</th>
                <th width="135"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($items as $item): ?>
               <?= $this->render('view/_item', ['item' => $item, 'filePath' => $filePath]) ?>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
    <?php Pjax::end() ?>
</div>
