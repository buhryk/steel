<?php

namespace backend\modules\seo\models;

use Yii;

class SeoLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'seo_lang';
    }

    public function rules()
    {
        return [
            [['seo_id', 'lang'], 'required'],
            [['seo_id'], 'integer'],
            [['meta_keywords', 'meta_description'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['meta_title', 'og_title'], 'string', 'max' => 255],
            [['og_description'], 'string', 'max' => 500],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seo_id' => 'Seo',
            'lang' => 'Язык',
            'meta_title' => 'Meta title',
            'og_title' => 'Og title',
            'og_description' => 'Og description',
            'meta_keywords' => 'Meta keywords',
            'meta_description' => 'Meta description',
        ];
    }

    public function getModel()
    {
        return $this->hasOne(Seo::className(), ['id' => 'seo_id']);
    }
}