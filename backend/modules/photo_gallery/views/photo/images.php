<?php
use backend\widgets\ImagesWidget;

$this->title = 'Редагування фотогалереї';
$this->params['breadcrumbs'][] = ['label' => 'Фотогалерея', 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => 'Фотогалерея #' . $model->primaryKey, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= ImagesWidget::widget([
        'model' => $model,
        'parameters' => [
            'table_name' => $model::tableName()
        ]
    ]); ?>
</div>