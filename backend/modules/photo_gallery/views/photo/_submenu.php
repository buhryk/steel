<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
?>

<h1><?= Html::encode($this->title) ?></h1>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            Редагування
        </a>
    </li>
    <li <?= ($action === 'images') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['images', 'id' => $model->primaryKey]) ?>">
            Редагування зображень
        </a>
    </li>
</ul>
<br>
