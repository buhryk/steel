<?php

namespace backend\modules\consumer;

use backend\modules\accesscontrol\AccessControlFilter;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\consumer\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
}
