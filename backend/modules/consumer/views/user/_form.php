<?php

use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\UserExpert;
use app\models\Subdivision;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <div class="col-md-8">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), [])?>

        <?= $form->field($model, 'line_of_activity_name')->textInput(['maxlength' => true])?>

        <?= $form->field($model, 'line_of_activity_description')->widget(ImperaviWidget::className(), [])?>

        <?= $form->field($model, 'contact')->widget(ImperaviWidget::className(), [])?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>



        <?= $form->field($model, 'status')->dropDownList($model::getStatusAll()) ?>

        <?= $form->field($model, 'image')->widget(MainInputFile::className(), [
            'language'      => 'ru',
            'path'          => 'company',
            'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple'      => false       // возможность выбора нескольких файлов
        ])?>



        <div class="form-group">
            <?= Html::submitButton('Зберегти', ['class' =>  'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h3>Змінити пароль</h3>
        </div>
        <?php $form = ActiveForm::begin([ 'options' => ['class'=>'form-horizontal']]); ?>

        <?= $form->field($modelChange, 'newPassword')->passwordInput(); ?>
        <?= $form->field($modelChange, 'newPasswordRepeat')->passwordInput(); ?>

        <div class="form-group">
            <?= Html::submitButton('Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


</div>
