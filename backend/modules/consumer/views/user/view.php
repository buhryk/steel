<?php
use yii\helpers\Html;

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Список пользователей', 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="user-view">
    <?= Html::a('<i class="fa fa-pencil"></i> Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <hr>

    <div class="row">
        <div class="col-lg-6">
            <h3>Основная информация</h3>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                <tr>
                    <th width="200px">ID</th>
                    <td><?= $model->primaryKey; ?></td>
                </tr>


                <tr>
                    <th><?= $model->getAttributeLabel('name'); ?></th>
                    <td><?= $model->name; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('email'); ?></th>
                    <td><?= $model->email; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('description'); ?></th>
                    <td><?= $model->description; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('image'); ?></th>
                    <td><img src="<?= $model->image; ?>" style="height: 100px"></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('line_of_activity_name'); ?></th>
                    <td><?= $model->line_of_activity_name; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('line_of_activity_description'); ?></th>
                    <td><?= $model->line_of_activity_description; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('contact'); ?></th>
                    <td><?= $model->contact; ?></td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>
</div>