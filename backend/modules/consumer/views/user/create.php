<?php

use yii\helpers\Html;



$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];

$this->params['breadcrumbs'][] = 'Создание';
?>
<div class="user-update">


    <?= $this->render('_form', [
        'model' => $model,
        'modelChange'=>$modelChange,
    ]) ?>

</div>
