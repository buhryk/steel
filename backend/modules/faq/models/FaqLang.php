<?php

namespace backend\modules\faq\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "{{%faq_lang}}".
 *
 * @property int $id
 * @property int $faq_id
 * @property string $title
 * @property string $text
 * @property int $lang_id
 */
class FaqLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'lang_id'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 333],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'record_id' => Yii::t('app', 'Faq ID'),
            'title' => Yii::t('app', 'Вопрос'),
            'text' => Yii::t('app', 'Ответ'),
            'lang_id' => Yii::t('app', 'Lang ID'),
        ];
    }
}
