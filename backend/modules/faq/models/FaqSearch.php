<?php

namespace backend\modules\faq\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\faq\models\Faq;

/**
 * FaqSearch represents the model behind the search form of `backend\modules\faq\models\Faq`.
 */
class FaqSearch extends Faq
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'position', 'page_id'], 'integer'],
            ['title', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faq::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                    'position' => SORT_ASC
                    ]
                ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->title) {
            $query->joinWith('lang');
            $query->andWhere(['like', 'title', $this->title]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page,
            'status' => $this->status,
            'position' => $this->position,
        ]);

        return $dataProvider;
    }
}
