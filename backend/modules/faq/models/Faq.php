<?php

namespace backend\modules\faq\models;

use backend\modules\core\models\Page;
use common\behaviors\LangBehavior;
use common\behaviors\PositionBehavior;
use common\models\BaseDataModel;
use common\models\Lang;
use Yii;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%faq}}".
 *
 * @property int $id
 * @property int $status
 * @property int $position
 */
class Faq extends ActiveRecord
{
    public $title;
    public $text;
    public $record_id;

    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
       return [
            [['title', 'text'], 'safe'],
            [['position', 'status', 'page_id'], 'integer'],
        ];
    }

    public function behaviors()
    {
       return [
            PositionBehavior::className(),
            [
                'class' => LangBehavior::className(),
                't' => new FaqLang(),
                'fk' => 'record_id',
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Статус'),
            'position' => Yii::t('app', 'Position'),
            'title' => Yii::t('app', 'Вопрос'),
            'text' => Yii::t('app', 'Ответ'),
        ];
    }

    public function getLang()
    {
        return $this->hasOne(FaqLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getText()
    {
        return $this->lang ? $this->lang->text : '';
    }

    static public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_NOTACTIVE => 'Неактивна'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

}
