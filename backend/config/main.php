<?php
use common\models\User;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'uk-UK',
    'modules' => [
        'core' => [
            'class' => 'backend\modules\core\Module',
        ],
        'request' => [
            'class' => 'backend\modules\request\Module',
        ],
        'news' => [
            'class' => 'backend\modules\news\Module',
        ],
        'tenders' => [
            'class' => 'backend\modules\tenders\Module',
        ],
        'committee' => [
            'class' => 'backend\modules\committee\Module',
        ],
        'consumer' => [
            'class' => 'backend\modules\consumer\Module',
        ],
        'catalog' => [
            'class' => 'backend\modules\catalog\Module',
        ],
        'blank' => [
            'class' => 'backend\modules\blank\Module',
        ],
        'publication' => [
            'class' => 'backend\modules\publication\Module',
        ],
        'images' => [
            'class' => 'backend\modules\images\Images',
        ],
        'faq' => [
            'class' => 'backend\modules\faq\Module',
        ],
        'table' => [
            'class' => 'backend\modules\table\Module',
        ],
        'slider' => [
            'class' => 'backend\modules\slider\Module',
        ],
        'section' => [
            'class' => 'backend\modules\section\Module',
        ],
        'translate' => [
            'class' => 'backend\modules\translate\Module',
        ],
        'photo-gallery' => [
            'class' => 'backend\modules\photo_gallery\Module',
        ],
        'video-gallery' => [
            'class' => 'backend\modules\video_gallery\Module',
        ],
        'gallery' => [
            'class' => 'backend\modules\gallery\Module',
        ],
        'user' => [
            'class' => 'backend\modules\user\Module',
        ],
        'email-delivery' => [
            'class' => 'backend\modules\email_delivery\Module',
        ],
        'accesscontrol' => [
            'class' => 'backend\modules\accesscontrol\AccessControl',
        ],
        'adminuser' => [
            'class' => 'backend\modules\adminuser\Module',
        ],
        'event' => [
            'class' => 'backend\modules\event\Event',
        ],
        'feedback' => [
            'class' => 'alpiiscky\feedback\FeedbackModule',
//            'layout' => '@app/views/layouts/main',
        ],
        'custom_form' => [
            'class' => 'backend\modules\custom_form\Module',
        ],

    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl' => '/uploads',
                'basePath' => '@frontend/web/uploads',
                //'path' => '/uploads/images/news/',
                'name' =>'Files'
            ],
        ],
    ],
    'components' => [
        'formatter' => [
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'H:i:s',
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
            'class' => 'backend\components\LangRequest',
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'zsdrgdhf346t4ey5',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
//            'class' => 'webvimark\modules\UserManagement\components\UserConfig',
//
//            // Comment this if you don't want to record user logins
//            'on afterLogin' => function($event) {
//                \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
//            }
        ],

        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl' => '/uploads',
                'basePath' => '@frontend/web/uploads',
                //'path' => '/uploads/images/news/',
                'name' =>'Files'
            ],
        ],
    ],
    'on beforeAction' => function(){
        if(!Yii::$app->user->isGuest){
            User::updateAll(['last_visit' => time()], ['id' => Yii::$app->user->id]);
        }
    },
    'params' => $params,
];



//$config['modules']['gii'] = [
//    'class' => 'yii\gii\Module',
//    'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
//    'generators' => [ //here
//        'crud' => [ // generator name
//            'class' => 'yii\gii\generators\crud\Generator', // generator class
//            'templates' => [ //setting for out templates
//                'myCrud' => '@common/templates/gii/crud/default', // template name => path to template
//            ]
//        ]
//    ],
//];
