var Form = {
    status:true,
    modalForm:true,
    init:function (frmaId, params) {
        var $this = this;
        $('body').on('submit','form#'+frmaId, function (event) {
            event.preventDefault();
            var url = $(this).attr('action'),
                data = new FormData($(this).get(0));

            $.ajax({
                method: "POST",
                url: url,
                async: true,
                processData: false, // Не обрабатываем файлы (Don't process the files)
                contentType: false,
                data: data,
                dataType: "json",
                success: function (data) {
                    if (data['status']) {
                        if (params.done) {
                            params.done(data);
                        }
                        if (data.message) {
                            toastr['success'](data.message)
                        }
                    } else {
                        $.each(data.errors, function (index, value) {
                            var element = $('#' + index);
                            element.parents('.form-group').removeClass('has-success').addClass('has-error');
                            element.next('.help-block').text(value);
                        });
                    }
                }
            });
        });
    }
}
