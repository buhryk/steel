$(document).ready(function () {
    $('.check-all').on('click', function () {
        var checkBoxes = $('input[type=checkbox]');
        $.each(checkBoxes, function() {
            $(this).prop('checked', true);
        });
    });

    $('.uncheck-all').on('click', function () {
        var checkBoxes = $('input[type=checkbox]');
        $.each(checkBoxes, function() {
            $(this).prop('checked', false);
        });
    });

    $('#submit-admin-menu-form').on('click', function () {
        var form = $('#admin-menu-form');
        console.log('err');
        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serializeArray(),

            success: function(data) {
                if (data.status) {
                    location.reload();
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
});
