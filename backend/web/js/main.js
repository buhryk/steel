/**
 * Created by Borys on 11.03.2017.
 */
$( document ).ready(function() {

    $('body').on('click', '.select-all', function () {
        var checkbox = $('.checkbox-item input[type = checkbox]');
        if ($(this).prop('checked') == true) {
            checkbox.prop('checked', true)
        } else {
            checkbox.prop('checked', false)
        }
    });
});

$( document ).ready(function() {

    $('body').on('click', '.select-all', function () {
        var checkbox = $('.checkbox-item input[type = checkbox]');
        if ($(this).prop('checked') == true) {
            checkbox.prop('checked', true)
        } else {
            checkbox.prop('checked', false)
        }
    });

    $('body').on('click', '.setting-delete-item', function () {
        $(this).parents('.item-setting').remove();
    })

    $('body').on('click', '.widget-delete-item', function () {
        $(this).parents('tr').remove();
    })
});

/*
 Custome validation form in modal
 */
function valiadModalForm(formId, params) {
    $('#'+formId).on('beforeSubmit', function () {
        var $yiiform = $(this);
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray(),
            }
        )
            .done(function(data) {
                if (data.message) {
                    toastr['success'](data['message']);
                }

                if(data.success) {
                    console.log(data);
                    if (data.reload) {
                        location.reload();
                    }else if (data.redirectUrl) {
                        window.location.href = data.redirectUrl;
                    }else if (data.modalUrl) {
                        openModalByUrl(data.modalUrl)
                    }else if (data.pjaxReload) {
                        $.pjax.reload({container: "#content-list"});
                        $('#main-modal').modal('hide');
                    }
                } else if (data.validation) {
                    if (params && params.validation) {
                        params.validation(data.validation);
                    }
                    $yiiform.yiiActiveForm('updateMessages', data.validation, true); // renders validation messages at appropriate places
                } else {
                    // incorrect server response
                }
            })
            .fail(function () {
                // request failed
            })

        return false; // prevent default form submission
    });
}

$('body').on('change', '.week-day input.day-includ', function () {
    var labelVal = $(this).attr('data-unchecked');
    if ($(this).prop("checked")) {
        $('#' + labelVal).prop("checked", false);
    } else {
        $('#' + labelVal).prop("checked", true);
    }
});
$('body').on('change', 'select.select-count-user', function () {
    var url = $(this).parents('form').attr('action'),
        data = $(this).parents('form').serialize() + '&not_save=1';
    $.post(url, data, function (data) {
        $('.modal-dialog').replaceWith(data);
    })
});
$('body').on('change', '.change-order', function () {
    var url = $(this).parents('form').attr('action'),
        data = $(this).parents('form').serialize() + '&not_save=1';
    $.post(url, data, function (data) {
        $('.modal-dialog').replaceWith(data);
    })
})