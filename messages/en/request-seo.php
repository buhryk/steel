<?php

return [
    'Your name' => 'Your name',
    'Your email' => 'Your email',
    'Website url' => 'Website url',
    'Success complete form message' => 'Your message was sent successfully! I will be in touch as soon as I can.',
    'Get Your Free SEOWAVE' => 'Get Your Free SEOWAVE',
    'Analysis Quote' => 'Analysis Quote',
    'Create request' => 'Create request'
];