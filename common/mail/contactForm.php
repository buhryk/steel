<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<p><strong>Від кого</strong>: <?= Html::encode($contact->name) ?> </p>
<p><strong>Email\Телефон</strong>: <?= Html::encode($contact->contactInfo) ?></p>
<p><strong>Текст повідомлення</strong>:</p>
<p><?= Html::encode($contact->message) ?>.</p>


