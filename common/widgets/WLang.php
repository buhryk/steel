<?php
namespace common\widgets;
use backend\modules\core\models\Menu;
use backend\modules\core\models\Page;
use backend\modules\core\models\PageLang;
use backend\modules\gallery\models\Gallery;
use backend\modules\gallery\models\GalleryLang;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsLang;
use common\models\Lang;
use Yii;

class WLang extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run()
    {

        return $this->render('lang/view', [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->where(
                'id != :current_id', [':current_id' => Lang::getCurrent()->id]

            )
                ->andWhere(['status' => Lang::STATUS_ACTIVE])
                ->all(),
            'true' => true
        ]);
    }
}