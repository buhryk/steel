<?php
namespace frontend\modules\gallery\controllers;

use backend\modules\core\models\PageCategory;
use backend\modules\gallery\models\Gallery;
use backend\modules\gallery\models\GalleryItem;
use yii\data\Pagination;
use yii\web\Controller;
use backend\modules\core\models\Page;
use yii\web\NotFoundHttpException;

class GalleryController extends Controller
{
    public function actionIndex($alias = 'foto-galerea-2017')
    {
        $category = $this->findCategory($alias);
        $query = Gallery::find()
            ->andWhere(['category_id' => $category->id])
            ->andWhere(['status' => Gallery::STATUS_ACTIVE])
            ->orderBy(['position' => SORT_ASC]);

        $pages = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'category' => $category
        ]);
    }


    public function actionView($alias)
    {
        $model = $this->findModel($alias);

        $query = GalleryItem::find()
            ->where(['gallery_id' => $model->id])
            ->orderBy(['position' => SORT_ASC]);

        $pages = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $modelsItem = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('view', [
            'model' => $model,
            'pages' => $pages,
            'modelsItem' => $modelsItem
        ]);
    }

    protected function findModel($alias)
    {
        if (($model = Gallery::find()->where(['alias' => $alias, 'status' => Gallery::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCategory($alias)
    {
        if (($model = PageCategory::find()->where(['alias' => $alias, 'status' => PageCategory::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}