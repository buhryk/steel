<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;


$this->title = $category->title;
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageH1'] = $this->title;
$this->params['category_id'] = $category->id;
?>

<h2 class="title"><?=$category->description ?></h2>

<?php if (isset($models)): ?>
    <div class="news">

        <?php foreach ($models as $model):?>
            <div class="news-item">
                <div class="news-item__image">
                    <?= Yii::$app->thumbnail->img($model->image, ['thumbnail' => ['width' => 250, 'height' => 250]], [
                        'alt' => $model->title,
                    ]) ?>
                </div>
                <div class="news-item__info">
                    <p class="news-item__date"><?=StringHelper::truncate($model->title, 39)?></p>
                    <p><?=StringHelper::truncate($model->description, 186)?></p>
                    <div class="news-item__links">
                        <?= Html::a( Yii::t('common', 'Переглянути'), ['view', 'alias' => $model->alias]) ?>
                    </div>
                </div>
            </div>
        <?php endforeach;?>

        <div class="pagination">
            <?php
                echo LinkPager::widget([
                    'pagination' => $pages,
                    'options' => ['class' => ''],
                    'firstPageLabel' => '&nbsp;',
                    'lastPageLabel' => '&nbsp;',
                    'prevPageLabel' => '<img src="/images/pagination-prev.png" alt="prev">',
                    'nextPageLabel' => '<img src="/images/pagination-next.png" alt="next">',
                    'activePageCssClass' => 'active',
                    'maxButtonCount' => 3,
                    'linkOptions' => ['class' => ''],
                ]);
            ?>
        </div>
    </div>
<?php endif; ?>
