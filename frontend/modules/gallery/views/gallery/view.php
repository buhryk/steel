<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use backend\modules\gallery\models\Gallery;

$this->title = $model->title;


$this->params['pageH1'] = $this->title;
$this->params['category_id'] = $category->id;
if ($model->category) {
    $this->params['breadcrumbs'][] = $model->category->parent ? $model->category->parent->title : '';
    $this->params['breadcrumbs'][] = ['label' => $model->category->title, 'url' => ['/gallery/gallery/index', 'alias' => $model->category->alias]];
    $this->params['category_id'] = $model->category->id;
    $back = ['/gallery/'.$model->category->alias];
}
$this->params['breadcrumbs'][] = $this->title;
?>

<h2 class="title"><?= $model->title ?></h2>
<?php if ($model->type == Gallery::TYPE_IMAGE) echo Html::a( Yii::t('common', 'Повернутись'), $back, ['class' => 'simple-link']) ?>
<div class="photo-gallery">

    <?php foreach ($modelsItem as $item):?>

        <?php if ($model->type == Gallery::TYPE_IMAGE): ?>
            <a href="<?=$item->value?>" data-fancybox="gallery" data-caption="<?=$item->description?>" class="photo-gallery__item">
                <?= Yii::$app->thumbnail->img($item->value, ['thumbnail' => ['width' => 250, 'height' => 250]]) ?>
            </a>
        <?php else: ?>
            <a href="<?= $item->value ?>" class="video-gallery-item" data-fancybox="video" >
                <img src="http://img.youtube.com/vi/<?= $item->getVideoKey() ?>/hqdefault.jpg" alt="video-preview">
            </a>
        <?php endif; ?>

    <?php endforeach;?>

</div>
<div class="photo-gallery__bottom">
    <?php if ($model->type == Gallery::TYPE_IMAGE) echo  Html::a( Yii::t('common', 'Повернутись'),  $back , ['class' => 'simple-link']) ?>
    <div class="pagination">
        <?php
            if($pages){
                echo LinkPager::widget([
                    'pagination' => $pages,
                    'options' => ['class' => ''],
                    'firstPageLabel' => '&nbsp;',
                    'lastPageLabel' => '&nbsp;',
                    'prevPageLabel' => '<img src="/images/pagination-prev.png" alt="prev">',
                    'nextPageLabel' => '<img src="/images/pagination-next.png" alt="next">',
                    'activePageCssClass' => 'active',
                    'maxButtonCount' => 3,
                    'linkOptions' => ['class' => ''],
                ]);
            }
        ?>
    </div>
</div>
