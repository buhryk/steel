<?php

namespace frontend\modules\gallery;

/**
 * news module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\gallery\controllers';

   public $layout = '/gallery';


    public function init()
    {
        parent::init();

    }
}
