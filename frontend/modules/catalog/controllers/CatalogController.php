<?php

namespace frontend\modules\catalog\controllers;

use backend\modules\catalog\models\Property;
use backend\modules\core\models\Page;
use backend\modules\email_delivery\models\Subscriber;
use backend\modules\images\models\Image;
use backend\modules\catalog\models\CatalogCategory;
use common\models\Lang;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use backend\modules\catalog\models\Catalog;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `catalog` module
 */
class CatalogController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public  $childrenIds = [];

    public function actionAll()
    {
        $page = Page::getPageByAlias('catalog');

//        pr($page);
        $query = $this->creationQuery();

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $model = new Subscriber();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Спасибо за подписку на новости'));
            return $this->refresh();
        } else {
            return $this->render('all', [
                'subscribe' => $model,
                'models' => $models,
                'pagination' => $pagination,
                'page' => $page
            ]);
        }

    }

    public function actionCategory($alias)
    {
        $ids = Yii::$app->request->get('categories_id');
        $letter = Yii::$app->request->get('letter');

        $check_category_id = '';

        if(!$ids) {
            if (Yii::$app->request->get('category_id')){
                $check_category_id = Yii::$app->request->get('category_id');
            }
            $category = $this->findCategory($alias);
            $this->childrenIds[] = $category->id;

            $ids =  $this->getChildrenIds($category->childrens);
        }

        $query = Catalog::find();
        $query->joinWith('categories');
        $query->andWhere(['status' => Catalog::STATUS_ACTIVE]);
        $query->andWhere(['catalog_to_category.category_id' => $ids]);

//        pr(count($query->all()));
        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pagination->offset)
        ->limit($pagination->limit)
            ->asArray()
            ->all();
//pr($models);





        $companiesQuery = Catalog::find();
        $companiesQuery->joinWith('categories');
        $companiesQuery->andWhere(['status' => Catalog::STATUS_ACTIVE]);
        $companiesQuery->andWhere(['catalog_to_category.category_id' => $ids]);
        $companies = $companiesQuery
            ->select(['company_id,COUNT(*) AS cnt'])
            ->groupBy(['company_id'])
            ->orderBy(['cnt' => SORT_DESC])
            ->asArray()
            ->all();


        $companySorted = [];
        $catalogIds = [];

        for ($i = 0; $i < count($models); $i++) {
            foreach ($companies as $k => $company) {
                foreach ($models as $record) {
                    if (!isset($companySorted[$record['id']])) {
                        if ($record['company_id'] == $company['company_id']) {
                            $catalogIds[] = $record['id'];
                            $companySorted[$record['id']] = $record;
                            break ;
                        }

                    }
                }

            }

        }

        $model = new Catalog();

        $letters = $model->getLettersItem($ids);

        if ($letter) {
            $models = $model->getItemByLetter($letter, $ids);

        }
//        pr($catalogIds);

        if (Yii::$app->request->isAjax) {
//            pr($ids);
            $html = $this->renderAjax('catalog-items', ['models' => $catalogIds, 'pagination' => $pagination]);

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'html' => $html,
            ];
        }


        return $this->render('category', [
            'models' => $catalogIds,
            'pagination' => $pagination,
            'childrens' => $category->childrens,
            'letters' => $letters,
            'check_category_id' => $check_category_id,
            'category' => $category
        ]);
    }


    public function getChildrenIds($model)
    {
        if($model){
            foreach ($model as $children){
                $this->childrenIds[] = $children->id;
                $this->getChildrenIds($children->childrens);
            }

        }

        return $this->childrenIds;
    }



    public function actionView($alias)
    {
        $model = $this->findModel($alias);

//        pr($model->property);
        $propertyList = Property::getPropertyAll();
        return $this->render('view', [
            'propertyList' => $propertyList,
            'model' => $model,
        ]);
    }

    protected function findCategory($alias)
    {
        if (($model = catalogCategory::find()->where(['alias' => $alias, 'status' => catalogCategory::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModel($alias)
    {
        if (($model = Catalog::find()->where(['alias' => $alias, 'status' => Catalog::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function creationQuery()
    {
        $query = Catalog::find();
        $query->andWhere(['status' => Catalog::STATUS_ACTIVE]);
        $query->orderBy(['id' => SORT_DESC]);

        return $query;
    }
}
