<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;use yii\widgets\LinkPager;

foreach ($models as $model):?>
    <?php
        $model = \backend\modules\catalog\models\Catalog::findOne($model);
    ?>
    <a href="<?= Url::to(['/catalog/catalog/view', 'alias' => $model->alias]) ?>" class="case-box">

        <img src="<?= $model->image ?>" alt="">
        <h3><?= StringHelper::truncate(strip_tags($model->title), 40 )?></h3>
        <p><?=date("d.m.Y",strtotime($model->data_pub))?></p>
        <p> <?= StringHelper::truncate(strip_tags($model->text), 40 )?></p>

    </a>
<?php endforeach;?>
<div class="pagination">
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
        'options' => ['class' => ''],
        'firstPageLabel' => '&nbsp;',
        'lastPageLabel' => '&nbsp;',
        'activePageCssClass' => 'active',
        'maxButtonCount' => 3,
        'linkOptions' => ['class' => ''],
    ]);
    ?>
</div>