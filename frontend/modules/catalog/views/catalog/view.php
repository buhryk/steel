<?php

use frontend\widgets\SubscribeWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;


$this->title = $model->title;

$backgroundImage = $model->image;

foreach ($model->images as $image){
    if ($image->is_main) {
        $backgroundImage = $image->path;
    }
    if ($backgroundImage) {
        break;
    }
}
?>

<main class="page-template">
    <section class="block_header long">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="<?=Url::to(['/']) ?>">Главная</a>
                <span>/</span>
                <?= Html::a($model->category->title, ['/catalog/catalog/category', 'alias' => $model->category->alias]) ?>
                <span>/</span>
                <p><?=$model->title?></p>
            </div>
        </div>
        <div class="container-100">
            <div class="text-information">
                <div class="title-block">
                    <h1><?=$model->title?></h1>
                    <p><?=$model->description?></p>
                </div>
            </div>
        </div>
    </section>
    <section class="template_4_project">
        <div class="container-100">
            <div class="left">
                <div class="gallery">
                    <?php foreach ($model->images as $image): ?>
                        <div class="img" data-src="<?= $image->path ?>">
                        <a href="##">
                            <img src="<?= $image->path ?>" alt="">
                        </a>
                    </div>
                    <?php endforeach; ?>
                    <?php
                    $count = count($model->images);
                    if ($count > 4): ?>
                        <div class="text">
                            <p>+<?= $count - 3 ?><br> фото</p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="right">
                <div class="row-blocks txt-content">
                    <?php foreach ($model->property as $item): ?>
                        <div class="box">
                            <h3><?= $item->property->title ?></h3>
                            <?php foreach ($item->property->catalogProperies as $propery_data): ?>
                                <p><?= $propery_data->propertyData->value ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
$this->registerJs(
    "$('.template_4_project .gallery').lightGallery({
        selector: '.img',
    });
    $('.template_4_project .gallery .text').click(function () {
        $('.template_4_project .gallery .img:nth-child(1)').trigger('click');
    });",
    View::POS_READY
);

if ($count == 4){
    $this->registerJs(
    "
    $('.template_4_project .gallery .img:nth-child(4)').css('display','block');
    ",
    View::POS_READY
);
}
?>


