<?php
use backend\modules\images\models\Image;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

$this->title = Html::encode($category->title);

?>

    <main class="page-template page-account">
        <section class="block_header">
            <div class="container-100">
                <div class="breadcrumbs">
                    <a href="<?=Url::to(['/']) ?>">Главная</a>
                    <span>/</span>
                    <p><?= $category->title ?></p>
                </div>
            </div>
            <div class="container-100">
                <div class="text-information">
                    <div class="title-block">
                        <h1><?= $category->title ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="block_catalog with-aside case_catalog">
            <div class="container-100">
                <aside class="acc" id="filters">
                    <?php foreach ($childrens as $children): ?>
                        <p class="acc_title"><?= Html::encode($children->title) ?></p>
                        <div class="acc_body" style="display: <?= $children->id == $check_category_id ? 'block' : '' ?>">
                            <?php foreach ($children->childrens as $item): ?>
                                <div class="check-box">
                                    <input type="checkbox" name="categories_id[]"
                                           id="cat_<?= $children->id ?>_item_<?= $item->id ?>"
                                           value="<?= $item->id ?>"
                                        <?= $children->id == $check_category_id ? 'checked' : '' ?>
                                    >
                                    <label for="cat_<?= $children->id ?>_item_<?= $item->id ?>"><?= $item->title ?></label>
                                    <?php if ($item->childrens): ?>
                                        <div class="chb-arrow-down"></div>
                                        <div class="sub-inputs-list">
                                            <?php foreach ($item->childrens as $item3): ?>
                                                <div class="check-box">
                                                    <input type="checkbox" name="categories_id[]"
                                                           id="cat_<?= $children->id ?>_item_<?= $item3->id ?>"
                                                           value="<?= $item3->id ?>"
                                                        <?= $children->id == $check_category_id ? 'checked' : '' ?>
                                                    >
                                                    <label for="cat_<?= $children->id ?>_item_<?= $item3->id ?>"><?= $item3->title ?></label>
                                                </div>
                                            <?php endforeach; ?>

                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </aside>
                <div class="right tree-cases" id="catalog-list">
                    <?= $this->render('catalog-items', ['models' => $models, 'pagination' => $pagination]) ?>
                </div>
            </div>
        </section>
        <section class="template_4_seoRight">
            <div class="container-100 txt-content">
                <?= $category->description ?>
            </div>
        </section>
    </main>

<?php
$this->registerJs(
    "categoryClick()",
    View::POS_READY,
    'my-button-handler'
);

