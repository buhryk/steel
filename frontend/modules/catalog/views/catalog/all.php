<?php

use common\models\Lang;
use frontend\widgets\SubscribeWidget;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;

$this->title = $page->title;
?>
<main class="page-template">
    <section class="block_header">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="<?=Url::to(['/']) ?>"><?= Yii::t('common', 'Главная') ?></a>
                <span>/</span>
                <p><?= $page->title ?></p>
            </div>
        </div>
        <div class="container-100">
            <div class="text-information">
                <div class="title-block">
                    <h1><?= $page->title ?></h1>
                    <?= $page->text ?>
                </div>
            </div>
            <div class="search">
                <form action="">
                    <div class="btn">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" viewBox="0 0 512 512"><path fill="white" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"/></svg>
                        <input class="btn" type="submit" value="">
                    </div>
                    <input class="input" type="text" placeholder="Поиск документа">
                </form>
                <div class="letters">
                    <a href="">А</a>
                    <a href="">Б</a>
                    <a href="">В</a>
                    <a href="">Г</a>
                    <a href="">Д</a>
                    <a href="">Е</a>
                </div>
            </div>
        </div>
    </section>

    <div class="block_catalog triple">
        <div class="container-100">
            <div class="catalog-list">

                <?= $this->render('catalog-items', ['models' => $models, 'pagination' => $pagination]) ?>

                <?= SubscribeWidget::widget(['type' => 2]) ?>
            </div>
        </div>
    </div>
</main>

