<?php

use frontend\widgets\SubscribeWidget;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = $model->title;

$backgroundImage = $model->image;

?>

<main class="page-single">
    <section class="header-single" style="background-image: url(<?= $backgroundImage ?>)">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="<?=Url::to(['/']) ?>">Главная</a>
                <span>/</span>
                <p><?=$model->title?></p>
            </div>
            <h1 class="title_1"><?=$model->title?></h1>
            <div class="row">
                <p class="date"><?=date("d.m.Y",strtotime($model->start_date))?></p>
            </div>
            <div class="description">
                <p><?=$model->short_description?></p>
            </div>
        </div>
    </section>
    <section class="content-single">
        <div class="container-990">
            <?=$model->text?>
        </div>
    </section>
    <?= SubscribeWidget::widget(['type' => 2]) ?>
</main>

