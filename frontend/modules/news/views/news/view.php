<?php

use frontend\widgets\SubscribeWidget;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = $model->title;

$backgroundImage = $model->image;

foreach ($model->images as $image){
    if ($image->is_main) {
        $backgroundImage = $image->path;
    }
    if ($backgroundImage) {
        break;
    }
}
?>

<main class="page-single">
    <section class="header-single" style="background-image: url(<?= $backgroundImage ?>)">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="<?=Url::to(['/']) ?>">Главная</a>
                <?php if ($model->category->parent): ?>
                    <span>/</span>
                    <?= Html::a($model->category->parent->title, ['/news/category/' . $model->category->parent->alias ]) ?>
                <?php endif; ?>
                <span>/</span>
                <?= Html::a($model->category->title, ['/news/news/category', 'alias' => $model->category->alias]) ?>
                <span>/</span>
                <p><?=$model->title?></p>
            </div>
            <h1 class="title_1"><?=$model->title?></h1>
            <div class="row">
                <p class="date"><?=date("d.m.Y",strtotime($model->data_pub))?></p>
                <p><?= $model->category->title ?></p>
            </div>
            <div class="description">
                <p><?=$model->description?></p>
            </div>
        </div>
    </section>
    <section class="content-single">
        <div class="container-990">
            <?=$model->text?>
        </div>
    </section>
    <?= SubscribeWidget::widget(['type' => 2]) ?>
</main>

