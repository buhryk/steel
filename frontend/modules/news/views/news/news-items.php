<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;

foreach ($models as $model):?>
    <div class="box">
        <?= Yii::$app->thumbnail->img($model->image, ['thumbnail' => ['width' => 480, 'height' => 270]], [
            'alt' => $model->title,
        ]) ?>
        <h2><?= StringHelper::truncate(strip_tags($model->title), 70 )?></h2>
        <p> <?= StringHelper::truncate(strip_tags($model->description), 250 )?></p>
        <div class="bottom"><p><?=date("d.m.Y",strtotime($model->data_pub))?></p>
            <?= Html::a(Yii::t('common', 'ПОДРОБНЕЕ'), ['/news/news/view', 'alias' => $model->alias]) ?>
        </div>
    </div>
<?php endforeach;?>
<div class="pagination">
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
        'options' => ['class' => ''],
        'firstPageLabel' => '&nbsp;',
        'lastPageLabel' => '&nbsp;',
        'activePageCssClass' => 'active',
        'maxButtonCount' => 3,
        'linkOptions' => ['class' => ''],
    ]);
    ?>
</div>