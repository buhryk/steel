<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
$this->title = 'Відеогалерея';
$this->params['breadcrumbs'][] = ['label' => 'Відеогалерея', 'url' => ['index']];

?>

<h2 class="title">Відеогалерея 2018</h2>
<?= Html::a('Повернутись', ['index'], ['class' => 'simple-link']) ?>

<?php if (isset($models)): ?>

    <div class="video-gallery">

        <?php foreach ($models as $model):?>

            <a href="https://www.youtube.com/watch?v=mQT4YWBq7OE" class="video-gallery-item" data-fancybox="video" data-caption="Участь у виставці агро 2016">
                <img src="http://img.youtube.com/vi/mQT4YWBq7OE/hqdefault.jpg" alt="video-preview">
            </a>
            <a href="https://www.youtube.com/watch?v=mQT4YWBq7OE" class="video-gallery-item" data-fancybox="video" data-caption="Участь у виставці агро 2016">
                <img src="http://img.youtube.com/vi/mQT4YWBq7OE/hqdefault.jpg" alt="video-preview">
            </a>
            <a href="https://www.youtube.com/watch?v=mQT4YWBq7OE" class="video-gallery-item" data-fancybox="video" data-caption="Участь у виставці агро 2016">
                <img src="http://img.youtube.com/vi/mQT4YWBq7OE/hqdefault.jpg" alt="video-preview">
            </a>
            <a href="https://www.youtube.com/watch?v=mQT4YWBq7OE" class="video-gallery-item" data-fancybox="video" data-caption="Участь у виставці агро 2016">
                <img src="http://img.youtube.com/vi/mQT4YWBq7OE/hqdefault.jpg" alt="video-preview">
            </a>

        <?php endforeach;?>
    </div>
    <div class="photo-gallery__bottom">
        <div class="pagination">

            <?php
            if($pagination){
                echo LinkPager::widget([
                    'pagination' => $pagination,
                    //Css option for container
                    'options' => ['class' => ''],
                    //        First option value
                    'firstPageLabel' => '&nbsp;',
                    //        Last option value
                    'lastPageLabel' => '&nbsp;',
                    //        Previous option value
                    'prevPageLabel' => '<img src="/images/pagination-prev.png" alt="prev">',
                    //Next option value
                    'nextPageLabel' => '<img src="/images/pagination-next.png" alt="next">',
                    //        //Current Active option value
                    'activePageCssClass' => 'active',
                    //        //Max count of allowed options
                    'maxButtonCount' => 3,
                    // Css for each options. Links
                    'linkOptions' => ['class' => ''],
                ]);
            }
            ?>

        </div>
        <?= Html::a('Повернутись', ['index'], ['class' => 'simple-link']) ?>
    </div>


<?php endif;?>