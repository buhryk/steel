<?php

namespace frontend\modules\video\controllers;

use backend\modules\images\models\Image;
use yii\data\Pagination;
use yii\web\Controller;
use backend\modules\video_gallery\models\Video;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `news` module
 */
class VideoController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $query = Video::find();

        $pagination = new Pagination([
            'defaultPageSize' => 1,
            'totalCount' => $query->count(),
        ]);

        $model = $query->orderBy(['id' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
           'models' => $model,
            'pagination' => $pagination
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
