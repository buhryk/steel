<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
?>
<h2 class="title">Фотогалерея 2017</h2>

<?php if (isset($models)): ?>
    <div class="news">
        <?php foreach ($models as $model):?>
            <div class="news-item">
                <div class="news-item__image">
                    <img src="<?=$model->image?>" alt="<?=$model->title?>">
                </div>
                <div class="news-item__info">
                    <p class="news-item__date"><h3><?=StringHelper::truncate($model->title, 39)?></h3></p>
                    <p><?=StringHelper::truncate($model->description, 186)?></p>
                    <div class="news-item__links">
                        <?= Html::a('Переглянути', ['view', 'id' => $model->id]) ?>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
        <div class="pagination">
            <?php
                 echo LinkPager::widget([
                        'pagination' => $pagination,
                        'options' => ['class' => ''],
                        'firstPageLabel' => '&nbsp;',
                        'lastPageLabel' => '&nbsp;',
                        'prevPageLabel' => '<img src="/images/pagination-prev.png" alt="prev">',
                        'nextPageLabel' => '<img src="/images/pagination-next.png" alt="next">',
                        'activePageCssClass' => 'active',
                        'maxButtonCount' => 3,
                        'linkOptions' => ['class' => ''],
                    ]);
            ?>
        </div>
    </div>
<?php endif; ?>