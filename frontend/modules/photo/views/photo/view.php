<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>

<h2 class="title">Назва папки</h2>
<?= Html::a('Повернутись', ['index'], ['class' => 'simple-link']) ?>
<div class="photo-gallery">

    <?php foreach ($model->images as $image):?>

        <a href="<?=$image->path?>" data-fancybox="gallery" class="photo-gallery__item">
            <img src="<?=$image->path?>" alt="<?=$image->alt?>">
        </a>

    <?php endforeach;?>

</div>
<div class="photo-gallery__bottom">
    <?= Html::a('Повернутись', ['index'], ['class' => 'simple-link']) ?>
    <div class="pagination">

        <?php
        if($pagination){
            echo LinkPager::widget([
                'pagination' => $pagination,
                //Css option for container
                'options' => ['class' => ''],
                //        First option value
                'firstPageLabel' => '&nbsp;',
                //        Last option value
                'lastPageLabel' => '&nbsp;',
                //        Previous option value
                'prevPageLabel' => '<img src="/images/pagination-prev.png" alt="prev">',
                //Next option value
                'nextPageLabel' => '<img src="/images/pagination-next.png" alt="next">',
                //        //Current Active option value
                'activePageCssClass' => 'active',
                //        //Max count of allowed options
                'maxButtonCount' => 3,
                // Css for each options. Links
                'linkOptions' => ['class' => ''],
            ]);
        }
        ?>

            </div>
        </div>

    </main>
</div>