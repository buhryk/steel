<?php
namespace frontend\widgets;

use backend\modules\section\models\SectionItem;
use yii\jui\Widget;

class SectionWidget extends Widget
{
    public $section;
    public $form;
    public $calendar;
    public $slider;
    public $type;
    public $_class;
    public $page;
    public $katalogSlider;
    public $color;
    public $imageLeft = false;

    public function run()
    {
        $section = SectionItem::getSectionByAlias($this->section);

        return $this->render('sections/type-' . $this->type, [
            'section' => $section,
            'form' => $this->form,
            'calendar' => $this->calendar,
            'slider' => $this->slider,
            'page' => $this->page,
            'katalogSlider' => $this->katalogSlider,
            'color' => $this->color,
            'imageLeft' => $this->imageLeft,
            'class' => $this->_class,
        ]);
    }
}