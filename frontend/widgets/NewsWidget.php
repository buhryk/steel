<?php
namespace frontend\widgets;

use backend\modules\news\models\News;
use backend\modules\news\models\NewsCategory;
use backend\modules\section\models\SectionItem;
use yii\jui\Widget;

class NewsWidget extends Widget
{
    public  $alias;
    public $section;
    public  $color;
    public $type = 1; //News
    public function run()
    {
        $section = SectionItem::getSectionByAlias($this->section);

        $category_id = NewsCategory::find()
            ->andWhere(['status' => NewsCategory::STATUS_ACTIVE])
            ->andWhere(['alias' => $this->alias])
            ->one()->id;
        $lastNews = News::find()
            ->andWhere(['status' => News::STATUS_ACTIVE])
            ->andWhere(['type' => $this->type])
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->joinWith('lang')
            ->orderBy(['data_pub' => SORT_ASC])
            ->andWhere(['category_id' => $category_id])
            ->all();
//        pr($lastNews);
        return $this->render('news', [
            'lastNews' => $lastNews,
            'type' => $this->type,
            'section' => $section,
            'color' => $this->color,
        ]);
    }
}