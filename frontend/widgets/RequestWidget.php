<?php
namespace frontend\widgets;

use frontend\models\RequestForm;

class RequestWidget extends \yii\bootstrap\Widget
{
    public $model;
    public $form_title;
    public $forms;


    public function init(){
        $this->model = new RequestForm();
    }

    public function run() {
        return $this->render('request', [
            'model' => $this->model,
            'form_title' => $this->form_title,
            'forms' => $this->forms,
        ]);
    }
}