<?php
namespace frontend\widgets;

use backend\modules\core\models\Setting;
use backend\modules\email_delivery\models\Subscriber;
use yii\base\Widget;

class SubscribeWidget extends Widget
{
    public $type;

    public function run()
    {
        $model = new Subscriber();

        $activity_categories = explode(',',Setting::getByKey('activity_category')->value);

        $array[''] = \Yii::t('common', 'Категория вашей деятельности');

        foreach ($activity_categories as $item) {
            $array[$item] = $item;
        }

        return $this->render('subscribe-widget-type-' . $this->type, [
            'model' => $model,
            'activity_categories' => $array,
        ]);
    }
}