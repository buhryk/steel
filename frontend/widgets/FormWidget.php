<?php
namespace frontend\widgets;

use frontend\models\RequestForm;
use \backend\modules\custom_form\models\Form;

class FormWidget extends \yii\bootstrap\Widget
{
    public $form;
    public $form_title;
    public $fields;


    public function init(){
        $model_form = Form::findOne($this->form);
        $this->fields = $model_form->getData(Form::ADDITIONAL_DATA_ITEMS) ?: [];

    }

    public function run() {
        return $this->render('form', [
            'fields' => $this->fields,
            'form_title' => $this->form_title,
            'form_id' => $this->form,
        ]);
    }
}