<?php
use backend\modules\slider\models\Slider;

$numbers = Slider::getSlider($slider);
 ?>

<section class="template_4_numbers <?= $color ?>" >
    <div class="container-100 txt-content">
        <?php if ($section->title): ?>
            <h2><?= $section->title ?></h2>
        <?php endif; ?>

        <div class="steps">
            <?php $i = 1; foreach ($numbers as $item): ?>
                <div class="box">
                    <span><?= $i ?></span>
                    <?= $item->title ?>
                </div>
            <?php $i++; endforeach; ?>

            <?= $section->text ?>
        </div>
        <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_1"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>