<section class="template_4_with-title-full fullImg <?= $color ?>">
    <!--    Если не нужен заголовок то удалить весь блок title-full    -->
    <?php if ( $section->title ): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <!--    До этого момента    -->
    <div class="left">
        <img src="<?= $section->image ?>" alt="">
    </div>
    <div class="right txt-content">
        <?= $section->text ?>
    </div>

</section>