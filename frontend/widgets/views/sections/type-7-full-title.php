<?php

 ?>

<section class="template_4_with-title-full fullImg <?= $color ?>">
    <div class="title-full txt-content">
        <h2><?= $section->title ?></h2>
    </div>
    <div class="left txt-content">
        <?= $section->text ?>
    </div>
    <div class="right">
        <img src="<?= $section->image ?>" alt="">
    </div>
</section>
