<?php
use backend\modules\event\models\Event;
use yii\helpers\Url;

$events = Event::find()
    ->where(['active' => Event::ACTIVE_YES, 'id' => $calendar])
    ->orderBy('start_date desc')
    ->limit(4)
    ->all();
 ?>
<section class="template_4_calendar <?= $color ?>" style="flex-wrap: wrap" >
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="left txt-content">
        <?= $section->text ?>
        <a href="<?= $section->url ?>" class="btn-orange"><?= $section->btn_name ?></a>
    </div>
    <div class="right-dates" style="background-image: url(<?= $section->image ?>)">
        <?php foreach ($events as $event): ?>
            <div class="box"  style="background-image: url(<?= $event['image'] ?>)">
                <h3><?= $event['title'] ?></h3>
                <?= $event['short_description'] ?>
                <a href="<?=Url::to(['/event/'.$event['alias']]) ?>"  class="btn-orange">ПОДРОБНЕЕ</a>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>
