<?php

?>

<section class="<?= $class ?> <?= $color ?>" >
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100">

        <div class="left"></div>
        <div class="right txt-content">
            <?= $section->text ?>
            <a href="<?= $section->url ?>" data-popup="<?= $form ? 'forma_1' : '' ?>" class="btn-orange"><?= $section->btn_name ?></a>
        </div>
    </div>
</section>

<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>

