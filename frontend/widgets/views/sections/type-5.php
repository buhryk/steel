
<section class="template_4_double-coll <?= $color ?>" style="background-image: url('/img/bg_1.png')">
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100">
        <div class="left txt-content">
            <?= $section->text ?>
        </div>
        <div class="right txt-content">
            <?= $section->description ?>
            <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_1"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
        </div>
    </div>
</section>

<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>