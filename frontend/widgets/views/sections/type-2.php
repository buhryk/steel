<?php

?>

<section class="template_4_fulImg <?= $color ?>">
    <div class="left txt-content">
        <?php if ($section->title): ?>
            <h2><?= $section->title ?></h2>
        <?php endif; ?>
        <?= $section->text ?>
        <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_1"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
    </div>
    <div class="right">
        <img src="<?= $section->image ?>" alt="">
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>