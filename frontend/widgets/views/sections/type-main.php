<?php
?>
<section class="template_4_header<?= $katalogSlider ? '' : '-simple' ?> <?= $color ?>"
         style="background-image: url(<?= $page->image ?>)">
    <div class="container-100">
        <h1><?= $page->title ?></h1>
        <p class="description-text"><?= $page->description ?></p>
        <?php if ($katalogSlider) : ?>
            <div class="page-home__ready">
                <div class="container-100" style="padding: 0">
                    <div class="left">
                        <?= $section->text ?>
                        <a href="<?= $section->url ?>" class="btn-orange"><?= $section->btn_name ?></a>
                    </div>
                    <div class="right">
                        <div class="dragscroll draggable">
                            <p id="floatingmes">Нажмите и потяните</p>
                            <?php foreach ($katalogSlider->childrens as $item):?>
                                <div data-href="<?= $section->url ?>?category_id=<?= $item->id ?>" class="box">
                                    <img src="<?= $item->image ?>" alt="">
                                    <h3><?= $item->title ?></h3>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div></section><?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?><?php endif; ?>