<?php

 ?>

<section class="template_4_textBlock <?= $color ?>">
    <div class="container-100 txt-content">
        <?php if ($section->title): ?>
            <h2><?= $section->title ?></h2>
        <?php endif; ?>

        <?= $section->text ?>
    </div>
</section>
