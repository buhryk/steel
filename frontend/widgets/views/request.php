<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$classname = \yii\helpers\StringHelper::basename(get_class($model));
$types = \backend\modules\request\models\Request::getAllTypes();
?>
<div class="popup form_pop" data-popup="forma_1">
    <?php $form = ActiveForm::begin([
        'id' => 'cooperate-form',
        'options' => [
            'onsubmit' => 'return false',
            'data-url' => Url::to(['/request/create']),
            'data-success-message' => Yii::t('request', 'Ваша заявка успешно отправлена')
        ]
    ]); ?>

    <p>
        <?php if (in_array('name', $forms)): ?>
            <span><input class="input" id="name" name="<?= $classname . '[name]'; ?>" type="text" placeholder="ФИО"></span>
        <?php endif; ?>
        <?php if (in_array('phone', $forms)): ?>
            <span>
                <input class="input" id="phone" type="tel" name="<?= $classname . '[phone]'; ?>" placeholder="<?= Yii::t('request', 'Ваше телефон'); ?>">
            </span>
            <?php $this->registerJs("
                    var selector = document.getElementById(\"phone\");
                    var im = new Inputmask(\"+38(999)999-99-99\");
                    im.mask(selector);
                ",
                \yii\web\View::POS_READY,
                'my-button-handler'
            ); ?>
        <?php endif; ?>
        <?php if (in_array('email', $forms)): ?>
            <span><input class="input" id="email"  name="<?= $classname . '[email]'; ?>" type="text" placeholder="E-mail"></span>
        <?php endif; ?>
        <?php if (in_array('text', $forms)): ?>
            <span><textarea class="input" id="text"  name="<?= $classname . '[text]'; ?>"></textarea></span>
        <?php endif; ?>

        <input type="hidden" name="<?= $classname . '[form_title]'; ?>" value="<?= $form_title ?>">

        <input type="submit" id="create_request" class="btn-orange" value="ПОДПИСАТЬСЯ">
    </p>

    <?php ActiveForm::end(); ?>
    <div class="exit-btn"></div>
</div>

<div class="popup form_pop" id="popup-success">
    <p></p>
    <div class="exit-btn"></div>
</div>