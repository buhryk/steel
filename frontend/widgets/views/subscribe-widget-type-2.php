<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$redirectUrl = Yii::$app->request->url;
?>

<div class="sub-row">
    <h2><?= Yii::t('common', 'ПОДПИШИСЬ, НА НАШУ E-MAIL РАССЫЛКУ') ?></h2>
    <?php $form = ActiveForm::begin([
        'action' => ['/site/subscribe-form', 'rediractUrl' => $redirectUrl],
        'fieldConfig' => [
            'options' => [
                'class' => 'input-box'
            ]]
        ,
    ]); ?>
    <div class="form-cont">
        <?= $form->field($model, 'name')
            ->textInput(['autofocus' => true])->input('name', ['placeholder' => Yii::t('common', 'ФИО')])->label(false); ?>

        <?= $form->field($model, 'email',['enableAjaxValidation' => true])
            ->textInput()->input('email', ['placeholder' => Yii::t('common', 'E-mail')])->label(false); ?>

        <?= $form->field($model, 'category', [
            'template' => '
       
        <div class="custom-select select-box input-box field-subscriber-category">
            {input}
        </div>',
        ])
            ->dropDownList($activity_categories)

            ->label(false) ?>

        <input type="submit" class="btn-orange"  value="<?= Yii::t('common', 'Подписаться') ?>">
    </div>
    <?php ActiveForm::end(); ?>
</div>


