<?php
use backend\modules\core\models\PageLang;
use common\widgets\WLang;
use yii\helpers\Url;
use backend\modules\core\models\Menu;
?>

<div class="right">
    <div class="menu-container">
        <ul>
            <?php foreach ($menuItems as $item): ?>
                <?php if ($item->type == 2): ?>
                    <?php if ($item->childrens && $item->show_children): ?>
                        <li data-menu="<?= $item->id ?>">
                            <a href="<?= Url::to($item->getUrl(true),true); ?>" class="<?= Menu::getCurrent()->id == $item->id ? 'active' : '' ?>"><?=$item->name ?></a>
                            <?php if ($item->childrens && $item->show_children): ?>
                                <ul class="sub-menu">
                                    <?php foreach ($item->childrens as $childItem):?>
                                        <li><a class="<?= Menu::getCurrent()->id == $childItem->id ? 'active' : '' ?>"
                                               href="<?= Url::to($childItem->getUrl(true),true); ?>">
                                                <?= $childItem->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>

                            <?php endif; ?>
                        </li>
                    <?php else: ?>
                        <li><a class="<?= Menu::getCurrent()->id == $item->id ? 'active' : '' ?>" href="<?= Url::to($item->getUrl(true), true); ?>"><?=$item->name ?></a></li>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <!--    Start lang-->
    <?= WLang::widget();?>
    <!--    End lang-->
</div>
<div class="menu-btn">
    <p>МЕНЮ</p>
</div>

<div class="big-menu">
    <div class="con">
        <div class="left" style="background-image: url(/img/head_l.jpg)">

            <ul>
                <?php foreach ($menuItems as $item): ?>
                    <?php if ($item->type == 1): ?>
                        <?php if ($item->childrens && $item->show_children): ?>
                            <li data-menu="<?= $item->id ?>"><a href="<?= Url::to($item->getUrl(true),true); ?>" class="<?= Menu::getCurrent()->id == $item->id ? 'active' : '' ?>"><?=$item->name ?></a></li>
                        <?php else: ?>
                            <li><a class="<?= Menu::getCurrent()->id == $item->id ? 'active' : '' ?>" href="<?= Url::to([$item->getUrl(true)]); ?>"><?=$item->name ?></a></li>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <ul>
                <?php foreach ($menuItems as $item): ?>
                    <?php if ($item->type == 2): ?>
                        <?php if ($item->childrens && $item->show_children): ?>
                            <li data-menu="<?= $item->id ?>"><a href="##" class="<?= Menu::getCurrent()->id == $item->id ? 'active' : '' ?>"><?=$item->name ?></a></li>
                        <?php else: ?>
                            <li><a class="<?= Menu::getCurrent()->id == $item->id ? 'active' : '' ?>" href="<?= Url::to([$item->getUrl(true)]); ?>"><?=$item->name ?></a></li>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="right" style="background-image: url(/img/head_r.jpg)">
            <?php foreach ($menuItems as $item): ?>
                <?php if ($item->childrens && $item->show_children): ?>
                    <div class="sub-menu" data-menu="<?= $item->id ?>">
                        <ul>
                            <?php foreach ($item->childrens as $childItem):?>
                                <li><a class="<?= Menu::getCurrent()->id == $childItem->id ? 'active' : '' ?>"
                                       href="<?= Url::to($childItem->getUrl(true),true); ?>">
                                        <?= $childItem->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>

            <div class="con-mob">
                <ul>
                    <?php foreach ($menuItems as $item): ?>
<!--                        --><?php //if ($item->type == 2): ?>
                            <?php if ($item->childrens && $item->show_children): ?>
                                <li class="has-children" data-menu="<?= $item->id ?>">
                                    <a href="<?= Url::to($item->getUrl(true),true); ?>" ><?=$item->name ?>  <span class="arrow"></span></a>
                                    <?php if ($item->childrens && $item->show_children): ?>
                                        <ul >
                                            <?php foreach ($item->childrens as $childItem):?>
                                                <li><a
                                                       href="<?= Url::to($childItem->getUrl(true),true); ?>">
                                                        <?= $childItem->name; ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>

                                    <?php endif; ?>
                                </li>
                            <?php else: ?>
                                <li><a href="<?= Url::to($item->getUrl(true), true); ?>"><?=$item->name ?></a></li>
                            <?php endif; ?>
<!--                        --><?php //endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <!--    Start lang-->
        <?= WLang::widget();?>
        <!--    End lang-->
        <div class="exit-btn"></div>
        <p class="deco">МЕНЮ:</p>
    </div>
</div>

