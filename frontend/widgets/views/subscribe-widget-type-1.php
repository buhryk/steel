<?php
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Url;

$redirectUrl = Yii::$app->request->url;
?>

<section class="page-home__contacts">
    <div class="left">
        <h2 class="title_1"><?= Yii::t('common', 'ПОДПИШИСЬ, ЧТОБЫ<br> НЕ ПРОПУСКАТЬ ВАЖНЫЕ СОБЫТИЯ') ?></h2>

        <?php $form = ActiveForm::begin([
            'action' => ['/site/subscribe-form', 'rediractUrl' => $redirectUrl],
            'fieldConfig' => [
                'template' => "<span>{input}{error}</span>",
            ],
        ]); ?>
        <p>
            <?= $form->field($model, 'name')
                ->textInput(['autofocus' => true])->input('name', ['placeholder' => Yii::t('common', 'ФИО'),'class' => 'input'])->label(false); ?>

            <?= $form->field($model, 'email',['enableAjaxValidation' => true])
                ->input('email', ['placeholder' => Yii::t('common', 'E-mail'),'class' => 'input'])->label(false); ?>
            <?= $form->field($model, 'category')
                ->textInput(['autofocus' => true])
                ->input('category', ['placeholder' => Yii::t('common', 'Категория вашей деятельности'),'class' => 'input'])->label(false) ?>

            <input type="submit" class="btn-orange"  value="<?= Yii::t('common', 'ПОДПИСАТЬСЯ') ?>">
        </p>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="right" style="background-image: url(/img/bg_2.png)"></div>
</section>

