<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use \backend\modules\custom_form\models\Form;
?>
<div class="popup form_pop" data-popup="forma_1">
    <?php $form = ActiveForm::begin([
        'id' => 'cooperate-form',
        'options' => [
            'onsubmit' => 'return false',
            'data-url' => Url::to(['/form/create']),
            'data-success-message' => Yii::t('request', 'Ваша заявка успешно отправлена')
        ]
    ]); ?>

    <p>
        <?php foreach ($fields as $field): ?>
            <?php if ($field['type'] == Form::TYPE_TEXT): ?>
                <span><input class="input" name="field[<?= $field['name'] ?>]" type="text" placeholder="<?= $field['placeholder'] ?>"></span>
            <?php endif; ?>
            <?php if ($field['type'] == Form::TYPE_EMAIL): ?>
                <span><input class="input"   name="field[<?= $field['name'] ?>]" type="text" placeholder="<?= $field['placeholder'] ?>"></span>
            <?php endif; ?>
            <?php if ($field['type'] == Form::TYPE_PHONE): ?>
                <span><input class="input"   name="field[<?= $field['name'] ?>]" type="tel" placeholder="<?= $field['placeholder'] ?>"></span>
                <?php $this->registerJs("
                    var selector = $('input[type=\'tel\']');
                    var im = new Inputmask(\"+38(999)999-99-99\");
                    im.mask(selector);
                ",
                    \yii\web\View::POS_READY,
                    'my-button-handler'
                ); ?>
            <?php endif; ?>
            <?php if ($field['type'] == Form::TYPE_AREA): ?>
                <span><textarea class="input"  name="field[<?= $field['name'] ?>]" placeholder="<?= $field['placeholder'] ?>"></textarea></span>
            <?php endif; ?>
            <?php if ($field['type'] == Form::TYPE_SELECT): ?>
                <div class="input-box select-box custom-select">
                <select   name="field[<?= $field['name'] ?>]">
                    <option value=""><?= $field['placeholder'] ?></option>
                    <?php foreach ( explode("\n", $field['option']) as $item): ?>
                        <option value="<?= $item ?>"><?= $item ?></option>
                    <?php endforeach; ?>
                </select>
                </div>
            <?php endif; ?>

            <?php if ($field['type'] == Form::TYPE_CHECKBOX): ?>
                <p class="popup-checkbox"><?= $field['placeholder'] ?></p>
                <?php $i = 0; foreach ( explode("\n", $field['option']) as $item): ?>
                    <div class="check-box">
                        <input type="checkbox" name="field[<?= $field['name'] ?>][]" id="<?= $item ?>" value="<?= $item ?>">
                        <label for="<?= $item ?>"><?= $item ?></label>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <input type="hidden" name="field[Название секции]" value="<?= $form_title ?>">
        <input type="hidden" name="form_id" value="<?= $form_id ?>">

        <input type="submit" id="create_form" class="btn-orange" value="ПОДПИСАТЬСЯ">
    </p>

    <?php ActiveForm::end(); ?>
    <div class="exit-btn"></div>
</div>

<div class="popup form_pop" id="popup-success">
    <p></p>
    <div class="exit-btn"></div>
</div>