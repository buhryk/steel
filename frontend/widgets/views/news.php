<?php

use backend\modules\news\models\News;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

?>
<?php if ($lastNews): ?>
<section class="block_catalog triple template_4" style="padding-bottom: 0">
    <div class="container-100">
<!--        <h2 class="title_1">--><?//= Yii::t('common', 'Последние ' . News::getTypeList()[$type])  ?><!--</h2>-->
        <h2 class="title_1"><?= $section->title  ?></h2>
        <div class="catalog-list">

            <?php foreach ($lastNews as $item): ?>
                <div class="box">
                    <?= Yii::$app->thumbnail->img($item->image, ['thumbnail' => ['width' => 480, 'height' => 270]], [
                        'alt' => $item->title,
                    ]) ?>
                    <h2><?= StringHelper::truncate(strip_tags($item->title), 70 )?></h2>
                    <p> <?= StringHelper::truncate(strip_tags($item->text), 250 )?></p>
                    <div class="bottom"><p><?=date("d.m.Y",strtotime($item->data_pub))?></p>
                        <?= Html::a(Yii::t('common', 'ПОДРОБНЕЕ'), ['/news/news/view', 'alias' => $item->alias]) ?>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>
