<?php
namespace frontend\controllers;

use backend\modules\catalog\models\CatalogCategory;
use backend\modules\core\models\Menu;
use backend\modules\core\models\Page;
use backend\modules\core\models\Setting;
use backend\modules\email_delivery\models\Subscriber;
use backend\modules\event\models\Event;
use backend\modules\news\models\News;
use backend\modules\section\models\SectionItem;
use backend\modules\slider\models\Slider;
use common\models\Lang;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $lastNews = News::find()
            ->andWhere(['status' => News::STATUS_ACTIVE])
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->joinWith('lang')
            ->all();

        $katalogSlider = CatalogCategory::find()->where(['alias' => 'catalog1'])->one();

        $katalog = SectionItem::getSectionByAlias('katalog-gotovykh-resheniy');
        $znaete = SectionItem::getSectionByAlias('ne-znaete-s-chego-nachat');
        $podrobnee = SectionItem::getSectionByAlias('podrobnee-pro-assotsiatsiyu');

        $model = new Subscriber();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Спасибо за подписку на новости'));
            return $this->refresh();
        } else {
            return $this->render('index', [
                'lastNews' => $lastNews,
                'katalogSlider' => $katalogSlider,
                'katalog' => $katalog,
                'znaete' => $znaete,
                'podrobnee' => $podrobnee,
            ]);
        }
    }

    public function actionGetIndex()
    {
                $output = curl_init();	//подключаем курл
        curl_setopt($output, CURLOPT_URL, 'http://194.183.181.188:81/');	//отправляем адрес страницы
        curl_setopt($output, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($output, CURLOPT_HEADER, 0);
        $out = curl_exec($output);		//помещаем html-контент в строку
        curl_close($output);	//закрываем подключение
        echo $out;
//
//        $dom =  new \DOMDocument( "1.0", "ISO-8859-15" );	//создаем объект
//        $dom->loadHTML($out);	//загружаем контент
//
//        $xpath = new \DOMXpath($dom);
//        $articles = $xpath->query('//div[@class="col-md-8"]');
//
//        foreach($articles as $container) {
//            pr($articles->loadHTML());
//        }
    }

    public function actionContact()
    {
        $this->layout = 'contact';

        $page = Page::getPageByAlias('contact-page');

        return $this->render('contact', [
                'page' => $page,
        ]);

    }

    public function actionCalendar()
    {
        $model = new Event();

        $years = $model->getEventsItem();

        $days = Yii::$app->request->get('days') ?? 365;

        $timestamp = $days * 86400;

        if ($year = Yii::$app->request->get('year')){
            $events = $model->getEventByLetter($year, $timestamp);
        } else {
            $events = $model->getEventByLetter($year = end($years)['year'], $timestamp);
        }
//pr($events);
        $page = Page::getPageByAlias('kalendar-meropriyatiy');

        return $this->render('calendar', [
                'page' => $page,
                'years' => $years,
                'year' => $year,
                'last_year' => end($years)['year'],
                'events' => $events,
        ]);

    }

    public function actionSubscribeForm($rediractUrl)
    {

        $model = new Subscriber();


        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        else if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', 'Спасибо за подписку на новости');
            return $this->redirect($rediractUrl);
        }

    }

}
