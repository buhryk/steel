<?php
namespace frontend\controllers;

use backend\modules\custom_form\models\FormLetter;
use frontend\models\RequestForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class FormController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new FormLetter();

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax) {

            $model->form_id = Yii::$app->request->post('form_id');
            $model->additional_data = Yii::$app->request->post();

//            pr($model);
            $response = [
                'status' => 'success',
            ];


                $model->save();


            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }
}
