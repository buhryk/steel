<?php
namespace frontend\controllers;

use backend\modules\committee\models\committee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CommitteeController extends Controller
{
    public function actionIndex()
    {
        $model = committee::find()->where(['status' => committee::STATUS_ACTIVE])->all();

        return $this->render('index', ['model' => $model]);
    }

    public function actionView($alias)
    {
        $model = $this->findModel($alias);

//        if ($model->menu->page_id) {
//            $page = Page::getPageById($model->menu->page_id);
//        }

        return $this->render('view', ['model' => $model]);
    }

    protected function findModel($alias)
    {
        if (($model = committee::find()->where(['alias' => $alias, 'status' => committee::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}