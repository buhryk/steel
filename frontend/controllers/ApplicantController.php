<?php
namespace frontend\controllers;

use backend\modules\core\models\Page;
use yii\web\Controller;

class ApplicantController extends Controller
{
    public function actionLogin()
    {
        $pageModel =  $page = Page::getPageByAlias('login-page');

        return $this->render('login', ['pageModel' => $pageModel]);
    }

    public function actionRegistration()
    {
        $pageModel =  $page = Page::getPageByAlias('registration-page');

        return $this->render('registration', ['pageModel' => $pageModel]);
    }


    public function actionChangePassword()
    {
        $pageModel =  $page = Page::getPageByAlias('change-password-page');

        return $this->render('change-password', ['pageModel' => $pageModel]);
    }
}