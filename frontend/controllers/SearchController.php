<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 18.12.2017
 * Time: 17:03
 */

namespace frontend\controllers;


use backend\modules\core\models\Page;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use common\models\Lang;

class SearchController extends Controller
{

    public function actionIndex(){

        $q = trim(Yii::$app->request->get('q'));
        if(!$q)
            return $this->render('view');
        $query = (new \yii\db\Query())
            ->select(['title', 'description', 'alias'])
            ->from(['page'])
            ->where(['lang_id' => Lang::getCurrent()->id])
            ->andFilterWhere([
                'or',
                'title like "%' . $q . '%" ',
                'text like "%' . $q . '%" ',
                'description like "%' . $q . '%" '
            ])
            ->join('LEFT JOIN', 'page_lang', 'record_id = id');



        $menu = (new \yii\db\Query())
            ->select(['name', 'id'])
            ->from(['menu'])
            ->where(['lang_id' => Lang::getCurrent()->id])
            ->andFilterWhere([
                'or',
                'name like "%' . $q . '%" ',
            ])
            ->join('LEFT JOIN', 'menu_lang', 'record_id = id')
            ->all();

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('view', compact('models', 'pagination', 'q', 'menu'));
    }

}