<?php
namespace frontend\controllers;

use backend\modules\catalog\models\Catalog;
use backend\modules\core\models\Page;
use backend\modules\core\models\Setting;
use backend\modules\news\models\News;
use backend\modules\tenders\models\tenders;
use common\components\user\events\FormEvent;
use frontend\models\LoginForm;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class MemberController extends Controller
{
    public $enableCsrfValidation = false;

    public $layout = 'cabinet';
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['login'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['login', 'logout', 'index', 'news', 'tenders', 'project', 'write'], 'roles' => ['@']],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'user' => Yii::$app->user->identity
        ]);
    }

    public function actionLogin()
    {
        $this->layout = 'main' ;
        if (!\Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(['/member/index']);
        }


        /** @var LoginForm $model */
        $model = \Yii::createObject(LoginForm::className());

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            return Yii::$app->getResponse()->redirect(['/member/index']);
        }

        $page = Page::getPageByAlias('members');

        return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
            'page' => $page
        ]);
    }

    protected function performAjaxValidation(Model $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            \Yii::$app->response->data   = ActiveForm::validate($model);
            \Yii::$app->response->send();
            \Yii::$app->end();
        }
    }

    public function actionLogout(){
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionNews(){
        $models = News::find()->where([
            'company_id' => Yii::$app->user->id,
            'status' => News::STATUS_ACTIVE
        ])
            ->orderBy(['updated_at' => SORT_ASC])
            ->all();

        return $this->render('news', [
            'models' => $models
        ]);
    }

    public function actionProject($type){
        $models = Catalog::find()->where([
            'company_id' => Yii::$app->user->id,
            'type' => $type,
            'status' => Catalog::STATUS_ACTIVE
        ])
            ->orderBy(['updated_at' => SORT_ASC])
            ->all();

        return $this->render('project', [
            'models' => $models
        ]);
    }




    public function actionTenders(){
        $ids = Yii::$app->request->get('categories_id');

        $query = tenders::find();
        $query->andWhere([
            'company_id' => Yii::$app->user->id,
            'status' => tenders::STATUS_ACTIVE
        ]);

        if($ids) {
            $query->andWhere(['category_id' => $ids]);
        }

        $query->orderBy(['id' => SORT_DESC]);

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        if (Yii::$app->request->isAjax) {

            $html = $this->renderAjax('tenders', ['models' => $models, 'pagination' => $pagination]);

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'html' => $html,
            ];
        }

        return $this->render('tenders', [
            'models' => $models
        ]);
    }

    public function actionWrite(){

        if ($text = Yii::$app->request->post('text')){
            if ($this->sendEmail(Setting::getValueByKey('emails_normal'), $text)) {
                Yii::$app->session->setFlash('success', 'Сообщение отправлено');
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Сообщение не отправлено'));
            }

            return $this->refresh();

        }

        return $this->render('write', [

        ]);
    }

    public function sendEmail($email, $text)
    {
//        pr($this);
        $user = Yii::$app->user->identity;

        return Yii::$app->mailer->compose(
            ['html' => 'writeToAdmin'],
            [
                'text' => $text,
                'user' => $user
            ])
            ->setTo(explode(',',$email))
            ->setFrom([$user->email => $user->name])
            ->setSubject('Отправка формы администратору')
            ->setTextBody($text)
            ->send();
    }
}
