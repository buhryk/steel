<?php
namespace frontend\controllers;

use backend\modules\catalog\models\CatalogCategory;
use backend\modules\core\models\Page;
use backend\modules\news\models\News;
use backend\modules\section\models\SectionItem;
use backend\modules\seo\models\Seo;
use backend\modules\slider\models\Slider;
use yii\web\Controller;

/**
 * Site controller
 */
class PageController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function actionCustomer()
    {

        $katalogSlider = CatalogCategory::find()->where(['alias' => 'catalog1'])->one();
        $servisySlider = Slider::findOne(5);


        $advantage = SectionItem::getSectionByAlias('1');
        $katalog = SectionItem::getSectionByAlias('katalog-gotovykh-resheniy');
        $pochemu = SectionItem::getSectionByAlias('pochemu-vybirayut-nas');
        $vybrat = SectionItem::getSectionByAlias('vy-gotovy-vybrat-ispolnitelya');
        $servisy = SectionItem::getSectionByAlias('nashi-servisy');

        $page = Page::getPageByAlias('customer');

        return $this->render('customer', [
            'page' => $page,
            'katalogSlider' => $katalogSlider,
            'servisySlider' => $servisySlider,
            'katalog' => $katalog,
            'pochemu' => $pochemu,
            'vybrat' => $vybrat,
            'servisy' => $servisy,
            'advantage' => $advantage
        ]);
    }

    public function actionDesigner()
    {

        $katalogSlider = CatalogCategory::find()->where(['alias' => 'catalog1'])->one();


        $katalog = SectionItem::getSectionByAlias('katalog-gotovykh-resheniy');
        $kalendar = SectionItem::getSectionByAlias('kalendar-sobytiy');

        $page = Page::getPageByAlias('designer');

        return $this->render('designer', [
            'page' => $page,
            'katalogSlider' => $katalogSlider,
            'katalog' => $katalog,
            'kalendar' => $kalendar,

        ]);
    }

    public function actionWhy()
    {

        $katalogSlider = CatalogCategory::find()->where(['alias' => 'catalog1'])->one();

        $page = Page::getPageByAlias('why');

        return $this->render('why', [
            'page' => $page,
            'katalogSlider' => $katalogSlider,

        ]);
    }

    public function actionArchitect()
    {

        $katalogSlider = CatalogCategory::find()->where(['alias' => 'catalog1'])->one();

        $page = Page::getPageByAlias('architect');

        $freedom = SectionItem::getSectionByAlias('steel-freedom');

        return $this->render('architect', [
            'page' => $page,
            'katalogSlider' => $katalogSlider,
            'freedom' => $freedom,

        ]);
    }

    public function actionAnalytic()
    {

        $page = Page::getPageByAlias('analytic');

        $obzory = SectionItem::getSectionByAlias('analiticheskie-obzory');

        return $this->render('analytic', [
            'page' => $page,
            'obzory' => $obzory,

        ]);
    }

    public function actionInfocenter()
    {

        $page = Page::getPageByAlias('infocenter');

        return $this->render('infocenter', [
            'page' => $page,
        ]);
    }

    public function actionView($alias)
    {
        $page = Page::getPageByAlias($alias);

        $widgets = $page->getData(Page::ADDITIONAL_DATA_ITEMS) ?: [];

        if (!empty($page->seo->lang))
        Seo::registerMetaTags($page->seo->lang);
//        pr($widgets);

        $katalogSlider = CatalogCategory::find()->where(['alias' => 'catalog1'])->one();


        if ($widgets){
            return $this->render('view-with-widgets', [
                'page' => $page,
                'katalogSlider' => $katalogSlider ?? false,
                'widgets' => $widgets,
            ]);
        } else {
            return $this->render('view', [
                'page' => $page,
            ]);
        }

    }



}
