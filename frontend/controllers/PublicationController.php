<?php
namespace frontend\controllers;


use backend\modules\core\models\Menu;
use backend\modules\core\models\Page;
use backend\modules\publication\models\Publication;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PublicationController extends Controller
{
    public function actionView($alias)
    {
        $pageModel = Page::getPageByAlias($alias);

        $query = Publication::find()
            ->andWhere(['menu_id' => $pageModel->menu->id])
            ->andWhere(['status' => Publication::STATUS_ACTIVE])
            ->orderBy(['position' => SORT_DESC]);

        $pages = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $publications = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('view', [
            'publications' => $publications,
            'pages' => $pages
        ]);
    }


}