<?php
namespace frontend\controllers;

use frontend\models\RequestForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class RequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new RequestForm();

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $validate = $model->validate();
            $errors = $model->getErrors();

            $response = [
                'status' => 'success',
                'validation' => $validate,
            ];

            if (!$validate) {
                $response['errors'] = $errors;
                $response['status'] = 'error';
            } else {
                $model->createRequest();
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }
}
