<?php
namespace frontend\controllers;

use yii\web\Controller;


class LabController extends Controller
{

    public function actionLab1($key)
    {
        return $this->renderpartial('lab1', [
            'result' => self::cesarEncryptString('етхйкфзвсзнюукпэгрщмвок',$key). "\n",
        ]);
    }

    public function actionLab1oleg()
    {
        for($i = 1; $i <= 32; $i++)
        {
           echo 'Ключ: ' . $i . '; Строка: ' . self::cesarEncryptString('ашхшъшееуэашхшъше',$i). "<br>";
        }

    }

    public static function cesarEncryptString($string, $key)
    {
        $string = str_replace(['ё', 'Ё'], ['е', 'Е'], $string);
        $chars_array = mb_str_split($string);
        $result_array = [];

        $start_upper_rus_code = mb_ord('А');
        $finish_upper_rus_code = mb_ord('Я');
        $start_lower_rus_code = mb_ord('а');
        $finish_lower_rus_code = mb_ord('я');
        $count_rus_letters = $finish_lower_rus_code - $start_lower_rus_code + 1;

        $start_upper_eng_code = mb_ord('A');
        $finish_upper_eng_code = mb_ord('Z');
        $start_lower_eng_code = mb_ord('a');
        $finish_lower_eng_code = mb_ord('z');
        $count_eng_letters = $finish_lower_eng_code - $start_lower_eng_code + 1;


        foreach ($chars_array as $char) {
            $char_code = mb_ord($char);
            if(in_array($char, [" ", "\r", "\n", ",", ".", ":", "?"])) {
                $result_array[] = $char;
            }


            $result_array[] = self::cesarEncryptChar($char_code, $key, $count_rus_letters, $start_lower_rus_code);

        }

        return implode($result_array);


    }


    private static function cesarEncryptChar($char_code, $key, $alphabet_length, $start_symbol_code)
    {
        $key = $key % $alphabet_length;
        $current_symbol_code = $char_code - $start_symbol_code;
        $new_symbol_difference = ($current_symbol_code+$key)%$alphabet_length;
        return mb_chr($start_symbol_code + $new_symbol_difference);
    }
}
