// Request
$( document ).ready(function() {
    $('#create_request').on('click', function () {
        var form = $('#cooperate-form');

        var successMessage = form.data('success-message');
        $('.validate-done').remove();

        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serialize(),
            success: function(data) {
                if (data.status == 'success') {

                    $('.exit-btn').click();
                    $('#popup-success').fadeIn();
                    $("#popup-success p").html(successMessage);


                    clearForm(form);
                } else if (data.errors) {
                    $.each(data.errors, function(index, value) {
                        console.log(value);
                        addError(index, value);

                    });
                }
            }
        });
    });

    function addError(elementId, value) {
        var element = $('#'+elementId);
        // element.parent().addClass("no-valid");
        element.parent().append('<div class="help-block">'+value+'</div>');
    }

    function clearForm(form) {
        $.each(form.find('.input, textarea'), function() {
            $(this).val('')
        });
    }
});

// Custom form
$( document ).ready(function() {
    $('#create_form').on('click', function () {
        var form = $('#cooperate-form');

        var successMessage = form.data('success-message');
        $('.validate-done').remove();

        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serialize(),
            success: function(data) {
                console.log(data);
                if (data.status == 'success') {

                    $('.exit-btn').click();
                    $('#popup-success').fadeIn();
                    $("#popup-success p").html(successMessage);


                    clearForm(form);
                } else if (data.errors) {
                    $.each(data.errors, function(index, value) {
                        console.log(value);
                        addError(index, value);

                    });
                }
            }
        });
    });

    function addError(elementId, value) {
        var element = $('#'+elementId);
        // element.parent().addClass("no-valid");
        element.parent().append('<div class="help-block">'+value+'</div>');
    }

    function clearForm(form) {
        $.each(form.find('.input, textarea'), function() {
            $(this).val('')
        });
    }
});