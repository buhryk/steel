$(document).ready(function () {
    //   FOR_ALL

    //anchors
    $('.anchor').click(function (e) {
        e.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    //accordion_aside
    $('.acc_title').click(function (e) {
        e.preventDefault();
        $(this).next('.acc_body').slideToggle();
    });

    //menu
    $('.menu-btn').click(function (e) {
        e.preventDefault();
        var ul_1,ul_2;

        $('.big-menu').fadeIn(500);
        $('body').addClass('fix');

        ul_1 = $('.big-menu .left ul:nth-child(1) li').length;ul_1 = ul_1 * 100; ul_1 = ul_1 + 100;
        var i=1;
        function liUp1() {$('.big-menu .left ul:nth-child(1) li:nth-child('+i+')').addClass('open');i++;}
        var timerId = setInterval(function () {liUp1();},100);
        setTimeout(function () {clearInterval(timerId); eee();},ul_1);

        function eee() {
            ul_2 = $('.big-menu .left ul:nth-child(2) li').length;ul_2 = ul_2 * 100; ul_2 = ul_2 + 100;
            var j=1;
            function liUp2() {$('.big-menu .left ul:nth-child(2) li:nth-child('+j+')').addClass('open');j++;}
            var timerId = setInterval(function () {liUp2();},100);
            setTimeout(function () {clearInterval(timerId);},ul_2);
        }
    });

    //tabs

    $('[data-tab]').click(function () {
        if (!$(this).hasClass('open')){
            var tab = $(this).data('tab');
            $('[data-tab]').removeClass('open');
            $('[data-tab='+tab+']').addClass('open');
            $('[data-tabBody]').css('display','none');
            $('[data-tabBody='+tab+']').css('display','block');
        }
    });
    $('[data-popup]').click(function (e) {
        if (!$(this).hasClass('popup')){
            e.preventDefault();
            var popup = $(this).data('popup');
            $('.popup[data-popup='+popup+']').fadeIn();
        }
    });

    $('.popup .exit-btn').click(function (e) {
        e.preventDefault();
        $(this).closest('.popup').fadeOut();
    });
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".popup"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.fadeOut(); // скрываем его
        }
    });

    $('.big-menu .exit-btn').click(function (e) {
        e.preventDefault();
        $('.big-menu').fadeOut();
        $('.big-menu .left ul li').removeClass();
        $('body').removeClass('fix');
    });

    $('.big-menu .arrow').click(function (e) {
        e.preventDefault();
        if (!$(this).hasClass('open')){
            $(this).closest('.con-mob').find('.arrow').removeClass('open');
            $(this).addClass('open');
            $(this).closest('.con-mob').find('.has-children').find('ul').slideUp();
            $(this).closest('a').next('ul').slideDown();
        }
        else {
            $(this).closest('.con-mob').find('.arrow').removeClass('open');
            $(this).closest('.con-mob').find('.has-children').find('ul').slideUp();
        }
    });

    if ($('main').hasClass('page-home')){
        //fixed header
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 1){
            $('.header').addClass('fix');
        }
        else {
            $('.header').removeClass('fix');
        }
        $(window).scroll(function () {
            scrollTop = $(window).scrollTop();
            if (scrollTop > 1){
                $('.header').addClass('fix');
            }
            else {
                $('.header').removeClass('fix');
            }
        });


    }
    //video play btn
    $('.video .btn').click(function (e) {
        e.preventDefault();
        $(this).closest('.video').find('video').get(0).play();
        $(this).closest('.video').find('video').attr('controls','');
        $(this).addClass('close');
        $(this).next('.under-btn').addClass('close');
        $(this).closest('.video').addClass('close');
    });
    $('.big-menu .left li').mouseenter(function () {
        var menu = $(this).data('menu');
        $('.big-menu .sub-menu').removeClass('active');
        $('.big-menu .sub-menu[data-menu='+menu+']').addClass('active');
        $('.big-menu .left li a').removeClass('active');
    });
    $('.big-menu .left li').mouseleave(function () {
        $('.big-menu .sub-menu').removeClass('active');
    });
    $('.big-menu .right .sub-menu').mouseenter(function () {
        $(this).addClass('active');
        var menu = $(this).data('menu');
        $('.big-menu .left li a').removeClass('active');
        $('.big-menu .left li[data-menu='+menu+'] a').addClass('active');
    });



    /**
     * @fileoverview dragscroll - scroll area by dragging
     * @version 0.0.8
     *
     * @license MIT, see http://github.com/asvd/dragscroll
     * @copyright 2015 asvd <heliosframework@gmail.com>
     */


    (function (root, factory) {
        if (typeof define === 'function' && define.amd) {
            define(['exports'], factory);
        } else if (typeof exports !== 'undefined') {
            factory(exports);
        } else {
            factory((root.dragscroll = {}));
        }
    }(this, function (exports) {
        var _window = window;
        var _document = document;
        var mousemove = 'mousemove';
        var mouseup = 'mouseup';
        var mousedown = 'mousedown';
        var EventListener = 'EventListener';
        var addEventListener = 'add'+EventListener;
        var removeEventListener = 'remove'+EventListener;
        var newScrollX, newScrollY;

        var dragged = [];
        var reset = function(i, el) {
            for (i = 0; i < dragged.length;) {
                el = dragged[i++];
                el = el.container || el;
                el[removeEventListener](mousedown, el.md, 0);
                _window[removeEventListener](mouseup, el.mu, 0);
                _window[removeEventListener](mousemove, el.mm, 0);
            }

            // cloning into array since HTMLCollection is updated dynamically
            dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
            for (i = 0; i < dragged.length;) {
                (function(el, lastClientX, lastClientY, pushed, scroller, cont){
                    (cont = el.container || el)[addEventListener](
                        mousedown,
                        cont.md = function(e) {
                            if (!el.hasAttribute('nochilddrag') ||
                                _document.elementFromPoint(
                                    e.pageX, e.pageY
                                ) == cont
                            ) {
                                pushed = 1;
                                lastClientX = e.clientX;
                                lastClientY = e.clientY;

                                e.preventDefault();
                            }
                        }, 0
                    );

                    _window[addEventListener](
                        mouseup, cont.mu = function() {pushed = 0;}, 0
                    );

                    _window[addEventListener](
                        mousemove,
                        cont.mm = function(e) {
                            if (pushed) {
                                (scroller = el.scroller||el).scrollLeft -=
                                    newScrollX = (- lastClientX + (lastClientX=e.clientX));
                                scroller.scrollTop -=
                                    newScrollY = (- lastClientY + (lastClientY=e.clientY));
                                if (el == _document.body) {
                                    (scroller = _document.documentElement).scrollLeft -= newScrollX;
                                    scroller.scrollTop -= newScrollY;
                                }
                            }
                        }, 0
                    );
                })(dragged[i++]);
            }
        }


        if (_document.readyState == 'complete') {
            reset();
        } else {
            _window[addEventListener]('load', reset, 0);
        }

        exports.reset = reset;
    }));


    //FOR SELECT
    //FOR SELECT
    //FOR SELECT
    //FOR SELECT
    var x, i, j, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /* For each element, create a new DIV that will act as the selected item: */
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /* For each element, create a new DIV that will contain the option list: */
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /* For each option in the original select element,
            create a new DIV that will act as an option item: */
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                /* When an item is clicked, update the original select box,
                and the selected item: */
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;

                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        var div = $(s).closest('div');
                        if (div.hasClass('linking')){
                            var link =  $(s).val();
                            location.href = link;
                        }
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            /* When the select box is clicked, close any other select boxes,
            and open/close the current select box: */
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }

    function closeAllSelect(elmnt) {
        /* A function that will close all select boxes in the document,
        except the current select box: */
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }

    /* If the user clicks anywhere outside the select box,
    then close all select boxes: */
    document.addEventListener("click", closeAllSelect);

    if ($('*').hasClass('template_4_accr')){
        $('.template_4_accr .box-item').each(function () {
            var height = $(this).find('.big').outerHeight();
            $(this).data('height',height);
            $(this).find('.big').css('padding',0);
            $(this).find('.big').height(0);
            $(this).removeClass('setup');
        });
        $('.template_4_accr .box-item').click(function (e) {
            var close = $(this).find('.close');
            if (!$(this).hasClass('open') && !close.is(e.target) && close.has(e.target).length === 0){
                e.preventDefault();
                $(this).addClass('open');
                var height = $(this).data('height');
                console.log(height);
                $(this).find('.big').height(height);
            }
        });
        $('.template_4_accr .box-item .close').click(function (e) {
            e.preventDefault();
            $(this).closest('.box-item').addClass('setup');
            $(this).closest('.box-item').removeClass('open');
            $(this).closest('.box-item').find('.big').height(0);
            $(this).closest('.box-item').removeClass('setup');
        });
    }
});