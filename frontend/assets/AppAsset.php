<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'style/lightgallery.min.css',
        'style/style.min.css',
    ];
    public $js = [
        'js/jquery.inputmask.bundle.min.js',
        'js/lightgallery-all.min.js',
        'js/slick.js',
        'js/script.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
