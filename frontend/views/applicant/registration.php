<?php

echo $pageModel->text;
?>
<form class="form">
    <input type="text" placeholder="<?= Yii::t('common', 'Прізвище')?>">
    <input type="text" placeholder="<?= Yii::t('common', 'Ім\’я')?>">
    <input type="text" placeholder="<?= Yii::t('common', 'По-батькові')?>">
    <input type="text" placeholder="<?= Yii::t('common', 'Логін')?>">
    <input type="password" placeholder="<?= Yii::t('common', 'Пароль')?>">
    <input type="password" placeholder="<?= Yii::t('common', 'Підтвердження паролю')?>">
    <input type="text" placeholder="<?= Yii::t('common', 'Адреса електронної пошти')?>">
    <input type="text" placeholder="<?= Yii::t('common', 'Телефон')?>">
    <input type="checkbox" id="agreement" name="agreement">
    <label for="agreement"><?= Yii::t('common', 'Я згоден з публічною офертою')?></label>
    <button>Зареєструватись</button>
</form>