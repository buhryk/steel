<?php

echo $pageModel->text;
?>

<div class="form-wrapper">
    <form class="form">
        <input type="text" placeholder="<?= Yii::t('common', 'Логін')?> *">
        <input type="password" placeholder="Пароль *">
        <div>
            <a href="/applicant/change-password"><?= Yii::t('common', 'Забули свій пароль/логін')?>?</a>
            <button><?= Yii::t('common', 'УВІЙТИ')?></button>
        </div>
    </form>
    <a href="/applicant/registration"><?= Yii::t('common', 'Зареєструватись')?> >>></a>
</div>
