<?php

echo $pageModel->text;
?>
<form class="form">
    <input type="radio" id="login" name="form">
    <label for="login"><?= Yii::t('common', 'Забув логін')?></label>
    <input type="radio" id="password" name="form">
    <label for="password"><?= Yii::t('common', 'Забув пароль')?></label>
    <input type="text" placeholder="<?= Yii::t('common', 'Електронна адреса')?> *">
    <!-- <input type="password" placeholder="<?= Yii::t('common', 'Пароль')?> *"> -->
    <button><?= Yii::t('common', 'УВІЙТИ')?></button>
</form>
