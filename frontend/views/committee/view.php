<?php

?>

<main class="page-template">
    <section class="block_header bg" style="background-image: url(/img/bg_1.png)">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <span>/</span>
                <a href="##">Комитеты</a>
                <span>/</span>
                <p><?= $model->title ?></p>
            </div>
        </div>
        <div class="container-100">
            <div class="text-information">
                <div class="title-block">
                    <?= $model->text ?>
                </div>
            </div>
        </div>
    </section>
    <section class="template_4_fulImg" style="background-color: #e0e9ee;">
        <div class="left txt-content">
            <?= $model->text2 ?>
        </div>
        <div class="right ">
            <img src="<?= $model->image ?>" alt="">
        </div>
    </section>
</main>