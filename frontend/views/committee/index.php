<?php

?>

<main class="page-template">
    <section class="block_header long" style="padding-bottom: 0">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
<!--                <span>/</span>-->
<!--                <a href="##">почему сталь</a>-->
                <span>/</span>
                <p>Комитеты</p>
            </div>
        </div>
        <div class="container-100">
            <div class="text-information">
                <div class="title-block">
                    <h1>Комитеты</h1>
                    <p>Для заказчиков строительства собраны примеры реализации объектов строительства в коммерческом,
                        аграрном, промышленном, жилищном, инфраструктурном и других сегментах недвижимости с конструктивными
                        решениями под любой тип зданий.</p>
                </div>
            </div>
        </div>
    </section>
    <?php use yii\helpers\Html;

    $i = 1; foreach ($model as $item): ?>
        <section class="template_4_defImg" style="<?= $i == 1 ? 'padding-bottom: 0;' : ''?> <?= $i % 2 != 0 ? 'background-color: #e0e9ee;' : ''?>" >
        <div class="container-100">
            <?php if ($i % 2 != 0): ?>
            <div class="left ">
                <img src="<?= $item->preview ?>" alt="">
            </div>
            <div class="right txt-content">
                <h2><?= $item->title ?></h2>
                <?= $item->description ?>
                <?= Html::a('Деятельность', ['committee/view', 'alias' => $item->alias], ['class' => 'btn-orange']) ?>
            </div>
            <?php else: ?>
                <div class="left txt-content">
                    <h2><?= $item->title ?></h2>
                    <?= $item->description ?>
                    <?= Html::a('Деятельность', Yii::$app->getUrlManager()->createUrl(['committee/view', 'alias' => $item->alias]), ['class' => 'btn-orange']) ?>
                </div>
                <div class="right">
                    <img src="<?= $item->preview ?>" alt="">
                </div>
            <?php endif; ?>
        </div>
    </section>
    <?php $i++; endforeach; ?>

</main>
