<?php

use backend\modules\news\models\News;
use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget; ?>
<main>
    <?= SectionWidget::widget([
        'section' => 'katalog-gotovykh-resheniy',
        'type' => 'main',
        'page' => $page,
        'katalogSlider' => $katalogSlider,
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'mirovye-keysy',
        'type' => 'case',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => '1',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg',
        'color' => 'e0e9ee',
    ]) ?>

    <section class="template_4_defVideo">
        <div class="container-100">
            <div class="left txt-content">
               <?= $freedom->text ?>
                <a href="<?= $freedom->url ?>" class="btn-orange"><?= $freedom->btn_name ?></a>
            </div>
            <div class="right">
                <div class="video">
                    <video poster="<?= $freedom->video_image ?>" autobuffer="" webkit-playsinline="false" playsinline="true">
                        <source src="<?= $freedom->video ?>" type="video/mp4">
                    </video>
                    <div class="btn"></div>
                </div>
            </div>
        </div>
    </section>

    <?= NewsWidget::widget([
        'alias' => $page->alias,
        'type' => News::TYPE_PUBLICATION,
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'dstu-po-ograzhdayuschim-konstruktsiyam-iz-metala',
        'type' => '2',
    ]) ?>


    <?= NewsWidget::widget([
            'alias' => $page->alias,
            'type' => News::TYPE_PRESENTATION,
    ]) ?>

    <section class="template_4_calendar"  style="background-color: #e0e9ee;">
        <div class="left txt-content">
            <h2>КАЛЕНДАРЬ СОБЫТИЙ</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p>
            <a href="##" class="btn-orange">ПЕРЕЙТИ В КАЛЕНДАРЬ</a>
        </div>
        <div class="right-dates" style="background-image: url(img/calendar.jpg)">
            <div class="box">
                <h3>19 СЕНТЯБРЯ СТАЛЬНАЯ ЭКОНОМИКА </h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
            <div class="box">
                <h3>34 ОКТЯБРЯ ЕЩЕ МЕРОПРИЯТИЕ</h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
            <div class="box">
                <h3>12 ДЕКАБРЯ КАК ПОСТРОИТЬ ДОМ!</h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
            <div class="box">
                <h3>28 ЯНВАРЯ ЗАВОД С НУЛЯ</h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
        </div>
    </section>

    <?= NewsWidget::widget([
        'alias' => $page->alias
    ]) ?>
</main>
