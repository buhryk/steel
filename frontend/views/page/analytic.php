<?php

use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget; ?>
<main>
    <?= SectionWidget::widget([
        'type' => 'main',
        'page' => $page,
    ]) ?>
    <?= SectionWidget::widget([
        'section' => 'indeks-utsss',
        'type' => '4',
        'color' => 'e0e9ee',
        '_class' => 'template_4_with-title-full',
    ]) ?>
    <?= SectionWidget::widget([
        'section' => 'indeks-stoimosti-konstruktsionnykh-materialov',
        'type' => '4',
        '_class' => 'template_4_with-title-full',
    ]) ?>
    <?= SectionWidget::widget([
        'section' => 'dolya-stali-v-stroitelstve',
        'type' => 'small-padding',
        'color' => 'e0e9ee',
        '_class' => 'template_4_defImg',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'analiticheskie-obzory',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg',

    ]) ?>

    <?= NewsWidget::widget() ?>
</main>
