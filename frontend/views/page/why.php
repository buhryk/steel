<?php

use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget; ?>
<main>
    <?= SectionWidget::widget([
        'section' => 'katalog-gotovykh-resheniy',
        'type' => 'main',
        'page' => $page,
        'katalogSlider' => $katalogSlider,
    ]) ?>
    <?= SectionWidget::widget([
        'section' => '1',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg',
        'color' => 'e0e9ee',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'mirovye-keysy',
        'type' => 'case',
    ]) ?>

    <?= NewsWidget::widget() ?>
</main>
