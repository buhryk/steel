<?php

use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget; ?>
<main>
    <?= SectionWidget::widget([
        'section' => 'katalog-gotovykh-resheniy',
        'type' => 'main',
        'page' => $page,
        'katalogSlider' => $katalogSlider,
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'sortament-metalloprokata-v-nalichii',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'ekonomika',
        'type' => '1',
        'color' => 'e0e9ee',
    ]) ?>


    <?= SectionWidget::widget([
        'section' => 'konsultatsii-ot-uchastnikov-otrasli',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'dstu-po-ograzhdayuschim-konstruktsiyam-iz-metala',
        'type' => '2',
        'color' => 'e0e9ee',

    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'obuchayuschie-materialy',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg',
    ]) ?>

    <section class="template_4_calendar"  style="background-color: #e0e9ee;">
        <div class="left txt-content">
            <?= $kalendar->text ?>
            <a href="<?= $kalendar->url ?>" class="btn-orange"><?= $kalendar->btn_name ?></a>
        </div>
        <div class="right-dates" style="background-image: url(<?= $kalendar->image ?>)">
            <div class="box">
                <h3>19 СЕНТЯБРЯ СТАЛЬНАЯ ЭКОНОМИКА </h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
            <div class="box">
                <h3>34 ОКТЯБРЯ ЕЩЕ МЕРОПРИЯТИЕ</h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
            <div class="box">
                <h3>12 ДЕКАБРЯ КАК ПОСТРОИТЬ ДОМ!</h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
            <div class="box">
                <h3>28 ЯНВАРЯ ЗАВОД С НУЛЯ</h3>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <a href="##" class="btn-orange">РЕГИСТРАЦИЯ</a>
            </div>
        </div>
    </section>
    <?= NewsWidget::widget(['alias' => $page->alias]) ?>
</main>
