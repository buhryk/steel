<?php

use backend\modules\section\models\SectionItem;
use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget;
use frontend\widgets\SubscribeWidget;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$first_widgets_element = reset($widgets)['widget'];

?>
<main class="<?= $first_widgets_element['widget'] != SectionItem::TYPE_HEADER ? 'page-template' : '' ?> template_4">
    <?php if($first_widgets_element['widget'] != SectionItem::TYPE_HEADER): ?>
        <section class="block_header ">
            <div class="container-100">
                <div class="breadcrumbs">
                    <a href="<?=Url::to(['/']) ?>">Главная</a>
                    <span>/</span>
                    <p><?= str_replace("\n", ' ', strip_tags($page->title)) ?></p>
                </div>
            </div>
            <div class="container-100">
                <div class="text-information">
                    <div class="title-block">
                        <h1><?= $page->title ?></h1>
                        <p><?= $page->description ?></p>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php
    foreach ($widgets as $widget){
        if ($widget['widget'] == SectionItem::TYPE_HEADER) { // HEADER
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'type' => 'main',
                'page' => $page,
                'katalogSlider' => $widget['alias'] ? $katalogSlider : false,
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif (in_array($widget['widget'], [1,2,3])){ // NEWS, PUBLICATIONS, PRESENTATIONS
            echo NewsWidget::widget([
                'alias' => $page->alias,
                'section' => $widget['alias'],
                'color' => $widget['color'] ? 'blue-section' : '',
                'type' => $widget['widget']
            ]);
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_1) {

            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 4,
                'color' => $widget['color'] ? 'blue-section' : '',
                '_class' => 'template_4_with-title-full',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_2) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 'small-padding',
                'color' => $widget['color'] ? 'blue-section' : '',
                '_class' => 'template_4_defImg',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_3) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'slider' => $widget['slider'],
                'type' => 'case',
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        }  elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_4) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 2,
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_5) { // video
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 'video',
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_6) { // calendar
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'calendar' => $widget['calendar'],
                'type' => 'calendar',
                'page' => $page,
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_7) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 5,
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_8) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'slider' => $widget['slider'],
                'type' => 6,
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_9) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'color' => $widget['color'] ? 'blue-section' : '',
                'type' => 1
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_10) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 'small-padding',
                'color' => $widget['color'] ? 'blue-section' : '',
                '_class' => 'template_4_defImg small-padding',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_11) {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 'small-padding',
                'color' => $widget['color'] ? 'blue-section' : '',
                '_class' => 'template_4_defImg small-padding',
                'imageLeft' => true,
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_12) { // Section with full title
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => '7-full-title',
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_13) { // Section with numbers
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'slider' => $widget['slider'],
                'type' => '8-numbers',
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        }  elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_14) { // Section with documents
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => '9-documents',
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_15) { // FAQ
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'type' => 'faq',
                'page' => $page,
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_16) { // Who can be a member
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'form' => $widget['form'],
                'slider' => $widget['slider'],
                'type' => 'who-can-be-member',
                'color' => $widget['color'] ? 'blue-section' : '',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_26) { // news slider
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'slider' => $widget['slider'],
                'color' => $widget['color'] ? 'blue-section' : '',
                'type' => '26',
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_SUBSCRIBE_1) { // Subscribe 1
            echo SubscribeWidget::widget(['type' => 1]);
        } elseif ($widget['widget'] == SectionItem::TYPE_SUBSCRIBE_2) { // Subscribe 2
            echo SubscribeWidget::widget(['type' => 2]);
        } elseif ($widget['widget'] == SectionItem::TYPE_TABLE) { // Table
            echo  SectionWidget::widget([
                'type' => 'table',
                'section' => $widget['alias'],
                'color' => $widget['color'] ? 'blue-section' : '',
                'page' => $page,
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_MEMBER) { // MEMBER
            echo  SectionWidget::widget([
                'type' => 'member',
                'slider' => $widget['slider'],
                'section' => $widget['alias'],
                'color' => $widget['color'] ? 'blue-section' : '',
                'page' => $page,
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_24) {
            echo  SectionWidget::widget([
                'type' => '24',
                'color' => $widget['color'] ? 'blue-section' : '',
                'section' => $widget['alias'],
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_25) {
            echo  SectionWidget::widget([
                'type' => '25',
                'color' => $widget['color'] ? 'blue-section' : '',
                'section' => $widget['alias'],
            ]) ;
        } elseif ($widget['widget'] == SectionItem::TYPE_PATTERN_26) {
            echo  SectionWidget::widget([
                'type' => '26',
                'color' => $widget['color'] ? 'blue-section' : '',
                'section' => $widget['alias'],
            ]) ;
        } else {
            echo  SectionWidget::widget([
                'section' => $widget['alias'],
                'type' => $widget['type'],
                'color' => $widget['color'] ? 'blue-section' : '',
                '_class' => $widget['class'],
                'page' => $page,
                'katalogSlider' => $katalogSlider,
            ]) ;
        }
    }
    ?>

</main>
<?php

$this->registerJs("

   
", \yii\web\View::POS_READY);
?>
