<?php

use backend\modules\section\models\SectionItem;
use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

?>
<main>
    <?= SectionWidget::widget([
        'section' => 'katalog-gotovykh-resheniy',
        'type' => 'main',
        'page' => $page,
        'katalogSlider' => $katalogSlider,
    ]) ?>
    <section class="template_4_double-coll" style="background-image: url(<?= $pochemu->image ?>)">
        <div class="container-100">
            <div class="left txt-content">
                <?= $pochemu->text ?>
            </div>
            <div class="right txt-content">
                <?= $vybrat->text ?>
                <a href="<?= $vybrat->url ?>" class="btn-orange"><?= $vybrat->btn_name ?></a>
            </div>
        </div>
    </section>
    <section class="template_4_services mTop">
        <div class="top">
            <div class="container-100">
                <h2 class="title_1"><?= $servisy->title ?></h2>
            </div>
        </div>
        <div class="bottom" style="background-image: url(<?= $servisy->image ?>)">
            <?php foreach ($servisySlider->items as $item):?>
                <div class="box">
                    <?= $item->title ?>
                    <?= $item->description ?>
                    <a href="<?= $item->url ?>" class="btn-orange">ПОДРОБНЕЕ</a>
                </div>
            <?php endforeach; ?>
        </div>
    </section>

    <?= SectionWidget::widget([
            'section' => 'ekonomika',
            'type' => '1',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'dstu-po-ograzhdayuschim-konstruktsiyam-iz-metala',
        'type' => '2',
        'color' => 'e0e9ee',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => '1',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg',
    ]) ?>

    <?= NewsWidget::widget(['alias' => $page->alias]) ?>
</main>