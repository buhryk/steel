<?php

use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget; ?>
<main>
    <?= SectionWidget::widget([
        'type' => 'main',
        'page' => $page,
    ]) ?>

    <?= NewsWidget::widget() ?>

    <?= SectionWidget::widget([
        'section' => 'intervyu',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg small-padding',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'stati',
        'type' => 'small-padding',
        'imageLeft' => true,
        'color' => 'e0e9ee',
        '_class' => 'template_4_defImg small-padding',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'fotogalereya',
        'type' => 'small-padding',
        '_class' => 'template_4_defImg small-padding',
    ]) ?>

    <?= SectionWidget::widget([
        'section' => 'video',
        'type' => 'small-padding',
        'imageLeft' => true,
        'color' => 'e0e9ee',
        '_class' => 'template_4_defImg small-padding',
    ]) ?>

</main>
