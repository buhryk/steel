<?php

use frontend\widgets\NewsWidget;
use frontend\widgets\SectionWidget; ?>
<main class="page-template">
    <section class="block_header ">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <span>/</span>
                <p><?= $page->title ?></p>
            </div>
        </div>
        <div class="container-100">
            <div class="text-information">
                <div class="title-block">
                    <h1><?= $page->title ?></h1>
                    <p><?= $page->description ?></p>
                </div>
            </div>
        </div>
    </section>
    <section class="template_4_indexContainer" style="background-color: #e0e9ee;">

    </section>
</main>
