<?php
use common\models\Lang;
use frontend\widgets\BreadcrumbsWidget;

?>
<div class="wrapper">
    <div class="bread-crumbs">
        <?= BreadcrumbsWidget::widget() ?>
    </div>
    <?php if ($page->additional_title): ?>
        <h2 class="title"><?=$page->additional_title ?></h2>
    <?php endif; ?>
    <div class="contacts__list">
        <?= $page->text; ?>
    </div>
</div>
<?php if (Lang::getCurrent()->id == 1):?>
    <iframe   id="map" src="https://www.google.com/maps/d/embed?mid=1cGbvNQs7FbLle9heHYCN_ssz1wdkCxQJ" ></iframe>
<?php else:?>
    <iframe  id="map" src="https://www.google.com/maps/d/embed?mid=14WHR6e3ZbeD75ZIw18Iw1n9VvYi63Q2D&hl=en" ></iframe>
<?php endif;?>
