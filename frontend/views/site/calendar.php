<?php
use yii\helpers\Url;
?>

<main class="page-template">
    <section class="block_header long">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="<?=Url::to(['/']) ?>">Главная</a>
                <span>/</span>
                <p><?= $page->title ?></p>
            </div>
        </div>
        <div class="container-100">
            <div class="text-information">
                <div class="title-block">
                    <h1><?= $page->title ?></h1>
                    <p><?= $page->description ?></p>
                </div>
            </div>
        </div>
    </section>
    <section class="template_4_calendar-nav">
        <div class="container-100">
            <?php if ($last_year == $year): ?>
                <div class="left-menu">
                    <a href="<?=Url::to(['/site/calendar/', 'days' => 365]) ?>" class="link active">Год</a>
                    <a href="<?=Url::to(['/site/calendar/', 'days' => 90]) ?>" class="link">Квартал</a>
                    <a href="<?=Url::to(['/site/calendar/', 'days' => 30]) ?>" class="link">Месяц</a>
                    <a href="<?=Url::to(['/site/calendar/', 'days' => 7]) ?>" class="link">Неделя</a>
                </div>
            <?php endif; ?>
            <div class="right-menu">
                <?php  foreach ($years as $item):?>
                    <a href="<?=Url::to(['/site/calendar/', 'year' => $item['year']]) ?>" class="link <?= $item['year'] == $year ? 'active' : '' ?>"><?= $item['year']; ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section class="template_4_services">
        <div class="bottom" style="background-image: url(<?= $page->image ?>)">
            <?php foreach ($events as $event): ?>
                <div class="box"  style="background-image: url(img/services.jpg)">
                    <h3><?= $event['title'] ?></h3>
                   <?= $event['short_description'] ?>
                    <a href="##" class="btn-orange">ПОДРОБНЕЕ</a>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
</main>