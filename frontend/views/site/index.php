<?php

use backend\modules\slider\models\Section;

use frontend\widgets\SubscribeWidget;

use yii\widgets\ActiveForm;

use yii\helpers\Html;

use yii\helpers\StringHelper;

use yii\helpers\Url;

use backend\modules\news\models\NewsCategory;





?>

<main class="page-home">

    <section class="page-home__main" style="background-image: url(/img/main.jpg)">

        <div class="row">

            <div class="left">

                <img src="/img/logo.png" alt="" class="logo">

            </div>

            <div class="right">

                <p>Фрейм с индексом</p>

            </div>

        </div>

        <div class="row">

            <div class="left">

                <?= $znaete->text ?>

                <a href="<?= $znaete->url ?>">К КАТАЛОГУ</a>

            </div>

            <div class="right">

                <?php if ($lastNews): ?>

                    <?php foreach ($lastNews as $item): ?>

                        <div class="box">

                            <?= Yii::$app->thumbnail->img($item->image, ['thumbnail' => ['width' => 480, 'height' => 270]], [

                                'alt' => $item->title,

                            ]) ?>

                            <h2><?= StringHelper::truncate(strip_tags($item->title), 70 )?></h2>

                            <p> <?= StringHelper::truncate(strip_tags($item->description), 250 )?></p>

                            <div class="bottom"><p><?=date("d.m.Y",strtotime($item->data_pub))?></p>

                                <?= Html::a(Yii::t('common', 'ПОДРОБНЕЕ'), ['/news/news/view', 'alias' => $item->alias]) ?>

                            </div>

                        </div>

                    <?php endforeach; ?>



                    <a href="<?= Url::to(['/news/news/all']) ?>" class="box all">

                        <h2><?= Yii::t('common', 'Все новости') ?></h2>

                    </a>

                <?php endif; ?>

            </div>

        </div>

    </section>



    <section class="page-home__ready" style="background-image: url(/img/bg_1.png)">

        <div class="container-100">

            <div class="left">

                <?= $katalog->text ?>

                <a href="<?= $katalog->url ?>" class="btn-orange">ПРОСМОТРЕТЬ ВЕСЬ КАТАЛОГ</a>

            </div>

            <div class="right">

                <div class="dragscroll draggable">
                    <p id="floatingmes">Нажмите и потяните</p>

                    <?php foreach ($katalogSlider->childrens as $item):?>

                        <div data-href="<?= $katalog->url ?>?category_id=<?= $item->id ?>" class="box">

                            <img src="<?= $item->image ?>" alt="">

                            <h3><?= $item->title ?></h3>

                        </div>

                    <?php endforeach; ?>

                </div>

            </div>

        </div>

    </section>

    <section class="page-home__assambl">

        <div class="container-100">

            <div class="content">

                <div class="left">

                    <?= $podrobnee->text ?>

                </div>

                <div class="right">

                    <div class="video">

                        <video poster="<?= $podrobnee->video_image ?>" autobuffer="" webkit-playsinline="false"  playsinline="true">

                            <source src="<?= $podrobnee->video ?>" type="video/mp4">

                        </video>

                        <div class="btn"></div>

                    </div>

                </div>

            </div>

            <div class="row-b">

                <div class="left">

                    <img src="<?= $podrobnee->image ?>" alt="">

                </div>

            </div>

        </div>

    </section>

    <?= SubscribeWidget::widget(['type' => 1]) ?>

</main>

<?php



//$this->registerJs("

//$.ajax({

//            url: '/site/get-index',

//            type: 'get',

//

//            success: function(html) {

//                $('.page-home__main .right').html($(html).find('.col-md-8').html());

//

//            }

//            });

//

//", \yii\web\View::POS_READY);

?>



