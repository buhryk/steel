<?php
use yii\widgets\LinkPager;
?>

<div class="news">
    <?php foreach ($publications as $item): ?>
        <div class="news-item publishing-item">
            <div class="news-item__image">
                <?= Yii::$app->thumbnail->img($item->image, ['thumbnail' => ['width' => 250, 'height' => 250]], [
                    'alt' => $item->title,
                ]) ?>
            </div>
            <div class="news-item__info">
                <p class="news-item__date"><?= $item->title ?></p>
                <?= $item->description ?>
            </div>
        </div>
    <?php endforeach; ?>

    <?php
    echo LinkPager::widget([
        'pagination' => $pages,
        'options' => ['class' => ''],
        'firstPageLabel' => '&nbsp;',
        'lastPageLabel' => '&nbsp;',
        'prevPageLabel' => '<img src="/images/pagination-prev.png" alt="prev">',
        'nextPageLabel' => '<img src="/images/pagination-next.png" alt="next">',
        'activePageCssClass' => 'active',
        'maxButtonCount' => 3,
        'linkOptions' => ['class' => ''],
    ]);
    ?>
</div>