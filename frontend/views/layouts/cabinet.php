<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\modules\core\models\Setting;
use backend\modules\tenders\models\tendersCategory;
use common\models\Lang;
use frontend\assets\CabinetAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\BreadcrumbsWidget;

AppAsset::register($this);
CabinetAsset::register($this);

$action = Yii::$app->controller->action->id;

$tenders = tendersCategory::find()->where(['status' => tendersCategory::STATUS_ACTIVE])->all();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Setting::getValueByKey('tag_manager_head')?>
</head>
<body>
<?php $this->beginBody() ?>
<?= Setting::getValueByKey('tag_manager_body')?>

<?= $this->render('header');?>
<main class="page-template page-account">
    <section class="block_header">
        <div class="container-100">
            <div class="breadcrumbs">
                <a href="<?=Url::to(['/']) ?>">Главная</a>
                <span>/</span>
                <p>ЛИЧНЫЙ КАБИНЕТ</p>
            </div>
        </div>
        <div class="container-100">
            <div class="text-information">
                <div class="title-block">
                    <h1>ЛИЧНЫЙ КАБИНЕТ</h1>
                </div>
            </div>
        </div>
    </section>
    <div class="block_catalog with-aside">
        <div class="container-100">
            <aside class="acc" >
                <p data-tab="1" class="acc_title" onclick="location.href='/member/index'">ИНФОРМАЦИЯ  О КОМПАНИИ</p>
                <p data-tab="2" class="acc_title" onclick="location.href='/member/project?type=1'">РЕАЛИЗОВАННЫЕ ПРОЕКТЫ</p>
                <p data-tab="3" class="acc_title" onclick="location.href='/member/news'">НОВОСТИ КОМПАНИИ</p>
                <p data-tab="4" class="acc_title triangle"
                   onclick="<?= $action != 'tenders' ? "location.href='/member/tenders'" : '' ?>">Тендеры</p>
                <div class="acc_body" style="display: <?= $action == 'tenders' ? 'block' : 'none' ?>">
                    <?php foreach ($tenders as $item): ?>
                        <div class="check-box">
                            <input type="checkbox" name="categories_id[]" id="cat_<?= $children->id ?>_item_<?= $item->id ?>" value="<?= $item->id ?>">
                            <label for="cat_<?= $children->id ?>_item_<?= $item->id ?>"><?= $item->title ?></label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <p data-tab="5" class="acc_title" onclick="location.href='/member/project?type=2'">ИНВЕСТИЦИОННЫЕ ПРОЕКТЫ</p>
                <p data-tab="6" class="acc_title" onclick="location.href='/member/write'">НАПИСАТЬ АДМИНИСТРАТОРУ</p>
                <?= Html::a('ВЫХОД', ['member/logout'], [
                    'class' => 'acc_title',
                    'onclick' => 'location.href=\'/member/logout\''
                ]) ?>
            </aside>
            <div id="lc_content" style="width: 100%">
                <?=$content?>
            </div>
        </div>
    </div>
</main>
<?= $this->render('footer'); ?>

<?= Setting::getValueByKey('tag_manager_body_end')?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
