<?php

use backend\modules\core\models\Setting;
use backend\modules\core\models\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\yii\helpers\Url::remember();
?>
<footer class="footer">
    <div class="container-100">
        <a href="<?=Url::to(['/']) ?>" class="logo">
            <img src="/img/logo2.png" alt="">
        </a>
        <div class="cont">
            <?= \backend\modules\core\models\Widget::getWidgetByKey('footer-block-contact') ?>
        </div>

        <?= \backend\modules\core\models\Widget::getWidgetByKey('footer-block-links') ?>

        <div class="socials">
            <?php
            $iconsItems = Setting::getDataByKey(Setting::FOOTER_SOCIAL_ICONS, Setting::ADDITIONAL_DATA_ITEMS);
            ?>
            <?php if ($iconsItems): ?>
                <?php foreach ($iconsItems as $item): ?>
                    <a target="_blank" class="fb" href="<?= $item['link'] ?>">
                        <img src="<?= $item['icons'] ?>">
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>

<!--            <a target="_blank" class="fb" href="##">-->
<!--                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"/></svg>-->
<!--            </a>-->
<!--            <a target="_blank" class="yt" href="##">-->
<!--                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" class="svg-inline--fa fa-youtube fa-w-18" role="img" viewBox="0 0 576 512"><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"/></svg>-->
<!--            </a>-->
        </div>
    </div>
    <div class="toppp">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">    <path d="M 15 4 L 15 9 L 2 9 L 2 15 L 15 15 L 15 20 L 23 12 L 15 4 z"></path></svg>
        <p>вверх</p>
    </div>
</footer>