<?php
use common\widgets\WLang;
use yii\helpers\Url;
?>
<header class="header">
    <div class="left">
        <div class="search">
            <form action="<?=Url::to(['/search/index'])?>">
                <div class="btn">
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" viewBox="0 0 512 512"><path fill="white" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"/></svg>
                    <input name="q" class="btn" type="submit" value="">
                </div>
                <input class="input" type="text" placeholder="<?= Yii::t('common', 'Поиск')?>">
            </form>
        </div>
        <a href="<?=Url::to(['/']) ?>" class="logo">
            <img src="/img/logo.png" alt="">
        </a>

    </div>

    <?= \frontend\widgets\MainMenuWidget::widget() ?>

</header>
<?php if( Yii::$app->session->hasFlash('success') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert" style="display: block">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif;?>
