<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>
<main>
    <section class="members" style="background-image: url(<?= $page->image ?>)">
        <div class="container-100">
            <h1><?= $page->title ?></h1>
            <div class="info">
                <div class="left">
                    <?= $page->text ?>
                </div>
                <div class="right">
                    <h2 class="title_1">ВОЙДИТЕ В ЛИЧНЫЙ КАБИНЕТ</h2>

                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnBlur' => false,
                        'validateOnType' => false,
                        'validateOnChange' => false,
                    ]) ?>
                    <?= $form->field($model, 'login', [
                        'template' => '{input}{error}',
                        'labelOptions' => ['class' => 'form__label'],
                        'inputOptions' => [
                            'autofocus' => 'autofocus',
                            'class' => 'form__input login__input',
                            'placeholder' => Yii::t('login', 'E-mail'),
                            'id' => 'enter-login',
                            'tabindex' => '1'
                        ]
                    ]); ?>
                    <?= $form->field($model, 'password', [
                        'template' => '{input}{error}',
                        'labelOptions' => ['class' => 'form__label'],
                        'inputOptions' => [
                            'type' => 'password',
                            'class' => 'form__input login__input',
                            'placeholder' => Yii::t('login', 'Пароль'),
                            'id' => 'enter-pass',
                            'tabindex' => '2'
                        ]
                    ]) ?>
                    <input type="submit" class="btn-orange" value="ВОЙТИ В ЛИЧНЫЙ КАБИНЕТ">

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </section>
</main>
