<?php

use frontend\widgets\CabinetMenu;
use yii\helpers\Html;
use yii\helpers\StringHelper;

?>

            
    <div data-tabBody="4" class="tab-body" style="display: block">
        <div class="catalog-list withoutImg">
            <?php foreach ($models as $model):?>
                <div class="box">

                    <?= is_file($model->image) ? Yii::$app->thumbnail->img($model->image, ['thumbnail' => ['width' => 480, 'height' => 270]], [
                        'alt' => $model->title,
                    ]) : '' ?>

                    <h2><?= StringHelper::truncate(strip_tags($model->title), 70 )?></h2>
                    <p> <?= StringHelper::truncate(strip_tags($model->text), 250 )?></p>
                    <div class="bottom"><p><?=date("d.m.Y",strtotime($model->data_pub))?></p>
                        <?= Html::a(Yii::t('common', 'ПОДРОБНЕЕ'), ['/news/news/view', 'alias' => $model->alias]) ?>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
