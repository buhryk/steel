<?php

use frontend\widgets\CabinetMenu;
use yii\helpers\Html;
use yii\helpers\StringHelper;

?>

<div data-tabBody="2" class="tab-body" style="display: block">
    <div class="right tree-cases">
        <?php foreach ($models as $model):?>
            <a href="<?= Yii::$app->getUrlManager()->createUrl(['/catalog/catalog/view', 'alias' => $model->alias]) ?>" class="case-box">
                <img src="<?= $model->image ?>" alt="">
                <h3><?= StringHelper::truncate(strip_tags($model->title), 70 )?></h3>
                <p><?=date("d.m.Y",strtotime($model->data_pub))?></p>
                <p> <?= StringHelper::truncate(strip_tags($model->text), 250 )?></p>
            </a>
        <?php endforeach;?>
    </div>
</div>
