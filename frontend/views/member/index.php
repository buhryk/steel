<?php


?>

<div data-tabBody="1" class="tab-body" style="display: block">
                <div class="profile-data">
                    <div class="img">
                        <img src="<?= $user->image ?>" alt="">
                    </div>
                    <div class="flex">
                        <div class="box txt-content">
                            <h3><?= $user->name ?></h3>
                            <?= $user->description ?>
                        </div>
                        <div class="box txt-content">
                            <h3><?= $user->line_of_activity_name ?></h3>
                            <?= $user->line_of_activity_description ?>
                        </div>
                        <div class="box txt-content">
                            <h3>КОНТАКТЫ</h3>
                            <?= $user->contact ?>
                        </div>
                    </div>
                </div>
            </div>
