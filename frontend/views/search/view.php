<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
$this->title = Yii::t('common', 'Пошук').'|'.$q;
$this->params['pageH1'] = $this->title;
$this->params['breadcrumbs'][] = Yii::t('common', 'Пошук');
$this->params['breadcrumbs'][] = $q;

?>
<center>
    <h1><?= Yii::t('common', 'Пошук') ?></h1>
    <form class="form" action="<?=Url::to(['/search/index'])?>">
        <input  name="q" value="<?=$q?>" type="text" placeholder="<?= Yii::t('common', 'пошук')?>">
        <button><?= Yii::t('common', 'Знайти')?></button>
    </form><br>
</center>
<?php if (!empty($models)): ?>

    <h3 align="center"><?= Yii::t('common', 'Сторінки')?></h3>
    <?php foreach ($models as $model):?>

        <p class="footer__title"><?= Html::a('<h4>'.$model[title].'</h4>', ['/' . $model[alias]]) ?></p>
        <p><?=StringHelper::truncate($model[description], 186)?></p>

    <?php endforeach;?>

<?php endif;?>

<?php //if(!empty($menu)): ?>
<!---->
<!--    <h3 align="center">--><?//= Yii::t('common', 'Категорії сторінок')?><!--</h3>-->
<!--    --><?php //foreach ($menu as $item):?>
<!--        --><?//= \frontend\widgets\SearchMenuWidget::widget(['parent' => $item[id]]) ?>
<!--    --><?php //endforeach;?>
<!---->
<?php //endif;?>

<div class="pagination">
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
        'options' => ['class' => ''],
        'firstPageLabel' => '&nbsp;',
        'lastPageLabel' => '&nbsp;',
        'prevPageLabel' => '<img src="/images/pagination-prev.png" alt="prev">',
        'nextPageLabel' => '<img src="/images/pagination-next.png" alt="next">',
        'activePageCssClass' => 'active',
        'maxButtonCount' => 3,
        'linkOptions' => ['class' => ''],
    ]);
    ?>
</div>

<?php if( empty($menu) && empty($models) ): ?>
    <h3><?= Yii::t('common', 'По запиту')?> "<?= $q ?>" <?= Yii::t('common', 'нічого не знайдено')?></h3>
<?php endif;?>
