<?php

use yii\db\Migration;

/**
 * Class m171211_105435_add_colum_menu
 */
class m171211_105435_add_colum_menu extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('menu', 'absolute_url', $this->smallInteger()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171211_105435_add_colum_menu cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_105435_add_colum_menu cannot be reverted.\n";

        return false;
    }
    */
}
