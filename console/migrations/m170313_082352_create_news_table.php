<?php

use yii\db\Migration;

/**
 * Class m171209_092736_news
 */
class m170313_082352_create_news_table extends Migration
{
    public $news = '{{%news}}';
    public $news_lang = '{{%news_lang}}';

    public $category = '{{%news_category}}';
    public $category_lang = '{{%news_categor_lang}}';


    public function safeUp()
    {


        $this->createTable($this->news, [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'image' => $this->string(),
            'alias' => $this->string(),
            'category_id' => $this->integer(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable($this->news_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->null(),
            'text' => $this->text()->notNull(),
        ]);

        $this->createTable($this->category, [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'alias' => $this->string(),
            'additional_data' => $this->json(),
            'parent_id' => $this->integer()->null()
        ]);

        $this->createTable($this->category_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->null(),
            'text' => $this->text()->null()
        ]);

        $this->addForeignKey('news-category-id', $this->news, 'category_id', $this->category, 'id', 'CASCADE');
        $this->addForeignKey('news-lang-id', $this->news_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');
        $this->addForeignKey('news-record-id', $this->news_lang, 'record_id', $this->news, 'id', 'CASCADE');

        $this->addForeignKey('news-category-lang-id', $this->category_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');
        $this->addForeignKey('news-category-record-id', $this->category_lang, 'record_id', $this->category, 'id', 'CASCADE');

        $this->addPrimaryKey('news_lang-pk', $this->news_lang, ['record_id', 'lang_id']);
        $this->addPrimaryKey('news_category_lang-pk', $this->category_lang, ['record_id', 'lang_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171209_092736_news cannot be reverted.\n";

        return false;
    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }
}
