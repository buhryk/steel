<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image`.
 */
class m171211_104635_create_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('image', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(255),
            'record_id' => $this->integer(),
            'path' => $this->string(255)->notNull(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1),
            'sort' => $this->integer()->notNull()->defaultValue(99),
            'is_main' => $this->smallInteger()->defaultValue(0),
            'key' => $this->string(40)
        ]);
        $this->createTable('image_lang', [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'alt' => $this->string(255),
            'lang_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('image');
    }
}
