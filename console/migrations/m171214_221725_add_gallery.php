<?php

use yii\db\Migration;

/**
 * Class m171214_221725_add_gallery
 */
class m171214_221725_add_gallery extends Migration
{
    public $gallery = '{{gallery}}';
    public $gallery_lang = '{{gallery_lang}}';
    public $gallery_item = '{{gallery_item}}';

    public function safeUp()
    {
//        $this->createTable($this->gallery, [
//            'id' => $this->primaryKey(),
//            'position' => $this->integer(),
//            'status' => $this->smallInteger()->defaultValue(1),
//            'created_at' => $this->integer(),
//            'updated_at' => $this->integer(),
//            'image' => $this->string(),
//            'alias' => $this->string(),
//            'category_id' => $this->integer(),
//            'additional_data' => $this->json(),
//            'type' => $this->smallInteger(1)
//        ]);
//
//        $this->createTable($this->gallery_lang, [
//            'record_id' => $this->integer()->notNull(),
//            'lang_id' => $this->integer()->notNull(),
//            'title' => $this->string(128)->notNull(),
//            'description' => $this->text()->null(),
//        ]);
//
//        $this->createTable($this->gallery_item, [
//            'id' => $this->primaryKey(),
//            'gallery_id' => $this->integer(),
//            'position' => $this->integer(),
//            'value' => $this->string()
//        ]);




        $this->addForeignKey('gallery-category-id', $this->gallery, 'category_id', 'page_category', 'id', 'CASCADE');
        $this->addForeignKey('gallery-lang-id', $this->gallery_lang, 'lang_id', 'lang', 'id', 'CASCADE');
        $this->addForeignKey('gallery-record-id', $this->gallery_lang, 'record_id', $this->gallery, 'id', 'CASCADE');

        $this->addForeignKey('gallery-item-gallery-id', $this->gallery_item, 'gallery_id', $this->gallery, 'id', 'CASCADE');

        $this->addPrimaryKey('gallery-lang-pk', $this->gallery_lang, ['record_id', 'lang_id']);

    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }
}
