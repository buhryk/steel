<?php

use yii\db\Migration;

/**
 * Class m171211_075852_create_menu
 */
class m171211_075852_create_menu extends Migration
{
    public $menu = '{{%menu}}';
    public $menu_lang = '{{%menu_lang}}';

    public function safeUp()
    {
        $this->createTable($this->menu, [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'position' => $this->integer(),
            'parent' => $this->integer()->null(),
            'url' => $this->string(),
            'page_id' => $this->integer()->null(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable($this->menu_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('menu-lang-id', $this->menu_lang, 'lang_id', 'lang', 'id', 'CASCADE');
        $this->addForeignKey('menu-record-id', $this->menu_lang, 'record_id', $this->menu, 'id', 'CASCADE');

        $this->addPrimaryKey('manu_lang-pk', $this->menu_lang, ['record_id', 'lang_id']);
    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171211_075852_create_menu cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_075852_create_menu cannot be reverted.\n";

        return false;
    }
    */
}
