<?php

use yii\db\Migration;

/**
 * Handles the creation of table `video`.
 */
class m171213_102138_create_video_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('video', [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'data_pub' => $this->dateTime()->null(),
            'video' => $this->string(),
            'alias' => $this->string(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable('video_lang', [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->addForeignKey('video-lang-id','video_lang', 'lang_id', '{{%lang}}', 'id', 'CASCADE');
        $this->addForeignKey('video-record-id', 'video_lang', 'record_id', '{{%video}}', 'id', 'CASCADE');
        $this->addPrimaryKey('video_lang-pk', 'video_lang', ['record_id', 'lang_id']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('video');
    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }
}
