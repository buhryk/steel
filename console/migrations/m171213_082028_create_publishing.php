<?php

use yii\db\Migration;

/**
 * Class m171213_082028_create_publishing
 */
class m171213_082028_create_publishing extends Migration
{
    public $publication = '{{%publication}}';
    public $publication_lang = '{{%publication_lang}}';

    public function safeUp()
    {
        $this->createTable($this->publication, [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'image' => $this->string(),
            'alias' => $this->string(),
            'menu_id' => $this->integer(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable($this->publication_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->null(),
        ]);

        $this->addForeignKey('publication-lang-id', $this->publication_lang, 'lang_id', 'lang', 'id', 'CASCADE');
        $this->addForeignKey('publication-record-id', $this->publication_lang, 'record_id', $this->publication, 'id', 'CASCADE');
        $this->addForeignKey('publication-menu-id-key', $this->publication, 'menu_id', 'menu', 'id', 'CASCADE');

        $this->addPrimaryKey('publication_lang-pk', $this->publication_lang, ['record_id', 'lang_id']);
    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }

    public function safeDown()
    {
        echo "m171213_082028_create_publishing cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171213_082028_create_publishing cannot be reverted.\n";

        return false;
    }
    */
}
